<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    protected $fillable = ['id','name'];
}
