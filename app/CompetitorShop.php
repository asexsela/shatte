<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class CompetitorShop extends Model
{
    protected $fillable = ['id','name','mall','city_id','type','status', 'author', 'job'];
}
