<?php

namespace EvonaApp\Http\Controllers\Admin;

use Auth;
use Validator;

use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(){
        
        if (Auth::check() && Auth::user()->isAdmin()){
            return redirect()->route('admin.dashboard');
        }else{
            return view('admin.login');
        }
        
    }
    
    public function login(Request $request){
        
        $validator = $this->validateLogin($request);
        
        if (!$validator->fails()){
            
            if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'admin' => 1], true)){
                
                return back();
                
            }else{
                
                $validator->errors()->add('email', 'Пользователь не найден');
                return back()->withErrors($validator)->withInput();   
            }
        }else{
            return back()->withErrors($validator)->withInput();
        }
    }
    
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.index');
    }
    
    public function dashboard(){
        
        return redirect()->route('admin.music.index');
        
        return view('admin.dashboard');
    }
    
    private function validateLogin($request){
        
        return Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        
    }
}
