<?php

namespace EvonaApp\Http\Controllers\Admin;

use Config;
use Music;
use Storage;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class MusicController extends Controller
{

    public function index(){
        return view('admin.music.list')->with([
            'path' => config('settings.filePaths.music'),
            'music' => Music::all(),
        ]);
    }

    public function edit(Request $request){
        
        foreach($request->volume as $id => $volume){

            $music = Music::find($id);
            $music->volume = $volume;

            if ($request->hasFile('sound.'.$id)){
                
                $fileName = $request->sound[$id]->getClientOriginalName();
                
                if (Music::where('name',$fileName)->count() > 0){
                    $fileName = time().'_'.$fileName;
                }
                
                $request->sound[$id]->storeAs(config('settings.filePaths.music'),$fileName,'uploads');
                Storage::disk('uploads')->delete(config('settings.filePaths.music').'/'.$music->name);
                
                $music->name = $fileName;
                        
            }
            
            $music->save();
            
        }
        
        return back();
        
    }

}
