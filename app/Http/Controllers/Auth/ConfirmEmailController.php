<?php

namespace EvonaApp\Http\Controllers\Auth;

use Auth;
use User;
use Validator;
use Mail;
use MailClass;
use Config;
use HttpService;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class ConfirmEmailController extends Controller
{
    public function index(Request $request){
        
        if (Auth::check()){
            $user = Auth::user();

            if ($user->confirmedEmail){
                //return redirect()->route('confirmSmsIndex');
                return redirect()->route('purchases');
            }
        }
        
        if ($request->has('code') && $request->has('id')){
            
            if (!Auth::check()){
                
                $checkUser = User::where([
                    'id' => $request->id,
                    'emailCode' => $request->code,
                ])->get();
                
                if (!$checkUser->isEmpty()){
                    $user = $checkUser[0];
                    Auth::login($user, true);
                }else{
                    $user = new User();
                }
                
            }
            
            $errorMessage = '';
            
            if ($user->id != $request->id) {
                $errorMessage = 'Вы используете чужой код подтверждения.';
            }
            if ($user->emailCode != $request->code) {
                $errorMessage = 'Не верный код подтверждения. Сделайте новый запрос на подтверждение e-mail.';
            }

            if ($errorMessage){
                return view('auth.confirmEmailWait')->with([
                   'userEmail' => $user->email,
                   'emailDomain' => preg_replace('/^[^@]+@/Uis','',$user->email),
                   'errorMessage' => $errorMessage,
                ]);
            }
            
            $user->confirmedEmail = 1;
            $user->confirmEmailWait = 0;
            $user->emailCode = '';
            $user->save();
            
            HttpService::makeRequest('editCard',[
                'code1C' => $user->code1C,
            ],[
                'email' => $user->email,
            ],false);
            
            //return redirect()->route('confirmSmsIndex');
            return redirect()->route('purchases');
        }
        
        if (!Auth::check()){
            return redirect()->route('personalIndex');
        }
        
        if ($user->confirmEmailWait){
            
            $errorMessage = session('message');
            
            return view('auth.confirmEmailWait')->with([
                'userEmail' => $user->email,
                'emailDomain' => preg_replace('/^[^@]+@/Uis','',$user->email),
                'errorMessage' => $errorMessage,
            ]);
        }
        
        return view('auth.confirmEmail')->with([
           'userName' => $user->name,
        ]);
    }
    
    public function sendEmail(Request $request){
        
        $user = Auth::user();
        
        if ($request->has('again')){
            
            if (session()->has('lastTryConfirmEmail') && time() - session('lastTryConfirmEmail') < 7*60){
                $status = 'fail';
            }else{
                $user->sendConfirmMail();
                $status = 'okseven';
            }
            
            return ['status' => $status];
        }
        
        $validator = $this->validateEmail($request);
        
        if(!$validator->fails()){
            
            $checkEmail = HttpService::makeRequest('checkEmail',[
                'email' => $request->email,
            ]);
            
            if (!$checkEmail->errors()){
                $user->sendConfirmMail($request->email);
            }else{
                return back()->withErrors($checkEmail->errors('email'))->withInput();
            }
            
            return redirect()->route('confirmEmailIndex');
            
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }
    
    private function validateEmail($request){
        
        $errorMessages = [
            'email.email' => 'Вы ввели некорректный e-mail.',
        ];
         
        return Validator::make($request->all(), [
            'email' => 'email',
        ],$errorMessages);
        
    }
    
    public function errorConfirm(){
            
            if (session()->has('lastTryConfirmEmail')){
                if (time() - session('lastTryConfirmEmail') < 5*60){
                    $status = 'fail';
                    $message = 'too-soon-five';
                }else{
                    if (session()->has('lastErrorConfirm') && time() - session('lastErrorConfirm') < 5*60){
                        $message = 'too-soon-early';
                        $status = 'fail';
                    }else{
                        session(['lastErrorConfirm' => time()]);
                        
                        $mail = new MailClass('Не приходит уведомление на e-mail','errorConfirm',Auth::user()->toArray());
                        
                        Mail::to([
                            Config::get('settings.mails.dev'),
                            Config::get('settings.mails.director'),
                        ])->send($mail);
                        
                        $status = 'okfive';
                        $message = '';
                    }
                }
            }else{
                $message = 'too-soon-never';
                $status = 'fail';
            }
        
        return ['status' => $status, 'message' => $message];
        
    }
}
