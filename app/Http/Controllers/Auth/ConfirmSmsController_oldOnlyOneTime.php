<?php

namespace EvonaApp\Http\Controllers\Auth;

use Auth;
use Validator;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class ConfirmSmsController extends Controller
{
    public function index(){
        
        $user = Auth::user();
        
        if ($user->confirmedSms()){
            return redirect()->route('purchases');
        }
        
        if (!$user->smsCode){
            $this->sendSms();
        }else{
            dump($user->smsCode);
        }
        
        return view('auth.confirmSms')->with([
            'userName' => $user->name,
            'userPhone' => '+7'.$user->phone,
        ]);
        
    }
    
    public function checkSmsCode(Request $request){
        
        $validator = $this->validateSms($request);
        
        if (!$validator->fails()){
            
            $user = Auth::user();
            
            if ($request->smsCode == $user->smsCode){
                
                $user->smsCode = '';
                $user->confirmedSms = 1;
                $user->save();
                
            }else{
                $validator->errors()->add('smsCode', 'Введенный код не совпадает с кодом, отправленным по смс.');
                return back()->withErrors($validator)->withInput();
            }
            
            return redirect()->route('purchases');
            
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }

    private function sendSms(){
        
        $user = Auth::user();
        $user->smsCode = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $user->save();
        
        //1C request send sms
        dump($user->smsCode);
        
    }
    
    private function validateSms($request){
        
        $errorMessages = [
            'digits' => 'Код должен состоять из 4 цифр',
        ];
         
        return Validator::make($request->all(), [
            'smsCode' => 'digits:4',
        ],$errorMessages);
        
    }
}
