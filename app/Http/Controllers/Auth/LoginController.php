<?php

namespace EvonaApp\Http\Controllers\Auth;

use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

use Auth;
use Fun;
use Mail;
use Config;
use MailClass;
use Validator;
use HttpService;
use User;

class LoginController extends Controller
{
    public function index(){
        
        if (Auth::check()){
            return $this->afterAuth();
        }
        
        return view('auth.index');
    }
    
    public function login(Request $request){
        
        $validator = $this->validateLogin($request);
        
        if (!$validator->fails()){
                
                $checkUser = HttpService::makeRequest('auth',[
                    'cardNumber' => $request->cardNumber,
                    'phone' => $request->phone
                ]);
                
                if (!$checkUser->errors()){
                    return $this->userUpdate($checkUser);
                }else{
                    
                    if (isset($checkUser->data['phone'])){
                        session(['rightPhone' => $checkUser->data['phone']]);
                    }else{
                        session()->forget('rightPhone');
                    }
                        
                    return back()->withErrors($checkUser->errors('cardNumber'))->withInput();
                }
           
        }else{
            return back()->withErrors($validator)->withInput();
        }
    }

    private function afterAuth(){
        if (!Auth::user()->confirmedEmail){
            return redirect()->route('confirmEmailIndex');
        }else{
            //return redirect()->route('confirmSmsIndex');
            return redirect()->route('purchases');
        }
    }    
    
    private function userUpdate($checkUser){
        
        $userData = $checkUser->data;
        
        $existingUser = User::where('cardNumber', $userData['cardNumber'])->get();
        
        $user = !$existingUser->isEmpty() ? $existingUser[0] : new User();
        
        foreach($userData as $field => $value){
            if ($field == 'fio'){
                $fio = explode(' ',trim($userData['fio']));
                $user->name = isset($fio[1]) ? $fio[1] : '';
                $user->lastName = $fio[0];
                $user->secondName = isset($fio[2]) ? $fio[2] : '';
            }else{
                if ($field == 'email' && !$value && $user->email && $user->confirmEmailWait == 1){
                    
                }else{
                    $user->$field = trim($value);
                }
            }
        }

        if ($user->email && $userData['email']){
            $user->confirmedEmail = 1;
            $user->confirmEmailWait = 0;
        }

        $user->save();
        
        $checkUser->request->user_id = $user->id;
        $checkUser->request->save();

        if (Auth::attempt([
            'cardNumber' => $user->cardNumber,
            'password' => $user->phone,
        ])){
            return $this->afterAuth();
        }
                
                
    }
    
    public function logout(){
        Auth::logout();
        session()->forget('confirmedSms');
        // return redirect()->route('personal');
        return back();
    }
    
    private function validateLogin($request){
        
        $errorMessages = [
            'cardNumber.required' => 'Вы не ввели логин.',
            'cardNumber.digits' => 'Неверный логин - номер карты состоит из 6 цифр.',
            'phone.required' => 'Вы не ввели пароль.',
            'phone.digits' => 'Номер телефона состоит из 10 цифр - вводится без цифры 8 или значения +7.',
        ];
         
        return Validator::make($request->all(), [
            'cardNumber' => 'required|digits:6',
            'phone' => 'required|digits:10',
        ],$errorMessages);
        
    }
    
    public function errorNotify(Request $request){
        
        $validator = $this->validateMessage($request);
        
        if (!$validator->fails()){
            
            $message = '';
            $res = 'OK';
            
            if ($request->phone != $request->oldPhone && $request->phone == session('rightPhone')){
                $message = 'Добрый день. При вводе тел. номера, Вы неверно указали свой номер. Ниже информация отражает тел. номер, который Вы ввели при авторизации, и тот, который Вы указали в форме обратной связи. Повторите попытку авторизации на сайте, с вводом номера, который Вы указали в форме обратной связи. Именно этот номер телефона, Вы сообщили нам, при выдаче Вам дисконтной карты. Если он неверный, то Вы можете авторизоваться по этому номеру, а позже в магазине внести изменение.';
                $message.= '<p>Номер, указанный при авторизации: +7 ' . $request->oldPhone . '</p>';
                $message.= '<p>Номер, указанный в обратной связи: +7 ' . $request->phone . '</p>';
                $res = 'FAIL';
            }else{
                $checkDiff = Fun::checkPhoneNumbers(session('rightPhone'), $request->phone);
                if ($checkDiff <= 3){
                    $res = 'OK';
                }
            }
            
            if ($request->oldPhone != session('rightPhone') && $request->phone != session('rightPhone')){
                $message = 'Добрый день. При получении дисконтной карты Вы сообщили иной номер моб. телефона. Для авторизации на сайте, Вам нужно либо указать тот же номер телефона, который был внесен в нашу базу данных, который Вы сообщили при выдаче дисконтной карты, либо изменить информацию о своем номере мобильного телефона в нашей базе данных. Изменить информацию о номере моб. телефона можно только в магазине EVONA.';
                $res = 'FAIL';
            }
            
            if (!$message){
                $mail = new MailClass($request->subject, 'errorAuth',$request->all());

                Mail::to([
                    Config::get('settings.mails.dev'),
                    Config::get('settings.mails.director')
                ])->send($mail);
            }
            
            return [
                'RES' => $res,
                'MESSAGE' => $message,
            ];
            
        }else{
            
            return [
                'ERROR' => $validator->errors(),
            ];
            
        }
        
    }
    
     private function validateMessage($request){
        
        $errorMessages = [
            'cardNumber.required' => 'Укажите номер карты. Он должен состоять из 6 цифр.',
            'cardNumber.digits' => 'Укажите номер карты. Он должен состоять из 6 цифр.',
            'phone.required' => 'Вы не ввели номер телефона.',
            'phone.digits' => 'Номер телефона состоит из 10 цифр - вводится без цифры 8 или значения +7.',
            'name.regex' => 'Укажите Ф. И. О. полностью. Допустимые символы кириилица, тире и пробелы',
        ];
         
        return Validator::make($request->all(), [
            'cardNumber' => 'required|digits:6',
            'phone' => 'required|digits:10',
            'email' => 'required|email',
            'name' => 'required|regex:/[а-яА-ЯёЁ\-\s]/Uis',
        ],$errorMessages);
        
    }

}
