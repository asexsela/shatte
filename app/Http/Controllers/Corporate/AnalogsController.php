<?php

namespace EvonaApp\Http\Controllers\Corporate;

use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

use Auth;
use Color;
use CompetitorShop;
use Product;
use Fun;
use HttpService;
use Validator;
use Imagick;

class AnalogsController extends Controller
{
    private $forTree = [];
    private $inc = 0;
    
    function index($code = NULL){
        
        $user = Auth::user();
        
        $groups = HttpService::makeRequest('getProductsGroupsForAnalogs');
        foreach($groups->data as $group){
            $g = Product::find($group['id']);
            $g = $g ? $g : new Product();
            $g->fill($group)->save();
        }
        
        $data = HttpService::makeRequest('getCompetitorShops');
        foreach($data->data as $shop){
            $c = CompetitorShop::where('id',$shop['id'])->first();
            $c = $c ? $c->fill($shop) : new CompetitorShop($shop);
            $c->save();
        }
        
        $data = HttpService::makeRequest('getColors');
        foreach($data->data as $color){
            $c = Color::where('id',$color['id'])->first();
            $c = $c ? $c->fill($color) : new Color($color);
            $c->save();
        }
        
        $groups = Product::where('isGroup',1)->orderBy('name')->get();
        foreach($groups as $group){
            $this->forTree[$group->parent][] = $group->toArray();
        }
        
        $cs = Color::orderBy('name')->get();
        $colors = [];
        foreach($cs as $color){
            $colors[] = [
                'code1C' => Fun::idToCode1C($color->id, 9),
                'name' => $color->name
            ];
        }
        
        $tree = $this->getTree(0);
        
        $analogInfo = [];
        if ($code){
            $data = HttpService::makeRequest('getAnalogInfo',['code1C' => $code]);
            $analogInfo = $data->data;
            $analogInfo['photo'] = $analogInfo['photo'] ? 'data:image/jpeg;charset=utf-8;base64,'.$analogInfo['photo'] : '';
        }
        
        $data = HttpService::makeRequest('getBreak',['code1C' => $user->code1C]);
        $breaks = $data->data;
        
        return view('corporate.analogs')->with([
            'colors' => $colors,
            'shops' => CompetitorShop::where([
                ['type', '<>', 2],
                'city_id' => $user->city_id,
                'status' => 1,
            ])->orWhere('type',2)
            ->where([
                'author' => $user->code1C,
                'job' => 1,
            ])->orWhere('type',2)
            ->where([
                'job' => 0,
            ])->orderBy('name')->get(),
            'tree' => $tree,
            'types' => [
                0 => 'Идентичный',
                1 => 'Схожий',
                2 => 'Идентичный+Схожий',
            ],
            'analogInfo' => $analogInfo,
            'message' => false,
            'breaks' => $breaks,
        ]);
    }
    
    function getTree($parent,$depth = 0){
        $res = [];
        $this->inc++;
        foreach($this->forTree[$parent] as $branch){    
            $d = $depth;
            $arr = [
                'name' => $branch['name'],
                'depth' => $d,
                'inc' => $this->inc,
                'lowest' => $branch['lowest'],
                'code1C' => Fun::idToCode1C($branch['id'], 8),
                'class' => $branch['lowest'] ? 'lowest' : '',
            ];
            if (isset($this->forTree[$branch['id']])){
                $d++;
                $arr['children'] = $this->getTree($branch['id'],$d);
            }
            $res[] = $arr;
        }
        
        return $res;
    }
    
    function loadProducts(Request $request){
        
        $code1C = $request->code1C;
        $check = Product::where('parent',$code1C)->orderBy('updated1C','desc')->first();
        $lastTime = $check ? date('Y-m-d H:i:s',strtotime($check->updated1C) + 1) : '00010101';    
        
        $products = HttpService::makeRequest('getProductsForAnalogs',['code1C' => $code1C,'date' => date('YmdHis',strtotime($lastTime))]);
        
        foreach($products->data as $product){
            $p = Product::where('id',$product['id'])->first();
            $p = $p ? $p->fill([$product]) : new Product($product);
            $p->save();
        }
        
        $ps = Product::where('parent', $code1C)->orderBy('name')->get();
        $products = [];
        foreach($ps as $product){
            $products[] = [
                'name' => mb_strtoupper(mb_substr($product->name,0,1)).mb_substr($product->name,1),
                'photo' => $product->photo,
                'code1C' => Fun::idToCode1C($product->id, 8),
            ];
        }
        
        return view('corporate.analogsProducts')->with([
            'products' => $products,
        ])->render();
        
    }
    
    function sendAnalog(Request $request){
        
        $user = Auth::user();
        
        $validator = $this->validateAnalog($request);
        
        if (!$validator->fails() || !$request->detected && $request->shop){
            if($request->hasFile('photo')){
                $arr = explode('.',$request->photo->getClientOriginalName());
                $ext = $arr[count($arr) - 1];
                $fileName = time().'-'.$user->id.'.'.$ext;
                $request->photo->storeAs(config('settings.filePaths.analogs'),$fileName,'uploads');
                
                $uploaded_img = public_path().config('settings.filePaths.analogs').$fileName;
                $img = new Imagick($uploaded_img);
                $orientation = $img->getImageOrientation();
                $resave = true;
                switch($orientation) { 
                    case imagick::ORIENTATION_BOTTOMRIGHT: 
                        $img->rotateimage("#000", 180);
                    break; 
                    case imagick::ORIENTATION_RIGHTTOP: 
                        $img->rotateimage("#000", 90);
                    break; 
                    case imagick::ORIENTATION_LEFTBOTTOM: 
                        $img->rotateimage("#000", -90);
                    break;
                    default:
                        $resave = false;
                    break;
                }
                if ($resave){
                    $img->setImageOrientation(imagick::ORIENTATION_TOPLEFT);
                    $img->writeImage($uploaded_img);
                    $img->clear();
                    $img->destroy();
                }
                $fileUrl = config('settings.filePaths.analogs').$fileName;
            }

            $check = HttpService::makeRequest('sendAnalog',[],[
                'employee' => $user->code1C,
                'shop' => Fun::idToCode1C($request->shop,9),
                'type' => $request->type,
                'product' => $request->product_id,
                'price' => $request->price,
                'domain' => str_replace('http://','',route('index')),
                'photo' => $fileUrl ?? '',
                'photo_name' => $fileName ?? '',
                'url' => $request->url,
                'colors' => $request->colors,
                'comment' => $request->comment,
                'subdivision_id' => $user->subdivision_id,
                'detected' => $request->detected,
                'break' => $request->break,
            ]);
            
            if (!$check->errors()){
                return view('corporate.analogs')->with(['message' => true]);
            }else{
                return back()->withErrors($check->errors())->withInput();   
            }
            
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }
    
    private function validateAnalog($request){
        
        $errorMessages = [];
        
        return Validator::make($request->all(), [
            'url' => 'required_if:shop_type,2',
            'photo' => 'required_if:shop_type,0|required_if:shop_type,1',
            'product_id' => 'required',
            'colors' => 'required_if:type,0|required_if:type,2',
            'comment' => 'required_if:type,1|required_if:type,2',
            'type' => 'required',
            'shop' => 'required',
            'price' => 'required|numeric'
        ], $errorMessages);
    }
    
    function doclist(){
        $user = Auth::user();
        $data = HttpService::makeRequest('getAnalogsList',['code1C' => $user->code1C]);

        $analogs = $data->data;
        
        return view('corporate.analogsList')->with([
            'analogs' => $analogs,
        ])->render();
        
    }
    
}
