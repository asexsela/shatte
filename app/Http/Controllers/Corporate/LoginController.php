<?php

namespace EvonaApp\Http\Controllers\Corporate;

use Auth;
use Validator;
use HttpService;
use User;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function index(Request $request){
        
        if (Auth::check() && Auth::user()->isEmployee()){
            
            return view('corporate.indexAuth')->with([
                'user' => Auth::user()
            ]);
            
        }else{
            
            return view('corporate.index');
            
        }
    }
    
    public function login(Request $request){
        
        $validator = $this->validateLogin($request);
        
        if (!$validator->fails()){
            
            $checkUser = HttpService::makeRequest('authEmployee',['email' => $request->email]);
            
            if (!$checkUser->errors()){
                
                $userData = $checkUser->data;
                
                $checkUser = User::where([
                    'email' => $request->email,
                    'employee' => 1,
                ])->get();
                
                $user = $checkUser->isEmpty() ? new User() : $checkUser[0];
                
                if ($user->confirmedEmailCorporate){
                    
                    if (Auth::attempt(['email' => $request->email, 'password' => $request->password],true)){
                        
                        $user->fill($userData);
                        $fio = explode(' ',trim($userData['full_name']));
                        $user->fill([
                            'name' => $fio[1],
                            'lastName' => $fio[0],
                            'secondName' => $fio[2],
                        ]);
                        $user->save();
                        
                        return redirect()->route('corporate.index');
                    }else{
                        $validator->errors()->add('email', 'Ошибка авторизации, проверьте введенные данные или <a href="'.route('corporate.registration.index').'">зарегистрируйтесь</a>');
                        return back()->withErrors($validator)->withInput();
                    }
                }else{
                    $validator->errors()->add('email', 'Вы не подтвердили свою электронную почту.<br><br>Если вы не получили письмо со ссылкой активации, <a href="'.route('corporate.registration.index').'">зарегистрируйтесь</a> ещё раз.');
                    return back()->withErrors($validator)->withInput();
                }
                
            }else{
                return back()->withErrors($checkUser->errors('email'))->withInput();
            }
            
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }
    
    public function logout(){
        Auth::logout();
        return redirect()->route('corporate.index');
    }
    
    private function validateLogin($request){
        
        $errorMessages = [
            'password.min' => 'Пароль должен содержать не менее 8 символов'
        ];
        
        return Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required|min:8',
        ], $errorMessages);
    }
    
}
