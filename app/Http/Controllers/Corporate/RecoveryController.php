<?php

namespace EvonaApp\Http\Controllers\Corporate;

use User;
use Validator;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class RecoveryController extends Controller
{
    public function index(Request $request){
        
        if ($request->has('code') && $request->has('id')){
            
            $errorMessage = '';
            
            $user = User::where([
                'id' => $request->id,
                'emailCode' => $request->code,
                'employee' => 1,
            ])->first();
            
            if (!$user){

                $errorMessage = 'Ссылка устарела. Попробуйте восстановить пароль снова';
                
            }
            
            return view('corporate.recovery')->with([
                'errorMessage' => $errorMessage,
                'code' => $request->code,
                'id' => $request->id,
            ]);
        
        }else{
            return redirect()->route('corporate.index');
        }
    }
    
    public function recovery(Request $request){
        
        $validator = $this->validateNewPassword($request);
        
        if(!$validator->fails()){
            
            $user = User::where([
                'id' => $request->id,
                'emailCode' => $request->code,
                'employee' => 1,
            ])->first();
            
            if($user){

                if($request->password1 == $request->password2){

                    $user->password = bcrypt($request->password1);
                    $user->save();
                    
                    return view('corporate.index')->with([
                        'message' => 'Пароль успешно изменен',
                    ]);

                }else{
                   $validator->errors()->add('password2', 'Пароли не совпадают');
                   return back()->withErrors($validator)->withInput();
                }
            }else{
               return redirect()->route('corporate.remember.index');
            }
        }else{
            return back()->withErrors($validator)->withInput();
        }   
    }
    
    private function validateNewPassword($request){
        
        $errorMessages = [
            'password1.min' => 'Пароль должен содержать не менее 8 символов',
            'password2.min' => 'Пароль должен содержать не менее 8 символов'
        ];
        
        return Validator::make($request->all(),[
            'password1' => 'required|min:8',
            'password2' => 'required|min:8',
        ],$errorMessages);
        
    }
    
}
