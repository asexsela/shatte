<?php

namespace EvonaApp\Http\Controllers\Corporate;

use Auth;
use Validator;
use HttpService;
use User;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class RegistrationController extends Controller
{
        public function index(Request $request){
        
        if ($request->has('code') && $request->has('id')){

            $checkUser = User::where([
                'id' => $request->id,
                'emailCode' => $request->code,
                'employee' => 1,
            ])->get();
            
            $errorMessage = '';
            
            if (!$checkUser->isEmpty()){
                
                $user = $checkUser[0];
                $user->confirmedEmailCorporate = 1;
                $user->emailCode = '';
                $user->save();
                
                Auth::login($user, true);
                return redirect()->route('corporate.index');
                
            }else{
                
                $errorMessage = 'Ссылка подтверждения устарела. Попробуйте зарегистрироваться снова';
                $validator = Validator::make([],[]);
                $validator->errors()->add('email', $errorMessage);
                
                return redirect()->route('corporate.registration.index')->withErrors($validator);
            }
           
        }
        
        if (Auth::check() && Auth::user()->isEmployee()){
            return redirect()->route('corporate.index');
        }else{
            return view('corporate.registration');
        }
        
    }
    
    public function registration(Request $request){
        
        $validator = $this->validateRegistration($request);
        
        if (!$validator->fails()){
            
            if ($request->password1 == $request->password2){
                
                $checkUser = User::where([
                    'email' => $request->email,
                    'employee' => 1,
                ])->get();
                
                $user = $checkUser->isEmpty() ? new User() : $checkUser[0];
                
                if (!$user->confirmedEmailCorporate){
                    
                    $checkUser = HttpService::makeRequest('authEmployee',['email' => $request->email]);
                    
                    if (!$checkUser->errors()){
                        
                        $user->fill($checkUser->data);
                        $fio = explode(' ',trim($checkUser->data['full_name']));
                        $user->fill([
                            'name' => $fio[1],
                            'lastName' => $fio[0],
                            'secondName' => $fio[2],
                            'email' => $request->email,
                            'password' => bcrypt($request->password1),
                            'employee' => 1,
                        ]);
                        
                        $user->save();
                        $user->sendConfirmMailCorporate($request->password1);
                        
                        return view('corporate.registration')->with(['email' => $request->email]);

                    }else{
                        return back()->withErrors($checkUser->errors('email'))->withInput();
                    }
                    
                }else{
                    $validator->errors()->add('email', 'Пользователь с таким e-mail адресом уже зарегистрирован');
                    return back()->withErrors($validator)->withInput();
                }
            }else{
                $validator->errors()->add('password2', 'Пароли не совпадают');
                return back()->withErrors($validator)->withInput();
            }
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }
    
    private function validateRegistration($request){
        
        $errorMessages = [
            'password1.min' => 'Пароль должен содержать не менее 8 символов',
            'password2.min' => 'Пароль должен содержать не менее 8 символов'
        ];
        
        return Validator::make($request->all(),[
            'email' => 'required|email',
            'password1' => 'required|min:8',
            'password2' => 'required|min:8',
        ],$errorMessages);
        
    }
    
}
