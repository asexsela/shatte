<?php

namespace EvonaApp\Http\Controllers\Corporate;

use Regulation;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;
use HttpService;
class RegulationsController extends Controller
{
    private $forTree = [];
    private $inc = 0;
    
    function index($code = NULL){
        
        if ($code){
            $html = file_get_contents(public_path(config('settings.filePaths.regulations').$code.'/'.$code.'.html')); 
            $html = preg_replace(
            '/src="(image[0-9]+)"/',
            'src="'.config('settings.filePaths.regulations').$code."/$1.jpg\"",
            str_replace(array(
                    "padding:8px;overflow:hidden;",
                    "width:100%;",
                    "p{line-height:1.15;margin:0px;}",
            ),array(
                    "padding:1%;",
                    "width:98%;",
                    "p{line-height:1.15;margin:8px 0;}"
            ),$html));
            return $html;
        }
        
        $this->checkUpdates();
        
        $regulations = Regulation::orderBy('isGroup')->orderBy('name')->get();
        foreach($regulations as $regulation){
            $this->forTree[$regulation->parent][] = $regulation;
        }
        
        $tree = $this->getTree(0);      
        
        return view('corporate.regulations')->with([
            'tree' => $tree,
        ]);
    }
    
    function getTree($parent,$depth = 0){
        $res = [];
        $this->inc++;
        foreach($this->forTree[$parent] as $branch){
            $d = $depth;
            $arr = [
                'name' => $branch['name'],
                'updated1C' => $branch['updated1C'],
                'depth' => $d,
                'isGroup' => $branch['isGroup'],
                'class' => !$branch['isGroup'] ? 'regl_el' : '',
                'date' => date('d.m.Y',strtotime($branch['updated1C'])),
                'dateClass' => strtotime($branch['updated1C']) + 3600*24*10 > time() ? 'newdate' : '',
                'code' => $branch['uniq'],
                'inc' => $this->inc,
            ];
            if (isset($this->forTree[$branch['id']])){
                $d++;
                $arr['children'] = $this->getTree($branch['id'],$d);
            }
            $res[] = $arr;
        }
        
        return $res;
    }
    
    function load(Request $request){
        dump($request->all());
    }
    
    function checkUpdates(){
        $lastTime = Regulation::orderBy('updated1C','desc')->first()->updated1C;
        //check new 1C
        $regulations = HttpService::makeRequest('getRegulations',[date('YmdHis',strtotime($lastTime))])->data;
        
        foreach($regulations as $regulation){
            
            $checkRegulation = Regulation::find($regulation['id']);
            $reg = $checkRegulation ? $checkRegulation : new Regulation();
            
            $reg->fill($regulation)->save();
            
        }
        
    }
}
