<?php

namespace EvonaApp\Http\Controllers\Corporate;

use Validator;
use User;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class RememberController extends Controller
{
    public function index(Request $request){
        
        return view('corporate.remember');
        
    }
    
    public function remember(Request $request){
        
        $validator = Validator::make($request->all(),[
            'email' => 'required|email',
        ]);
        
        if (!$validator->fails()){
            
            $user = User::where('email',$request->email)->first();
            
            if (!$user){
                $validator->errors()->add('email','Пользователь с таким e-mail не найден.<br>Попробуйте <a style="color: red;" href="'.route('corporate.registration.index').'">зарегистрироваться</a>');
                return back()->withErrors($validator)->withInput();
            }else{
                
                $user->sendRememberLinkCorporate();
                
                return view('corporate.index')->with([
                    'message' => 'На указанный Вами адрес электронной почты<br> отправлено письмо с ссылкой для восстановления пароля.',
                ]);
                
            }
            
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }
}
