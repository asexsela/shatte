<?php

namespace EvonaApp\Http\Controllers;

use Illuminate\Http\Request;

class FunController extends Controller
{
    static function checkPlural($count, $words){
        $cases = [2, 0, 1, 1, 1, 2];
        return $words[($count%100>4&&$count%100<20)?2:$cases[min($count%10,5)]];
    }
    
    static function checkPhoneNumbers($phone1, $phone2){
        $str1Arr = str_split($phone1);
        $str2Arr = str_split($phone2);

        return array_diff_assoc($str1Arr, $str2Arr);
    }
    
    static function struc1C($arr){
        $is = [];
        $vs = [];
        foreach($arr as $i => $v){
            $is[] = $i;
            $vs[] = '"'.$v.'"';
        }
        
        $str = 'Струк = Новый Структура("'.implode(',',$is).'", '.implode(',',$vs).');';
        
        
        
        return $str;
    }
    
    static function soot1C($arr){
        $str = 'Соотв = Новый Соответствие;';
        
        foreach($arr as $i => $v){
            
            if (is_array($v)){
                foreach($v as $j => $k){
                    $str.= 'Соотв.Вставить("'.$i.'['.$j.']", "'.$k.'");';
                }
            }else{
                $str.= 'Соотв.Вставить("'.$i.'", "'.$v.'");';
            }
            
        }
        
        return $str;
    }
    
    static function idToCode1C($id,$length){
        $tmp = $id.'';
        while(strlen($tmp) < $length){
            $tmp = '0'.$tmp;
        }
        return $tmp;
    }
    
}
