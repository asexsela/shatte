<?php

namespace EvonaApp\Http\Controllers;

use Auth;
use Config;
use HttpRequest;
use Fun;
use Illuminate\Http\Request;

class HighLoadController extends Controller
{
    
    static function checkLimit($method){
        
        $limit = self::checkConfig('highloadLimitTimes',$method);
        $limitTime = self::checkConfig('highloadLimitTime',$method);
        
        if (Auth::check()){
            $history = Auth::user()->highLoadHistory($method);
        }else{
            $history = session()->has('highLoad.'.$method) ? session('highLoad.'.$method) : [];
        }
        
        if (count($history) >= $limit && time()-end($history) <= $limitTime){
            $timeLeft = $limitTime - (time() - $history[0]);
            return ['error'=>[
                Config::get('settings.commonError') => Config::get('settings.highLoadError').$timeLeft.' '.Fun::checkPlural($timeLeft, ['секунду','секунды','секунд']),
            ]];
        }else{
            return false;
        }
    }
    
    static function checkConfig($config, $method){
        return  Config::get('settings.'.$config.'.'.$method) ? 
                Config::get('settings.'.$config.'.'.$method) : 
                Config::get('settings.'.$config.'.default');
    }
    
    static function add($method, $url){
        
        $request = new HttpRequest([
            'method' => $method,
            'url' => $url,
            'ip' => $_SERVER['REMOTE_ADDR'],
        ]);
        
        if (Auth::check()){
            
            $user = Auth::user();
            
            $user->httpRequests()->save($request);
            
        }else{
            
            $limit = self::checkConfig('highloadLimitTimes',$method);
            
            $history = session()->has('highLoad.'.$method) ? session('highLoad.'.$method) : [];
            
            if (count($history) >= $limit){
                array_pop($history);
            }

            $request->save();
            
            array_unshift($history,time());
            session(['highLoad.'.$method => $history]);
            
        }
        
        return $request;
        
    }
}
