<?php
namespace EvonaApp\Http\Controllers;

use Timer;
use Validator;
use HighLoad;
use HttpRequest;
use Fun;
class HttpServiceController extends Controller
{
    const commonError = 'commonError';
    public $data;
    public $request;
    private $validator;
    protected $url, $username, $password;

    public function __construct($data, $request = false) {
        $this->data = $data;
        if(isset($data['error'])){
            $this->addErrors($data['error']);
        }
        if($request){
            $this->request = $request;
        }
    }
    
    public function addErrors($errors){
        if (!$this->validator){
            $this->validator = Validator::make([],[]);
        }
        foreach($errors as $field => $message){
            $this->validator->errors()->add($field,$message); 
        }
    }
    
    public function errors($field = false){
        if ($field && $this->validator){
            foreach($this->validator->errors()->toArray() as $key => $message){
                if ($key == self::commonError){
                    $this->validator->errors()->add($field,$message[0]);
                    break;
                }
            }
        }
        
        return $this->validator ? $this->validator->errors() : false;
    }
    
    static public function makeRequest($method,$params = [],$addParams = [],$checkLimit = true){
        
        if (1 == 2){
            echo '<pre>';
            echo Fun::struc1C($params)."\n";
            echo Fun::soot1C($addParams)."\n";
            echo 'ЗапросыССайта.Метод(Струк, Соотв);';
            echo '</pr>';
            exit;
        }
        
        // $data = $checkLimit ? HighLoad::checkLimit($method) : false;
        $data = false;
        
        $timeouts = [
            'getRegulations' => 999,
            'checkPoll' => 999,
            'getProductsForAnalogs' => 999,
        ];
        
        $timeout = isset($timeouts[$method]) ? $timeouts[$method] : 15;
        
        if (!$data){

            $url = '';
            $urlService = env('HTTP_SERVICE_URL').$method.'/';
            $username = env('HTTP_SERVICE_USERNAME');
            $password = env('HTTP_SERVICE_PASSWORD');

            if (count($params) > 0){
                foreach($params as $param){
                    $url.= rawurlencode($param).'/';
                }
            }

            if (count($addParams) > 0){
                $url.='?'.http_build_query($addParams);
            }
            
            $context = stream_context_create([
                'http' => [
                    'header'  => 'Authorization: Basic ' . base64_encode($username.':'.$password),
                    'timeout' => $timeout,
                ],
            ]);
            
            $request = HighLoad::add($method, $url);
            
            Timer::begin();

            $jsonData = file_get_contents($urlService.$url, false, $context);
            $data = json_decode($jsonData,true);
            
            $request->data = $jsonData;
            $request->duration = Timer::get_time();
            $request->save();

    
        }
        return new self($data, isset($request) ? $request : NULL);
        
    }
    
}
