<?php

namespace EvonaApp\Http\Controllers\Personal;

use Auth;
use Validator;
use HttpService;
use HighLoad;
use Sms;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class InfoController extends Controller
{
    public function index() {
        
        $user = Auth::user();
        
        return view('personal.info')->with([
            'user' => $user,
        ]);
        
    }
    
    public function indexSms() {
        
        $user = Auth::user();
        
        if (session()->has('changeInfoData')){
            
            if (!$user->smsCodeInfo){
                $this->sendSms();
            }
            
            return view('personal.infoConfirmSms')->with([
                'userName' => $user->name,
                'userPhone' => $user->phone, // session->data->phone
            ]);
            
        }else{
            redirect()->route('infoIndex');
        }
        
    }
    
    public function changeInfo(Request $request){

        $user = Auth::user();
        $validator = $this->validateInfo($request);
        
        if (!$validator->fails()){
            
            $checkLimit = HighLoad::checkLimit('editCard');
            
            if (!$checkLimit){
                
                if ($user->email != $request->email){
                    $checkEmail = HttpService::makeRequest('checkEmail',[
                        'email' => $request->email,
                    ]);

                    if ($checkEmail->errors()){
                        return back()->withErrors($checkEmail->errors('email'))->withInput();
                    }
                }
                
                if ($user->name != $request->name || $user->secondName != $request->secondName || $user->lastName != $request->lastName){
                    
                    session(['changeInfoData' => $request->all()]);
                    return redirect()->route('infoSmsIndex');
                    
                }else{
                    return $this->updateInfo($request->all());
                }
            }else{
                $hs = new HttpService($checkLimit);
                return back()->withErrors($hs->errors('lastName'))->withInput();
            }   
        }else{
            return back()->withErrors($validator)->withInput();
        }
    }
    
    public function validateInfo($request){
        
        $errorMessages = [
            'name.regex' => 'Имя введено некорректно, допустимые символы: кириллица и тире.',
            'lastName.regex' => 'Фамилия введена некорректно, допустимые символы: кириллица и тире.',
            'secondName.regex' => 'Отчество введено некорректно, допустимые символы: кириллица и тире.',
            'phone.digits' => 'Номер телефона состоит из 10 цифр. Номер телефона вводится без цифры 8 или значения +7',
        ];
         
        return Validator::make($request->all(), [
            'name' => 'required|regex:/[а-яА-ЯёЁ\-]/Uis',
            'lastName' => 'required|regex:/[а-яА-ЯёЁ\-]/Uis',
            //'phone' => 'required|digits:10',
            'email' => 'required|email',
        ],$errorMessages);
        
    }
    
    private function updateInfo($data){
        
        $checkUpdate = Auth::user()->updateInfo($data);
        if ($checkUpdate){
            if (!$checkUpdate->errors()){
                return redirect()->route('infoIndex');
            }else{
                return back()->withErrors($checkUpdate->errors('lastName'))->withInput();
            }
        }else{
            return back()->withInput();
        }
        
    }
    
    public function checkSmsCode(Request $request){
        
        $user = Auth::user();
        
        if ($request->has('cancelSms')){
            
            session()->forget('changeInfoData');
            
            //Cron clear codes midnight!
//            $user->smsCodeInfo = '';
//            $user->save();
            
            return redirect()->route('infoIndex');
        
            
        }
        
        $validator = $this->validateSms($request);
        
        if (!$validator->fails()){
            
            
            if ($request->smsCodeInfo == $user->smsCodeInfo){
                
                $data = session('changeInfoData');
                return $this->updateInfo($data);
                
            }else{
                $validator->errors()->add('smsCodeInfo', 'Введенный код не совпадает с кодом, отправленным по смс.');
                return back()->withErrors($validator)->withInput();
            }
            
            return redirect()->route('purchases');
            
        }else{
            return back()->withErrors($validator)->withInput();
        }
        
    }

    private function sendSms(){
        
        $user = Auth::user();
        $user->smsCodeInfo = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $user->save();
        
        Sms::send($user->code1C, 'Ваш код: '.$user->smsCodeInfo);
        
    }
    
    private function validateSms($request){
        
        $errorMessages = [
            'digits' => 'Код должен состоять из 4 цифр',
        ];
         
        return Validator::make($request->all(), [
            'smsCodeInfo' => 'digits:4',
        ],$errorMessages);
        
    }
    
}
