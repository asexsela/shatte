<?php

namespace EvonaApp\Http\Controllers\Personal;

use Auth;
use Poll;
use Fun;
use PollProcess;
use PollQuestion;
use PollResult;
use PollAnswer;
use PollEmployee;
use HttpService;
use Validator;
use Sms;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class PollController extends Controller
{
    public function check(){
        
        $user = Auth::user();
        
        $process = $user->pollsProcesses()->where([
            'status' => 0,
        ])->first();
        
        if ($process){
            
            if ($process->waitSms){
                
                return [
                    'ok' => 1,
                    'view' => $this->waitSms($process->poll),
                ];
                
            }
            
            if ($process->question){
               
                $view = $process->question->type == 1 ?
                        $this->getQuestionSelector($process->question) :
                        $this->getQuestion($process->question,$process);    
                
                return [
                    'ok' => 1,
                    'view' => $view,
                ];
                
            }
            
                
        }else{
            
            $r = HttpService::makeRequest('checkPoll',['code1C' => $user->code1C]);
            $data = $r->data['poll'];
            
            if ($data){
                
                $poll = Poll::where('code1C',$data['code1C'])->first();
                
                if (!$poll){

                    $poll = new Poll();

                    $poll->code1C = $data['code1C'];
                    $poll->discount = $data['discount'];
                    $poll->days = $data['days'];
                    $poll->messageBefore = $data['messageBefore'];
                    $poll->messageAfter = $data['messageAfter'];
                    $poll->save();

                    foreach($data['questions'] as $question){

                        $pollQuestion = new PollQuestion([
                            'type' => $question['type'],
                            'list' => $question['list'],
                            'index' => $question['index'],
                            'name' => $question['name'],
                        ]);

                        $poll->questions()->save($pollQuestion);

                        foreach($question['answers'] as $answer){
                            $pollQuestion->answers()->save(new PollAnswer($answer));
                        }
                    }   

                }else{
//                    $checkIgnored = $user->pollsResults()->where([
//                        'poll_id' => $poll->id,
//                        'ignored' => 1,
//                    ])->first();
//
//                    if ($checkIgnored){
//                        return [
//                            'ok' => 0,
//                        ];
//                    }
                    
//                    $checkFinish = $user->pollsProcesses()->where([
//                        'status' => 1,
//                        'poll_id' => $poll->id,
//                    ])->first();
//                    
//                    if ($checkFinish){
//                        return [
//                            'ok' => 0,
//                        ];
//                    }
                    
                }

                if (isset($data['employees'])){
                    $for_json = [];
                    foreach($data['employees'] as $employee){
                        $checkEmployee = PollEmployee::where('code1C',$employee['code1C'])->first();
                        $pollEmployee = $checkEmployee ? $checkEmployee : new PollEmployee();
                        $pollEmployee->photo = $employee['code1C'].'.jpg';
                        $pollEmployee->fill($employee)->save();
                        $for_json[] = $pollEmployee->code1C;
                    }
                    
                    $process = new PollProcess([
                        'user_id' => Auth::user()->id,
                        'poll_id' => $poll->id,
                        'employees' => json_encode($for_json),
                    ]);

                    $process->save();
                }
            }
        }
        
        if ($process){    
            return [
                'ok' => 1,
                'id' => $process->poll_id,
                'helloUrl' => route('poll.hello').'/',
            ];
        }
        
    }
    
    public function hello(Request $request){
        
        $poll = Poll::find($request->id);
        
        return view('personal.poll.hello')->with([
            'poll' => $poll,
        ]);
        
    }
    
    public function start(Request $request){
        
        $poll = Poll::find($request->id);
        
        if ($request->has('old')){
            return $this->next($request);
        }else{
            $question = $poll->questions()->first();
        }
          
        if ($question->type == 0){
            return $this->next($request);
        }
        
        $process = $poll->currentProcess();
        $process->fill([
            'status' => 0,
            'currentQuestion' => $question->id,
        ])->save;
        
        $poll->processes()->save($process);
        
        return $this->getQuestionSelector($question);
        
    }
    
    public function next(Request $request){
        
        $user = Auth::user();
        
        if (!$request->has('answer') && $request->old){
            return ['error' => 'Необходимо выбрать ответ'];
        }
        
        $poll = Poll::find($request->id);
        $question = $poll->getNextQuestion($request->list,$request->old);
        
        $process = $poll->currentProcess();
        
        if ($request->old){
            
            if ($request->old == -1){
                $process->currentEmployee = $request->answer;
            }else{
                $result = new PollResult([
                    'poll_question_id' => $request->old,
                    'poll_answer_id' => $request->answer,
                    'poll_process_id' => $process->id,
                    'poll_id' => $poll->id,
                ]);

                $user->pollsResults()->save($result);
            }
        }
        
        if ($question){
            
            if ($question->list == 1 && !$request->old){
                $question = false;
            }else{
                $process->currentQuestion = $question->id;
            }
            
            $process->save();
        
            return $this->getQuestion($question,$process);
            
        }else{
            
            $process->waitSms = 1;
            
            $user->smsCodeInfo = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
            $user->save();
        
            Sms::send($user->code1C, 'Ваш код: '.$user->smsCodeInfo);
            
            $process->save();
            
            return $this->waitSms($poll);
            
        }
            
    }
    
    private function waitSms($poll){
        return view('personal.poll.sms')->with([
                    'poll' => $poll,
                    'user' => Auth::user(),
                ])->render();
    }
    
    public function finishPoll(Request $request){
        
        $validator = Validator::make($request->all(),[
            'smsCode' => 'required|digits:4'
        ],[
            'digits' => 'Код должен состоять из 4 цифр'
        ]);
        
        if ($validator->fails()){
            return ['error' => $validator->errors()->first()];
        }
        
        $user = Auth::user();
        
        if ($request->smsCode != $user->smsCodeInfo){
            return ['error' => 'Введенный код не совпадает с кодом, отправленным по смс.'];
        }else{
            
            $poll = Poll::find($request->id);
            
            $process = $poll->currentProcess();
            
            $process->status = 1;
            $process->waitSms = 0;
            $process->timePassed = date('Y-m-d H:i:s');
            $process->save();

            $this->finish($poll);

            return [
                'finish' => view('personal.poll.finish')->with([
                    'poll' => $poll,
                ])->render(),
            ];
        }
        
        
        
        
    }
    
    private function finish($poll){
        
        $user = Auth::user();
        
        $process = $poll->processes()->where('user_id', $user->id)->orderBy('created_at', 'desc')->first();
        
        $results = $user->pollsResults()->with('question','answer')->where([
            'poll_id' => $poll->id,
            'poll_process_id' => $process->id,
        ])->orderBy('created_at', 'desc')->get();
         
        $response = [];
        
        $inc = 0;
        foreach($results as $result){
            $inc++;
            if ($inc == 1){
                $response['ignored'] = $result->ignored;
                if (count($result->question) > 0){
                    $response['list'] = $result->question->list;
                }
            }
            if ($response['ignored'] == 0){
                $response['question_'.$result->question->index] = $result->answer->index;
            }
        }

        if (isset($response['list']) && $response['list'] == 1){

            $response['employee'] = $process->employee->code1C;

            $employees = json_decode($process->employees,true);

            $inc = 0;
            foreach($employees as $employee){
                $inc++;
                $response['employee_'.$inc] = $employee;
            }
        }
        
        HttpService::makeRequest('setPoll',['code1C' => $user->code1C],$response);
    }
    
    private function getQuestion($question,$process){
        
        if (!$question && $process){
            
            $employees = PollEmployee::whereIn('code1C', json_decode($process->employees, true))->get()->toArray();
            foreach($employees as $i => $employee){
                $employees[$i]['name'] = $employee['name'];
                $employees[$i]['photo'] = config('settings.filePaths.employee').$employee['photo'];
            }
            
            return view('personal.poll.questionEmployee')->with([
                'employees' => $employees,
                'id' => $process->poll_id,
            ])->render();
        }
        
        if ($process->currentEmployee){
            $employeeName = $process->employee->name;
            $question->name = str_replace('[Имя]',$employeeName,$question->name);
        }
        
        return view('personal.poll.question')->with([
            'question' => $question,
            'answers' => $question->answers,
        ])->render();
        
    }
    
    private function getQuestionSelector($question) {
        
        return view('personal.poll.questionSelector')->with([
            'question' => $question,
            'answers' => $question->answers,
        ])->render();
        
    }
    
    public function ignore(Request $request){
        
        $poll = Poll::find($request->id);
        $process = $poll->currentProcess();
        
        Auth::user()->pollsResults()->save(new PollResult([
            'poll_id' => $request->id,
            'poll_process_id' => $process->id,
            'ignored' => 1,
        ]));
        

        $process->status = 1;
        $process->waitSms = 1;
        $process->save();
        
        $this->finish($poll);
        
    }
}