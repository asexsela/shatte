<?php

namespace EvonaApp\Http\Controllers\Personal;

use Auth;
use HttpService;
use Purchase;
use Timer;
use Validator;
use Mail;
use Config;
use MailClass;
use Illuminate\Http\Request;
use EvonaApp\Http\Controllers\Controller;

class PurchasesController extends Controller
{
    public function index(){
        
        $user = Auth::user();
        $checkPurchases = HttpService::makeRequest('getPurchases',['code1C' => $user->code1C]);
        if (!$checkPurchases->errors()){
            $purchases = $checkPurchases->data;
            $user->discount = $purchases['userInfo']['discount'];
            $user->save();

            if ($purchases['purchases'] > 0){
                $user->purchases()->delete();
                foreach($purchases['purchases'] as $purchase){
                    $user->purchases()->save(new Purchase($purchase));
                }
            }
        }
        
        $totalPrice = $user->purchases->sum('finalPrice');
        
        $divider=89;
        $linePos=(int)$totalPrice/$divider;
        if ($linePos >= 616) $linePos=616;
        
        //подгрузить пороги из 1С
        $next_d = '';
        $rest = 0;
        if($totalPrice<10000){$next_d="15%";$rest=10000-$totalPrice;}
        elseif($totalPrice<25000){$next_d="20%";$rest=25000-$totalPrice;}
        elseif($totalPrice<45000){$next_d="25%";$rest=45000-$totalPrice;}
        $repl='Для увеличения скидки до '.$next_d.' Вам осталось совершить покупок всего на <b>'.number_format($rest, 0,'',' ').'</b> <span class="b-rub">Р</span>';
        if($totalPrice>=45000){$repl='';}
        
        $process = $user->pollsProcesses()->orderBy('timePassed','desc')->first();
        $pollMessage = '';
        if ($process && $process->status == 1){/// + проверка по дате
            
            $checkNotUseDiscount = HttpService::makeRequest('checkPollDiscount',['code1C' => $user->code1C]);
            
            if ($checkNotUseDiscount->data['res'] == 'ok'){
                $dateEndDiscount = date('d.m.Y',strtotime($checkNotUseDiscount->data['time']));
                $pollMessage = 'Спасибо за участие в опросе. Ваша скидка по дисконтной карте на следующую покупку (единоразово) будет увеличена на '.$process->poll->discount.'% до '.$dateEndDiscount;
            }
            
        }
        
        return view('personal.purchases')->with([
            'purchases' => $user->purchases,
            'totalPrice' => $totalPrice,
            'linePos' => $linePos,
            'repl' => $repl,
            'pollMessage' => $pollMessage,
            'checkPurchases' => $checkPurchases,
        ]);
    }
    
    public function errorPurchases(Request $request){

        $validator = $this->validateError($request);
        $error = '';
        
        // if(!$validator->fails()){
            
            // if (session()->has('lastErrorCommon') && time() - session('lastErrorCommon') <= 5*60){
            //     $status = 'fail';
            //     $error = 'Нельзя отправлять сообщения чаще, чем один раз в пять минут';
            // }else{
                session(['lastErrorCommon' => time()]);
                $status = 'ok';

                $mail = new MailClass('Ошибка на сайте','errorCommon',array_merge(['comment' => $request->comment],Auth::user()->toArray()));

                Mail::to([
                    Config::get('settings.mails.dev')
                    // Config::get('settings.mails.director'),
                ])->send($mail);
            // }
        // }else{
        //     $error = $validator->errors()->first('comment');
        //     $status = 'fail';
        // }
        
        return ['status' => $status,'error' => $error];
        
    }
    
    private function validateError($request){
         
        return Validator::make($request->all(), [
            'comment' => 'required',
        ]);
        
    }
}
