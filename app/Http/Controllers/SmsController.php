<?php

namespace EvonaApp\Http\Controllers;

use Config;
use HttpService;
use Illuminate\Http\Request;

class SmsController extends Controller
{
    public static function send($code1C, $message){
        
        $params = Config::get('app.debug') ? ['phone' => Config::get('settings.testSms')] : [];
        
        HttpService::makeRequest('sendSms',[
            'code1C' => $code1C,
            'message' => $message
        ],$params);
    }
}
