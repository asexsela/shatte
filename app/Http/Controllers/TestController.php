<?php

namespace EvonaApp\Http\Controllers;

use HttpService;
use Vacancy;
use Auth;
use User;
use MailClass;
use Config;
use Mail;
use DB;
use Poll;
use PollQuestion;
use PollAnswer;
use PollEmployee;
use PollProcess;
use Timer;
use Sms;
use Fun;
use Regulation;
use Color;
use CompetitorShop;
use Product;
use Imagick;
use Illuminate\Http\Request;

class TestController extends Controller
{
    
    public $tree = [];
    
    public function index(){
        
        session(['test' => 123]);
        
    }
    
    public function method($method){
        DB::connection()->enableQueryLog();
        return $this->$method();
    }
    
    private function session(){
        dump(session()->all());
    }
    
    private function server(){
        dump($_SERVER);
    }
    
    private function request(){
        dump(Request::url());
    }
    
    private function import_poll(){
        Timer::begin();
        
        $r = HttpService::makeRequest('checkPoll',['code1C' => '123']);
        
        $data = $r->data['poll'];
        
        $checkPoll = Poll::where('code1C',$data['code1C'])->get();
        
        if ($checkPoll->isEmpty()){  
            
            $poll = new Poll();
            
            $poll->code1C = $data['code1C'];
            $poll->discount = $data['discount'];
            $poll->days = $data['days'];
            $poll->save();
                  
            foreach($data['questions'] as $question){

                $pollQuestion = new PollQuestion([
                    'type' => $question['type'],
                    'list' => $question['list'],
                    'index' => $question['index'],
                    'name' => $question['name'],
                ]);
                
                $poll->questions()->save($pollQuestion);
                
                foreach($question['answers'] as $answer){
                    $pollQuestion->answers()->save(new PollAnswer($answer));
                }
            }
            
            if (isset($data['employees'])){
                $for_json = [];
                foreach($data['employees'] as $employee){
                    $checkEmployee = PollEmployee::where('code1C',$employee['code1C'])->first();
                    $pollEmployee = $checkEmployee ? $checkEmployee : new PollEmployee();
                    $pollEmployee->photo = $employee['code1C'].'.jpg';
                    $pollEmployee->fill($employee)->save();
                    $for_json[] = $pollEmployee->code1C;
                }
            }
            
            $process = new PollProcess([
                'user_id' => Auth::user()->id,
                'poll_id' => $poll->id,
                'employees' => json_encode($for_json),
            ]);
            
            $process->save();
            
        }
        
        dump(Timer::get_time());
    }
    
    private function import_employees(){
        $r = HttpService::makeRequest('checkPoll',['code1C' => '123']);
        
        $data = $r->data['poll'];
        
        $poll = Poll::first();
        
        if (isset($data['employees'])){
                $for_json = [];
                foreach($data['employees'] as $employee){
                    $checkEmployee = PollEmployee::where('code1C',$employee['code1C'])->first();
                    $pollEmployee = $checkEmployee ? $checkEmployee : new PollEmployee();
                    $pollEmployee->photo = $employee['code1C'].'.jpg';
                    $pollEmployee->fill($employee)->save();
                    $for_json[] = $pollEmployee->code1C;
                }
            }
            
            $process = new PollProcess([
                'user_id' => Auth::user()->id,
                'poll_id' => $poll->id,
                'employees' => json_encode($for_json),
            ]);
            
            $process->save();
            
            dump($process);
        
    }


    private function check_question(){
        $poll = Poll::find(1);
        
        dump($poll->questions[0]->answers);
    }
    
    private function relations(){
        //$res = Auth::user()->results()->where('poll_')
        //dump($res);
        
        $user = Auth::user();
        
        $poll = Poll::find(3);
        $process = $user->pollsProcesses()->where([
            'status' => 1,
            'poll_id' => $poll->id,
        ])->first();
        
        $results = $user->pollsResults()->with('question','answer')->where('poll_id',$poll->id)->get();
        
        
        
        $inc = 0;
        foreach($results as $result){
            $inc++;
            if ($inc == 1){
                $response['list'] = $result->question->list;
                $response['ignored'] = $result->ignored;
            }
            
            $response['question_'.$result->question->index] = $result->answer->index;
        }
        
        if ($response['list'] == 1){
            
        
        
            $response = [
                'employee' => $process->employee->code1C,
            ];
            $employees = json_decode($process->employees,true);
            $inc = 0;
            foreach($employees as $employee){
                $inc++;
                $response['employee_'.$inc] = $employee;
            }
        }
        //HttpService::makeRequest('setPoll',['code1C' => $user->code1C],$response);
        echo Fun::arr1C($response);
        
    }
    
    function process_employee(){
        $process = PollProcess::find(1);
        dump($process->employee);
    }
        
    
    function import_regulations(){
        $regulations = HttpService::makeRequest('getRegulations',['0']);

        Regulation::truncate();
        foreach($regulations->data as $data){
            $r = new Regulation($data);
            $r->save();
        }
        
        dump(Regulation::all());
        
    }
    
    function get_tree($children,$parent = 0){
        foreach($children as $child){
            $this->tree[$child['code1C']] = [
                'parent' => $parent,
                'name' => $child['name'],
            ];
            if (isset($child['children'])){
                $this->get_tree($child['children'],$child['code1C']);
            }
        }
    }
    
    function tree2(){
        $reg = Regulation::find(1);
        
        $children = $reg->children;
        foreach($children as $child){
            dump($child->parent);
        }
    }
    
    function current_process(){
        $poll = Poll::find(3);
        
        dump($poll->currentProcess()->toArray());
    }
    
    function remove_poll(){
        $poll = Poll::find(4);
        $poll->removeFull();
    }
    function finish_poll(){
        $poll_c = new Personal\PollController();
        $poll = Poll::find(1);
        $poll_c->finish($poll);
    }
    
    function get_colors(){
        $data = HttpService::makeRequest('getColors');
        foreach($data->data as $color){
            $c = Color::where('id',$color['id'])->first();
            $c = $c ? $c->fill([$data]) : new Color($color);
            $c->save();
        }
    }
    
    function get_competitor_shops(){
        $data = HttpService::makeRequest('getCompetitorShops');
        foreach($data->data as $shop){
            $c = CompetitorShop::where('id',$shop['id'])->first();
            $c = $c ? $c->fill([$shop]) : new CompetitorShop($shop);
            $c->save();
        }
    }
    
    function get_groups(){
        $groups = HttpService::makeRequest('getProductsGroupsForAnalogs');
        foreach($groups->data as $group){
            $g = Product::find($group['id']);
            $g = $g ? $g : new Product();
            $g->fill($group)->save();
        }
        
    }
    
    function get_products(){
        $products = HttpService::makeRequest('getProductsForAnalogs',['code1C' => Fun::idToCode1C(21842, 8)]);
        Product::truncate();
        foreach($products->data as $data){
            $p = new Product($data);
            $p->save();
        }
        
        dump(Product::all());
    }
   
    function products_truncate(){
        dd(Product::truncate());
    }
    
    function timestamp(){
        dump(time());
    }
    
    function product_check(){
        $code1C = '00021842';
        $check = Product::where('parent',$code1C)->orderBy('updated1C','desc')->first();//->updated1C;
        $lastTime = $check ? $check->updated1C : 0;
        dd($lastTime);
    }
    
    function auth(){
        //Auth::loginUsingId(1);
    }
    
    function check_exif(){
        $path = str_replace('test.','',public_path().config('settings.filePaths.analogs').'1522332560-11218.jpg');
        $img = new Imagick($path);
        $orientation = $img->getImageOrientation();
        switch($orientation) { 
            case imagick::ORIENTATION_BOTTOMRIGHT: 
                $img->rotateimage("#000", 180);
            break; 
            case imagick::ORIENTATION_RIGHTTOP: 
                $img->rotateimage("#000", 90);
            break; 
            case imagick::ORIENTATION_LEFTBOTTOM: 
                $img->rotateimage("#000", -90);
            break; 
        }
    }
    
    function check_csh(){
        $c = \EvonaApp\CompetitorShop::first();
        dump($c);
        $c->fill(['type' => 0]);
        $c->save();
        
    }
    
     function auth_jiroler(){
         Auth::loginUsingId('1926');
     }
   
}