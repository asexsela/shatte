<?php

namespace EvonaApp\Http\Controllers;

use Illuminate\Http\Request;

class TimerController extends Controller
{
    private static $start_time;
     
    static function begin(){
        self::$start_time = microtime(true);
    }
     
    static function get_time(){
        return number_format(microtime(true) - self::$start_time,10);
    }
}
