<?php

namespace EvonaApp\Http\Controllers;

use Validator;
use Vacancy;
use Illuminate\Http\Request;

class VacancyController extends Controller
{
    public function index(){
        return view('vacancy');
    }

    public function formHandler(Request $request){

        $validator = $this->validateInfo($request);

        if (!$validator->fails()){

            $data = $request->all();
            
            $types = ['1355939' => 'Продавец-консультант','1355940' => 'Директор магазина'];
            $times = ['now' => 'Работа нужна срочно', 'look' => 'Рассматриваю предложения'];
            $cities = [ '61' => 'Ярославль',
                        '62' => 'Иваново',
                        '63' => 'Владимир',
                        '64' => 'Калининград',
                        '65' => 'Вологда',
                        '66' => 'Кострома',
                        '68' => 'Мурманск',
                        '69' => 'Сыктывкар', 
                        '70' => 'Рязань',
                        '225905' => 'Петрозаводск',
                        '225906' => 'Пенза',
                        '225907' => 'Архангельск',
                        '250664' => 'Смоленск',
                        '1232981' => 'Тверь'];

            
            $data['city'] = $cities[$data['city']];
            $data['type'] = $types[$data['type']];
            $data['time'] = $times[$data['time']];
            
            $vacancy = Vacancy::create($data);

            $vacancy->sendMail();

            return [
                'RESULT' => 'OK',
            ];
        }else{
            return [
                'ERROR' => $validator->errors(),
            ];
        }

    }

    private function validateInfo($request){
        $errorMessages = [
            'name.regex' => 'Имя введено некорректно, допустимые символы: кириллица и тире.123',
            'lastName.regex' => 'Фамилия введена некорректно, допустимые символы: кириллица и тире.',
            'secondName.regex' => 'Отчество введено некорректно, допустимые символы: кириллица и тире.',
            'phone.digits' => 'Номер телефона состоит из 10 цифр. Номер телефона вводится без цифры 8 или значения +7',
        ];

        return Validator::make($request->all(), [
            'name' => 'required|regex:/[а-яА-ЯёЁ\-]/Uis',
            'lastName' => 'required|regex:/[а-яА-ЯёЁ\-]/Uis',
            'phone' => 'required|digits:10',
            'email' => 'required|email',
            'photo' => 'required',
        ],$errorMessages);
    }
}
