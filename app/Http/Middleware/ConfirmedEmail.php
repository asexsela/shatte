<?php namespace EvonaApp\Http\Middleware;

use Closure;
use Auth;

class ConfirmedEmail {

    public function handle($request, Closure $next)
    {
        
        if ( Auth::check() && Auth::user()->confirmedEmail)
        {
            return $next($request);
        }

        return redirect()->route('confirmEmailIndex');

    }

}