<?php

namespace EvonaApp\Http\Middleware;

use Closure;
use Auth;

class ConfirmedSms
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check() && Auth::user()->confirmedEmail() && Auth::user()->confirmedSms())
        {
            return $next($request);
        }

        return redirect()->route('confirmSmsIndex');
    }
}
