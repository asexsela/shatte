<?php

namespace EvonaApp\Http\ViewComposers;

use Route;
use Illuminate\Contracts\View\View;

class MenuAdminComposer {

    public function compose(View $view){
        
        $view->with([
            'menu' => config('settings.adminMenu'),
            'currentRoute' => Route::currentRouteName(),
        ]);
       
    }

    public function create($configs){

    }

}