<?php

namespace EvonaApp\Http\ViewComposers;

use Music;
use Illuminate\Contracts\View\View;

class MusicComposer {

    public function compose(View $view){
        
        $view->with([
            'path' => config('settings.filePaths.music'),
            'music' => Music::all(),
        ]);
       
    }

    public function create($configs){

    }

}