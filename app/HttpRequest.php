<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class HttpRequest extends Model
{
    protected $fillable = ['method','time','duration','url','ip','data'];
    
    public function user(){
        return $this->belongsTo('EvonaApp\User');
    }
}
