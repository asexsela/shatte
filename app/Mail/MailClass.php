<?php

namespace EvonaApp\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailClass extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $subject;
    public $view;
    public $viewPlain;

    public function __construct($subject,$view,$data)
    {
        $this->subject = $subject;
        $this->view = 'mail.'.$view;
        $this->viewPlain = $this->view.'Plain';
        $this->data = $data;
    }

    public function build()
    {
        $return = $this->subject($this->subject)->view($this->view);
        
        if (view()->exists($this->viewPlain)){
            $return->text($this->viewPlain);
        }
        
        return $return;
    }
}
