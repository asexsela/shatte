<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $fillable = ['name','volume'];
    
}
