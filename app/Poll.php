<?php

namespace EvonaApp;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $fillable = ['code1C','discount','days','messageBefore','messageAfter'];
    
    public function questions(){
        return $this->hasMany('PollQuestion')->orderBy('index');
    }
    
    public function processes(){
        return $this->hasMany('PollProcess');
    }
    
    public function results(){
        return $this->hasMany('PollResult');
    }
    
    public function getNextQuestion($list, $old = NULL){
        
        $question = $this->questions()->where('list',$list)->limit(1);
        if ($old){
            $question->where('id','>',$old);
        }
        
        $res = $question->get();
        
        return $res->isEmpty() ? false : $res[0];
        
    }
    
    public function currentProcess(){
        
        $processes = $this->processes()->where([
            'user_id' => Auth::user()->id,
            'status' => 0,
        ])->get();
        
        return $processes->isEmpty() ? false : $processes[0];
        
    }
    
    public function removeFull(){
        foreach($this->questions as $question){
            $question->answers()->delete();
        }
        $this->questions()->delete();
        $this->processes()->delete();
        $this->results()->delete();
        
        $this->delete();
        
    }
}
