<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class PollAnswer extends Model
{
    protected $fillable = ['name','rate','index'];
    
    public function question(){
        return $this->belongsTo('PollQuestion');
    }
    
    public function results(){
        return $this->hasMany('PollResult');
    }
}
