<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class PollEmployee extends Model
{
    protected $fillable = ['code1C','name','photo'];
    
}
