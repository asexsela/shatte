<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class PollProcess extends Model
{
    protected $fillable = ['user_id', 'poll_id', 'list', 'status', 'currentQuestion', 'currentEmployee', 'employees', 'waitSms', 'timeDiscountEnd'];
    
    public function user(){
        return $this->belongsTo('User');
    }
    
    public function poll(){
        return $this->belongsTo('Poll');
    }
    
    public function question(){
        return $this->belongsTo('PollQuestion','currentQuestion');
    }
    
    public function employee(){
        return $this->belongsTo('PollEmployee','currentEmployee');
    }
}
