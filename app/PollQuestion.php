<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class PollQuestion extends Model
{
    protected $fillable = ['poll_id','type','list','index','name'];
    
    public function poll(){
        return $this->belongsTo('Poll');
    }
    
    public function answers(){
        return $this->hasMany('PollAnswer');
    }
    
    public function processes(){
        return $this->hasMany('PollProcess');
    }
    
    public function results(){
        return $this->hasMany('PollResult');
    }
    
}