<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class PollResult extends Model
{
    protected $fillable = ['user_id', 'poll_id', 'poll_answer_id', 'poll_question_id', 'poll_process_id', 'ignored'];
    
    public function answer(){
        return $this->belongsTo('PollAnswer','poll_answer_id');
    }
    
    public function question(){
        return $this->belongsTo('PollQuestion','poll_question_id');
    }
    
    public function process(){
        return $this->belongsTo('PollProcess','poll_process_id');
    }
    
    public function user(){
        return $this->belongsTo('User');
    }
}
