<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['id','name','isGroup','lowest','parent','photo','vendorCode','updated1C'];
}
