<?php

namespace EvonaApp\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Request;
use Schema;
use View;
use Auth;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Limit index length in migrations
        //Ограничение длины индекса в миграциях
        Schema::defaultStringLength(191);
        
        //Global views
        //Глобальные вьюхи
        view()->composer('admin.include.menu', 'EvonaApp\Http\ViewComposers\MenuAdminComposer');
        view()->composer('include.music', 'EvonaApp\Http\ViewComposers\MusicComposer');
        
        //Sql log (http://evonafashion.ru?sql)
        //Лог запросов к бд, выводится с параметром sql в url (http://evonafashion.ru?sql)
        if (Request::has('sql')){
            DB::listen(function ($query) {
                dump([
                    $query->sql,
                    $query->bindings,
                    $query->time
                ]);
            });
        }
        
        //session log - /test/session/
        
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
