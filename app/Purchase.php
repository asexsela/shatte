<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class Purchase extends Model
{
    protected $fillable = ['name','price','quantity','finalPrice','datePurchase','discount','vendorCode'];
    
    public function user(){
        return $this->belongsTo('EvonaApp\User');
    }
}
