<?php

namespace EvonaApp;

use Illuminate\Database\Eloquent\Model;

class Regulation extends Model
{
    protected $fillable = ['id','name','code1C','parent','uniq','html','updated1C','isGroup'];
    
    public $incrementing = false;
    
    function parent(){
        return $this->belongsTo('Regulation','parentId');
    }
    
    function children(){
        return $this->hasMany('Regulation','parentId');
    }
    
}
