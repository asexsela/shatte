<?php

namespace EvonaApp;

use Mail;
use HttpService;
use HighLoad;
use MailClass;
use Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $fillable = [
        'name', 'email', 'password', 'code1C', 'name', 'lastName', 'secondName', 'phone', 'email', 'cardNumber', 'dateBirthday', 'dateReceiving', 'sizeClothing', 'employee', 'city_id', 'subdivision_id',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    //protected $visible = ['code1C', 'name', 'lastName', 'secondName', 'phone', 'email', 'cardNumber', 'dateBirthday', 'dateReceiving', 'sizeClothing','employee'];
    
    public function purchases(){
        return $this->hasMany('EvonaApp\Purchase')->orderBy('datePurchase');
    }
    
    public function highLoadHistory($method){
        $limit = HighLoad::checkConfig('highloadLimitTimes',$method);
        $history = [];
        $requests = $this->httpRequests()->select('created_at')->where('method',$method)->orderBy('created_at','desc')->take($limit)->get();
        foreach ($requests as $request){
            $history[] = strtotime($request->time);
        }
        return $history;
    }
    
    public function httpRequests(){
        return $this->hasMany('EvonaApp\HttpRequest');
    }
    
    public function pollsProcesses(){
        return $this->hasMany('PollProcess');
    }
   
    public function pollsResults(){
        return $this->hasMany('PollResult');
    }
    
    public function getAuthPassword () {
        if (Request::has('phone')){
            return bcrypt($this->phone);
        }else{
            return $this->password;
        }
    }
    
    public function confirmedEmail(){
        return $this->confirmedEmail;
    }
    
    public function confirmedSms(){
        return $this->confirmedEmail && $this->confirmedSms;
    }
    
    public function sendConfirmMail($email = false){
        
        if($email){
            $this->email = $email;
        }
        
        $this->emailCode = md5($this->email.time());
        $this->confirmedEmail = 0;
        $this->confirmEmailWait = 1;
        $this->save();

        $mail = new MailClass('Добро пожаловать на сайт Evona','confirmEmail',[
            'name' => $this->name,
            'cardNumber' => $this->cardNumber,
            'phone' => $this->phone,
            'confirmLink' => route('confirmEmailIndex').'/?code='.$this->emailCode.'&id='.$this->id,
        ]);

        Mail::to($this->email)->send($mail);
        
        session(['lastTryConfirmEmail' => time()]);
        
    }
    
    public function sendConfirmMailCorporate($password){
        
        $this->emailCode = md5($this->email.time());
        $this->confirmedEmailCorporate = 0;
        $this->save();

        $mail = new MailClass('Корпоративный раздел: регистрация','confirmEmailCorporate',[
            'name' => $this->name,
            'email' => $this->email,
            'password' => $password,
            'confirmLink' => route('corporate.registration.index').'/?code='.$this->emailCode.'&id='.$this->id,
        ]);

        Mail::to($this->email)->send($mail);
        
        session(['lastTryConfirmEmail' => time()]);
        
    }
    
    public function sendRememberLinkCorporate(){
        
        $this->emailCode = md5($this->email.time());
        $this->save();

        $mail = new MailClass('Корпоративный раздел: восстановление пароля','newPasswordCorporate',[
            'name' => $this->name,
            'newPasswordLink' => route('corporate.recovery.index').'/?code='.$this->emailCode.'&id='.$this->id,
        ]);

        Mail::to($this->email)->send($mail);
        
        session(['lastTryNewPassword' => time()]);
        
    }
    
    public function updateInfo($data){
        
        $diffs = [];
        if ($this->name != $data['name'] || $this->secondName != $data['secondName'] || $this->lastName != $data['lastName']){
            
            $diffs['name'] = $data['lastName'].' '.$data['name'].' '.$data['secondName'];
            
            $this->name = $data['name'];
            $this->secondName = $data['secondName'];
            $this->lastName = $data['lastName'];
            
            $this->smsCodeInfo = '';
            
        }
        if ($this->email != $data['email']){
            $this->email = $data['email'];
            $this->sendConfirmMail($data['email']);
        }
        
        
        if (count($diffs)){
            
            $checkUpdate = HttpService::makeRequest('editCard',[
                'code1C' => $this->code1C,
            ],$diffs);
            
            if (!$checkUpdate->errors()){
                $this->save();
            }
            
            return $checkUpdate;
        }
        
        return false;
    }
    
    public function isAdmin(){
        return $this->admin;
    }
    
    public function isEmployee(){
        return $this->employee && $this->confirmedEmailCorporate;
    }
}
