<?php

namespace EvonaApp;

use MailClass;
use Mail;
use Config;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    protected $fillable = ['name','lastName','type','time','city','age','experience','email','phone','photo','resume','comment'];

    public function sendMail(){

        $data = $this->toArray();
        $vacancyUrl = url('/').Config::get('settings.filePaths.vacancy');

        $data['urlPhoto'] = $vacancyUrl.$data['photo'];
        if (isset($data['resume'])){
            $data['urlResume'] =  $vacancyUrl.$data['resume'];
        }

        $mail = new MailClass('Новая анкета','vacancy',$data);

        Mail::to([
            Config::get('settings.mails.dev'),
            Config::get('settings.mails.divisional')
        ])->send($mail);

    }

}
