<?php

return [
    
    'highloadLimitTimes' => [
        'default' => 5,
        'getPurchases' => 1,
        'editCard' => 2,
    ],
    
    'highloadLimitTime' => [
        'default' => 60,
        'getPurchases' => 300,
    ],
    
    'highLoadError' => 'Слишком много запросов к базе данных, попробуйте через ', //** секунд
    
    'commonError' => 'commonError',
    
    // 'mails' => [
    //     'dev' => '44@evonafashion.ru',
    //     'director' => '00@evonafashion.ru',
    //     'divisional' => '30@evonafashion.ru',
    // ],
    'mails' => [
        'dev' => 'a.selyukov@place-start.ru'
    ],
    
    'filePaths' => [
        'vacancy' => '/uploads/vacancy/',
        'music' => '/uploads/music/',
        'employee' => '/uploads/employee/',
        'regulations' => '/uploads/regulations/',
        'products' => '/uploads/products/',
        'analogs' => '/uploads/analogs/',
    ],
    
    'testSms' => '9159624533',
    
    
    'adminMenu' => [
        'music' => [
            'name' => 'Музыка',
            'route' => 'admin.music.index',
            'fa' => 'fa-music',
        ],
    ]

];

