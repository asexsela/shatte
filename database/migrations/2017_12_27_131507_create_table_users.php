<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cardNumber',6)->index('cardNumber');
            $table->string('phone',10)->index('phone');
            $table->string('code1C',15);
            $table->string('name',255);
            $table->string('lastName',255);
            $table->string('secondName',255);
            $table->string('email',255);
            $table->string('sizeClothing',8);
            $table->tinyInteger('discount');
            $table->tinyInteger('confirmedEmail');
            $table->tinyInteger('confirmEmailWait');
            $table->tinyInteger('confirmedSms');
            $table->string('remember_token',100);
            $table->string('emailCode',32);
            $table->string('smsCode',4);
            $table->string('smsCodeInfo',4);
            $table->date('dateBirthday');
            $table->dateTime('dateReceiving');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
