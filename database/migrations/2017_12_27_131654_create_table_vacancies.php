<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVacancies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacancies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',64);
            $table->string('lastName',64);
            $table->string('type',32);
            $table->string('time',32);
            $table->string('city',32);
            $table->tinyInteger('age');
            $table->string('experience',32);
            $table->string('phone',10);
            $table->string('email',64);
            $table->string('photo',64);
            $table->string('resume',64)->nullable();
            $table->string('comment',1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacancies');
    }
}
