<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPollProcessesSmsTime extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('poll_processes', function($table) {
            $table->datetime('timeDiscountEnd');
            $table->tinyInteger('waitSms')->after('status');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('poll_processes', function($table) {
            $table->dropColumn('timeDiscountEnd');
            $table->dropColumn('waitSms');
        });
    }
}
