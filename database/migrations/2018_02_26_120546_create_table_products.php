<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integer('id')->primary('id');
            $table->integer('parent')->index('parentId');
            $table->tinyInteger('isGroup');
            $table->tinyInteger('lowest');
            $table->string('name',255);
            $table->string('vendorCode',16);
            $table->string('photo',255);
            $table->dateTime('updated1C');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
