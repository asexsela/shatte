<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductsAnalogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products_analogs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('subdivision_id')->index('subdivision_id');
            $table->integer('employee_id')->index('employee_id');
            $table->integer('product_id')->index('product_id');
            $table->tinyInteger('type');
            $table->string('colors',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products_analogs');
    }
}
