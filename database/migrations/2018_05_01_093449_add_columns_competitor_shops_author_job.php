<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsCompetitorShopsAuthorJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('competitor_shops', function (Blueprint $table) {
            $table->string('author',16);
            $table->tinyInteger('job');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('competitor_shops', function (Blueprint $table) {
            $table->dropColumn('author');
            $table->dropColumn('job');
        });
    }
}
