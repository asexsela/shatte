<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'ps-admin',
            'email' => 'psadmin@admin.com',
            'password' => bcrypt('password'),
            'admin' => 1,
            'employee' => 1,
        ]);
    }
}
