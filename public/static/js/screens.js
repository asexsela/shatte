/**
 * jQuery Plugin to obtain touch gestures from iPhone, iPod Touch and iPad, should also work with Android mobile phones (not tested yet!)
 * Common usage: wipe images (left and right to show the previous or next image)
 * 
 * @author Andreas Waltl, netCU Internetagentur (http://www.netcu.de)
 * @version 1.1.1 (9th December 2010) - fix bug (older IE's had problems)
 * @version 1.1 (1st September 2010) - support wipe up and wipe down
 * @version 1.0 (15th July 2010)
 */
(function($){$.fn.touchwipe=function(settings){var config={min_move_x:20,min_move_y:20,wipeLeft:function(){},wipeRight:function(){},wipeUp:function(){},wipeDown:function(){},preventDefaultEvents:true};if(settings)$.extend(config,settings);this.each(function(){var startX;var startY;var isMoving=false;function cancelTouch(){this.removeEventListener('touchmove',onTouchMove);startX=null;isMoving=false}function onTouchMove(e){if(config.preventDefaultEvents){e.preventDefault()}if(isMoving){var x=e.touches[0].pageX;var y=e.touches[0].pageY;var dx=startX-x;var dy=startY-y;if(Math.abs(dx)>=config.min_move_x){cancelTouch();if(dx>0){config.wipeLeft()}else{config.wipeRight()}}else if(Math.abs(dy)>=config.min_move_y){cancelTouch();if(dy>0){config.wipeDown()}else{config.wipeUp()}}}}function onTouchStart(e){if(e.touches.length==1){startX=e.touches[0].pageX;startY=e.touches[0].pageY;isMoving=true;this.addEventListener('touchmove',onTouchMove,false)}}if('ontouchstart'in document.documentElement){this.addEventListener('touchstart',onTouchStart,false)}});return this}})(jQuery);








window.onload = function() {
	document.querySelector('.logo').style.visibility = 'visible'
}

let title = document.querySelectorAll('.title-animation')
Array.prototype.slice.call( title ).forEach(function(elem, index) {
	elem.innerHTML = elem.innerHTML.replace(/(.)/g, '<span class="anime2">$1</span>'); // Оборачиваем буквы в span
})

var ScreenSlider = (function() {
	var defaultArgs = {
		container: null,
		body: null,
		nav: null,
	}
	function ScreenSlider(params) {
		var args = params || defaultArgs;

		if (args.container === null || args.body === null || args.nav === null) return 
		
		this.wheelArea = document.querySelector('.screens')
		this.container = document.querySelector(args.container)
		this.body = document.querySelector(args.body)
		this.nav = document.querySelector(args.nav)

		if (this.container === null || this.body === null || this.nav === null) return

		this.breakpoint = 1200




		this.init()
	}
	

	ScreenSlider.prototype = {
		init: function() {
			var _this = this
			this.container.screens = this
	
			// Идентификатор найденого экрана по hash из url
			var hashFounded = false
	
			// Иницилизация экранов
			Array.prototype.slice.call(this.body.children).forEach(function(screen, index) {
				screen.navItem = _this.nav.children[index]
				new ScreenSlide(_this.container, screen)
	
				// Нахождение и иницилизация текущего экрана по hash в url
				if (document.location.hash) {
					if ('#' + screen.id === document.location.hash) {
						_this.initCurrentScreen(screen, index)
						hashFounded = true
					}
				}
			})
	
			// Иницилизация первого экрана как текущего, если не было найдено экрана
			// по hash в url
			if (!hashFounded) {
				var screen = this.body.children[0]
				this.initCurrentScreen(screen, 0)
			}
	
			// Иницилизация навигации
			Array.prototype.slice.call(this.nav.children).forEach(function(navItem, index) {

				if (_this.body.children[index].classList.contains('current'))
					navItem.classList.add('active')
				navItem.screen = _this.body.children[index]
				
				// Регистрация клика по элементу навигации
				navItem.onclick = _this.onClickNavItem.bind(_this)
			})
	

			var dataToScreen = document.querySelectorAll('[data-to-screen]')
			Array.prototype.slice.call(dataToScreen).forEach(function(button) {
				button.onclick = function(event) {
					event.preventDefault()
					$('.modal').modal('hide')
					var name = button.getAttribute('data-to-screen')
					Array.prototype.slice.call(_this.body.children).forEach(function(item) {
						if (name === item.id) {
							_this.toScreen(item)
							
						}
					})
				}
			})

			//регистрация события swipe
			if (!_this.isMobile()) {
				$("body").touchwipe({
					wipeUp: function() { _this.onPrevScreen() },
					wipeDown: function() { _this.onNextScreen() },
					min_move_x: 20,
					min_move_y: 20,
					preventDefaultEvents: false
			   });
			}



			// Регистрация события переключения экрана с помощью стрелочек
            function keyClick(event) {
                if ( event.keyCode == 38 || event.keyCode == 33 ) {
					_this.onPrevScreen() // вверх
				}
                if ( event.keyCode == 40 || event.keyCode == 34 ) {
					_this.onNextScreen() // вниз
				}
            };
			window.addEventListener("keyup", keyClick);

			// Регистрация события кручения колесиком в области экранов
			if (this.wheelArea) {
				if ('onwheel' in document) {
					// IE9+, FF17+, Ch31+
					this.wheelArea.addEventListener("wheel", onWheel);
				} else if ('onmousewheel' in document) {
					// устаревший вариант события
					this.wheelArea.addEventListener("mousewheel", onWheel);
				} else {
					// Firefox < 17
					this.wheelArea.addEventListener("MozMousePixelScroll", onWheel);
				}
			} else { // IE8-
				this.wheelArea.attachEvent("onmousewheel", onWheel);
			}

			function onWheel(event) {
				var target = event.target


				// if( $(target).closest('.gmap')[0] ==  $('.gmap')[0] ) return


				if (_this.isMobile()) return



				if(event.deltaY) {
					if (event.deltaY > 1 || event.deltaY < -1) {
						if( event.deltaY > 0 ) _this.onNextScreen()
						else _this.onPrevScreen()
					}
				} else {
					if (event.wheelDelta > 1 || event.wheelDelta < -1) {
						if( event.wheelDelta < 0 ) _this.onNextScreen()
						else _this.onPrevScreen()
					}
				}
				
			}
		},

		initCurrentScreen: function(screen) {
			this.current = screen
			this.current.fullScreenSlide.animeBegin()
			this.current.fullScreenSlide.onEnter(this.current)
			this.current.fullScreenSlide.logoLoading(this.current)
		},

		onClickNavItem: function(event) {
			var item = event.currentTarget
			if (item.screen === this.current) return
			this.current.fullScreenSlide.onChange(item.screen)
		},

		toScreen: function(screen) {
			this.current.fullScreenSlide.onChange(screen)
		},

		onPrevScreen: function() {
			if (this.current.previousElementSibling !== null) {
				this.current.fullScreenSlide.onChange(this.current.previousElementSibling, 'top')
			}
		},

		onNextScreen: function() {
			if (this.current.nextElementSibling !== null) {
				this.current.fullScreenSlide.onChange(this.current.nextElementSibling, 'down')
			}
		},

		isMobile: function() {
			return (window.innerWidth < this.breakpoint) ? true : false
		}
	}

	return ScreenSlider
}())

var ScreenSlide = (function() {
	function ScreenSlide(container, screen) {
		this.container = container
		this.screen = screen


		this.logoContainer = document.querySelector('.logo')
		this.logoWidth = this.logoContainer.clientWidth
		this.logoHeight = this.logoContainer.clientHeight


		if (this.screen === null) return
		this.screen.fullScreenSlide = this
	}

	ScreenSlide.prototype = {
		intervals: [null, null, null],
		
		animeBegin: function() {
			this.container.screens.animationStarted = true
		},

		animeEnd: function() {
			this.container.screens.animationStarted = false
		},

		onChange: function(willScreen, direction) {
			var direction = direction || null
			var _this = this
	
			if (this.container.screens.animationStarted) return
			this.animeBegin()

			this.logoAnime(willScreen)
			// Начало смены экрана на новый
			this.onLeave(function() { _this.onEnter(willScreen) })
			
			// Смена хеш в URL для того что бы при обновлении страницы была нужная нам странца
			if (willScreen.id !== undefined)
				document.location.hash = willScreen.id
	
			// Смена элемента навигации на новый экран
			this.screen.navItem.classList.remove('active')
			willScreen.navItem.classList.add('active')
		},

		onLeave: function(params) {
			var callback = params || function() {}
			var _this = this

			this.exitScreenAnimate(this.screen)

			anime({
				targets: this.screen,
				opacity: 0,
				delay: 200,
				duration: 400,
				easing: 'easeInOutCubic',
				complete: function() {
					_this.screen.style.display = 'none'
					_this.screen.classList.remove('current')
					_this.screen.style.display = ''
					_this.screen.style.opacity = ''

					callback()
				}
			})
		},

		onEnter: function(willScreen, params ) {
			var callback = params || function() {}
			var _this = this
			var _callback = function() {
				setTimeout(function() { _this.animeEnd() }, 500)
				callback()
			}

			this.entryScreenAnimate(willScreen)

			this.container.screens.current = willScreen
			willScreen.classList.add('current')
			willScreen.style.opacity = '0'


			anime({
				targets: willScreen,
				opacity: 1,
				duration: 600,
				easing: 'easeInOutQuad',
				complete: function() { _callback() }
			})

			
			var newcustomEvent = document.createEvent('HTMLEvents');
			newcustomEvent.initEvent('screen.enter', true, true);
			this.screen.fullScreenSlide.container.dispatchEvent(newcustomEvent);
		},

		entryScreenAnimate: function(willScreen) {
			this.bgEntryAnime(willScreen)
			this.svgAnime(willScreen)
			this.girlAnime(willScreen)
			this.textAnimate(willScreen)
			this.titleAnime(willScreen)


			if(willScreen.id !== 'main-screen' && willScreen.id !== 'contacts-screen') {
				this.bgLogoEntryAnime()
			}

			if(willScreen.id == 'advantage-screen' || this.isMobile()) {
				this.intInterval()
			}

			if(willScreen.id == 'shop-screen' || this.isMobile()) {
				this.gallery()
			}

			if( willScreen.id == 'contacts-screen' ) {
				this.markersAnimate()
			}



		},

		exitScreenAnimate: function(screen) {
			this.bgExitAnime(screen)
			this.svgExitAnime(screen)
			if(screen.id !== 'main-screen' && screen.id !== 'contacts-screen') {
				this.bgLogoExitAnime()
			}
		},

		logoLoading: function(currentScreen) {
			let text = this.logoContainer.querySelector('.logo__text')


			let w, h, top

			if (this.isMobile()) {
				w = '100px'
				h = '47px'
				top = '10px'	
			} else {
				w = '150px'
				h = '70px'
				top = 0
			}

			if( currentScreen.id == 'main-screen' ) {
				this.logoContainer.style.width = this.logoWidth+'px'
				this.logoContainer.style.height = this.logoHeight+'px'
				this.logoContainer.style.top = '25vh'

				text.style.opacity = 1
				text.style.visibility = 'visible'
			} else {
				this.logoContainer.classList.add('position-top')
				this.logoContainer.style.width = w
				this.logoContainer.style.height = h
				this.logoContainer.style.top = top

				text.style.opacity = 0
				text.style.visibility = 'hidden'
			}
		},

		logoAnime: function(currentScreen) {
			let text = this.logoContainer.querySelector('.logo__text')

			let w, h, top

			if (this.isMobile()) {
				w = '100px'
				h = '47px'	
				top = '10px'
			} else {
				w = '150px'
				h = '70px'
				top = 0
			}

			if( currentScreen.id == 'main-screen' ) {
				anime({
					targets: text,
					opacity: 1,
					duration: 800,
					easing: 'easeInOutCubic',
					complete: function() {
						text.style.visibility = 'visible'
					}
				})
				anime({
					targets: this.logoContainer,
					top: [top, '25vh'],
					scale: 1,
					width: this.logoWidth+'px',
					height: this.logoHeight+'px',
					duration: 800,
					easing: 'easeInOutCubic',
				})
				this.logoContainer.classList.remove('position-top')
			} else {
				if( !this.logoContainer.classList.contains('position-top') ) {
					this.logoContainer.classList.add('position-top')
					anime({
						targets: text,
						opacity: 0,
						duration: 0,
						duration: 800,
						easing: 'easeInOutCubic',
						complete: function() {
							text.style.visibility = 'hidden'
						}
					})

					anime({
						targets: this.logoContainer,
						scale: .6,
						top: ['25vh', '5vh'],
						opacity: 0,
						duration: 500,
						easing: 'easeInOutCubic',
						complete: function() {
							anime({
								targets: this.logoContainer,
								scale: 0,
								top: top,
								duration: 0,
								width: w,
								height: h,
								complete: function() {
									anime({
										targets: this.logoContainer,
										opacity: 1,
										scale: 1,
										easing: 'easeInOutCubic',
									})
								}.bind(this)
							})
						}.bind(this)
					})
				}
			}
		},

		svgAnime: function(currentScreen) {
			let path = currentScreen.querySelectorAll('.path-animation')
			for (let index = 0; index < path.length; index++) {
				const element = path[index];
				anime({
					targets: element,
					fillOpacity: [0, 1],
					translateY: [element.getAttribute('data-anime'), 0],
					easing: element.getAttribute('data-easing'),
				});
			}
		},

		svgExitAnime: function(currentScreen) {
			let path = currentScreen.querySelectorAll('.path-animation')
			for (let index = 0; index < path.length; index++) {
				const element = path[index];
				anime({
					targets: element,
					fillOpacity: [1, 0],
					translateY: [0, -1 * element.getAttribute('data-anime')],
					easing: element.getAttribute('data-easing'),
				});
			}
		},

		girlAnime: function(currentScreen) {
			let girl = currentScreen.querySelectorAll('.girl-animation')
			for (let index = 0; index < girl.length; index++) {
				const element = girl[index];
				anime({
					targets: element,
					opacity: [0, 1],
					translateX: [element.getAttribute('data-anime'), 0],
					easing: element.getAttribute('data-easing'),
					duration: 1500,
				});
			}
		},

		bgEntryAnime: function(nextScreen) {
			let bg = nextScreen.querySelectorAll('.bg')
			for (let index = 0; index < bg.length; index++) {
				const element = bg[index];
				if( element.classList.contains('bg--left') ) {
					anime({
						targets: element,
						top: ['100%', 0],
						duration: 500,
						easing: 'easeInOutQuart',
					});
				} else if( element.classList.contains('bg--right') ) {
					anime({
						targets: element,
						bottom: ['100%', 0],
						duration: 500,
						easing: 'easeInOutQuart',
					});
				}
			}
		},

		bgExitAnime: function(currentScreen) {
			let bg = currentScreen.querySelectorAll('.bg')
			for (let index = 0; index < bg.length; index++) {
				const element = bg[index];

				if( element.classList.contains('bg--left') ) {
					anime({
						targets: element,
						top: [0, '100%'],
						duration: 500,
						easing: 'easeInOutQuart',
					});
				} else if( element.classList.contains('bg--right') ) {
					anime({
						targets: element,
						bottom: [0, '100%'],
						duration: 500,
						easing: 'easeInOutQuart',
					});
				}
			}
		},

		bgLogoEntryAnime: function() {
			let logoBg = document.querySelectorAll('.logo-bg')
			for (let index = 0; index < logoBg.length; index++) {
				const element = logoBg[index];
				anime({
					targets: element,
					scale: [.4, 1],
					opacity: [0, 1],
					translateY: [300, 0],
					duration: 1500,
					easing: 'easeInOutQuart',
				});
			}

		},

		bgLogoExitAnime: function() {
			let logoBg = document.querySelectorAll('.logo-bg')
			for (let index = 0; index < logoBg.length; index++) {
				const element = logoBg[index];
				anime({
					targets: element,
					scale: [1, .4],
					opacity: [1, 0],
					translateY: [0, 300],
					duration: 500,
					easing: 'easeInOutQuart',
				});
			}
		},

		textAnimate: function(currentScreen) {
			let text = currentScreen.querySelectorAll('.text-animate')
			for (let index = 0; index < text.length; index++) {
				const element = text[index];

				anime({
					targets: element,
					translateX: [element.getAttribute('data-anime'), 0],
					delay: 200 * index + 100,
					duration: 700,
					easing: element.getAttribute('data-easing')
				});
				anime({
					targets: element,
					opacity: [0, 1],
					delay: 300 * index + 200,
					duration: 1000,
					easing: 'linear'
				});
			}
		},

		titleAnime: function(currentScreen) {
			if( currentScreen.querySelector('.anime2') ) {
				anime({
					targets: '.anime2',
					opacity:[0, 1],
					easing:'easeInOutQuad',
					delay: function(el, index) {
						return index * 70;
					},
					direction: 'alternate',
					loop: false
				});
			}
		},

		intInterval: function() {
			const _this = this;
			
			let countNumber = document.querySelectorAll('.count-number')

			Array.prototype.slice.call(countNumber).forEach(function(elem, index) {
				if (_this.intervals[index] !== null) {
					clearInterval(_this.intervals[index]);
				}

				elem.innerText = 0;
				let max = +elem.getAttribute('data-max')
				let speed = +elem.getAttribute('data-speed')

				let itteration = speed / max
				let step = 1

				if( itteration  < 4 ) step = 4 / itteration
				
				_this.intervals[index] = setInterval(function() {
					if( elem.innerText >= max ) return

					elem.innerText = parseInt(elem.innerText) + step
				},itteration )
			})

		},

		gallery: function() {
			anime({
				targets: document.querySelector('.gallery'),
				opacity: [0, 1],
				duration: 1500,
				easing:'easeInOutSine',
			})
			let gallery = document.querySelectorAll('.gallery__item')
			let translateX, translateY
			for (let index = 0; index < gallery.length; index++) {
				const element = gallery[index];
				if( window.innerWidth < 1200 ) {
					if( element.classList.contains('item1') ) {
						translateX = [0, 10]
						translateY = [0, -20]
					} else if( element.classList.contains('item2') ) {
						translateX = [0, 10]
						translateY = [0, 10]
					} else if( element.classList.contains('item3') ) {
						translateX = [0, 10]
						translateY = [0, -10]
					} else if( element.classList.contains('item4') ) {
						translateX = [0, 10]
						translateY = [0, 20]
					}
				} else {
					if( element.classList.contains('item1') ) {
						translateX = [0, 30]
						translateY = [0, -60]
					} else if( element.classList.contains('item2') ) {
						translateX = [0, 30]
						translateY = [0, 30]
					} else if( element.classList.contains('item3') ) {
						translateX = [0, 30]
						translateY = [0, -30]
					} else if( element.classList.contains('item4') ) {
						translateX = [0, 30]
						translateY = [0, 60]
					}
				}

				anime({
					targets: element,
					translateX: translateX,
					translateY: translateY,
					delay: 500,
					duration: 1500,
					easing:'easeOutElastic',
				})
				
			}
		},

		markersAnimate: function() {
			let markers = document.querySelectorAll('.map__marker')
			anime({
				targets: markers,
				opacity: [0, 1],
				scale: [0, 1],
				rotate: 45,
				duration: 500,
				delay: function(el, i, l) {
					return i * 100;
				},
				easing:'easeInOutSine',
			})
		},

		isMobile: function() {
			return (window.innerWidth < 1200) ? true : false
		}
	}


	return ScreenSlide
}())

var screenSliderArgs = {
    container: '.screens',
    body: '.screens-body',
	nav: '.screens-nav',
}
var screenSlider = new ScreenSlider(screenSliderArgs)

screenSlider.container.addEventListener('screen.enter', function() {
	custom_resize();
})
