function custom_resize() {
    let galleryContainer = document.querySelector('.gallery')
    if (galleryContainer) {
        galleryContainer.style.height = galleryContainer.clientWidth / 1.087719298245614 +'px'
    }
}
custom_resize();
$(window).resize(function () {
	custom_resize();
});


function openMobileMenu(event) {
	event.preventDefault();

	if( this.classList.contains('active') ) {
		this.classList.remove('active')

		document.querySelector('.mobile-menu').classList.remove('opened')
		document.querySelector('body').classList.remove('opened')
	} else {
		this.classList.add('active')
		document.querySelector('.mobile-menu').classList.add('opened')
		document.querySelector('body').classList.add('opened')
	}
}
document.querySelector('.mobile-menu-open').onclick = openMobileMenu



var dataAnchore = document.querySelectorAll('[data-anchore]')
Array.prototype.slice.call(dataAnchore).forEach(function(button) {
	button.onclick = function(event) {
		event.preventDefault()
		var name = button.dataset.anchore
		
		document.querySelector('.mobile-menu').classList.remove('opened')
		document.querySelector('body').classList.remove('opened')
		document.querySelector('.mobile-menu-open').classList.remove('opened')
		
		target_offset = $('#'+name).offset().top;
		console.log( target_offset )
		$('html, body').animate({scrollTop:target_offset}, 500);
	}
})

let cities = document.querySelectorAll('.cities__link')
Array.prototype.slice.call( cities ).forEach(function(city) {
	city.onclick = openModel
})

function openModel(event) {
	let target = event.currentTarget
	let container = document.querySelector('.modal-body__container')
	let html = target.parentNode.querySelector('.hide-content').innerHTML
	$('.modal').modal('show')

	console.log( container )

	container.innerHTML = html

}

$('.change-personal-info').click(function() {
    $('.personal-info').hide();
    $('.personal-info-edit').show();
    localStorage.setItem('read-info', 1)
});

$('.personal-info-do-not-save').click(function() {
    $('.personal-info-edit').hide();
    $('.personal-info').show();
    localStorage.setItem('read-info', 0)

});

let loc = localStorage.getItem('read-info');

if (loc == 1) {
    $('.personal-info').hide();
    $('.personal-info-edit').show();
} else {
    $('.personal-info-edit').hide();
    $('.personal-info').show();
}

$('.site-input').keyup(function(e) {

    let length_text = $(this).val();

    if (length_text.length <= 0) {
        $(this).parent().parent().find('label').addClass('empty_input');
    } else {
        $(this).parent().parent().find('label').removeClass('empty_input');
    }
    
});

$('.checkPHONE').keyup(function() {

    $(this).parent().find('.message_phone').show();

});

// $('.send_errors').click(function(e) {

//     let token = $('meta[name="csrf-token"]').attr('content');

//     e.preventDefault();

//     $.ajax({
//         type: 'POST',
//         url: '/personal/items/error/',
//         data: {
//             '_method': 'POST',
//             '_token': token,
//             'comment': $('#comment').val()
//         },
//         success: function(data) {
//             console.log(data);
//         }
//     });

// });