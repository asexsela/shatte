<ul class="sidebar-menu tree" data-widget="tree">
    <!--<li class="header">Навигация</li>-->
    @foreach($menu as $nav)
        <li class="@if($currentRoute == $nav['route']) active @endif"><a href="{{ route($nav['route']) }}"><i class="fa {{ $nav['fa'] }}"></i> <span>{{ $nav['name'] }}</span></a></li>
    @endforeach
</ul>