@extends('layouts.templateAdminMain')

@section('content')

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('admin.music.edit') }}/" method="post" enctype="multipart/form-data">
            <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Музыка</h3>
            </div>
            <div class="box-body">
               {{ csrf_field() }}
              <table class="table table-bordered">
                <tbody>
                <tr>
                  <th style="width: 10px">#</th>
                  <th>Название</th>
                  <th>Файл</th>
                  <th>Громкость</th>
                  <th style="width: 62px"></th>
                </tr>
                @foreach($music as $sound)
                    <tr data-id="{{ $sound->id }}">
                      <td style="width: 10px">{{ $sound->id }}.</td>
                      <td>
                          <span data-id="{{ $sound->id }}" data-volume="{{ $sound->volume/100 }}" class="music-controls paused va-middle"></span>
                          <audio preload="auto" src="{{ $path.$sound->name }}"  id="mp3-{{ $sound->id }}" loop></audio>
                          <span class="va-middle">{{ $sound->name }}</span></td>
                      <td><input type="file" name="sound[{{ $sound->id }}]" ></td>
                      <td>
                            <input type="text" name="volume[{{ $sound->id }}]" value="{{ $sound->volume }}" class="slider form-control music-slider" data-slider-min="0" data-slider-max="100"
                             data-slider-step="1" data-slider-value="{{ $sound->volume }}" data-slider-orientation="horizontal"
                             data-slider-selection="before" data-slider-tooltip="hide" data-slider-tooltip="show" data-slider-id="blue">
                      </td>
                      <td  style="width: 62px" class="td-volume">
                          <span class="badge bg-light-blue">{{ $sound->volume }}%</span>
                      </td>
                    </tr>
                @endforeach
              </tbody></table>
            </div>
                <div class="box-footer"><button type="submit" class="btn btn-primary ">Сохранить</button></div>

          </div>
               </form>
        </div>
    </div>
</section>

<script>
    $(function () {
        $('.music-slider').on('slide',function(){
            var _ = $(this),
                id = _.parents('tr').data('id'),
                volume = _.data('slider').getValue();
            _.parents('tr').find('.td-volume span').html(volume + '%');
            $('#mp3-' + id)[0].volume = volume / 100;
        });
        
        $(document).on('click','.music-controls',function(){
            var _ = $(this);
            var player = _.data('id');
            var volume = _.data('volume');
            if (_.hasClass('paused')){
                $('#mp3-' + player)[0].volume = parseFloat(volume);
                $('#mp3-' + player)[0].play();
                $('.music-controls.played').click();
            }else{
                $('#mp3-' + player)[0].pause();
            }


            _.toggleClass('paused').toggleClass('played');
        })
        
    });
</script>

@endsection