@extends('layouts.templateMain')

@section('title', 'Первая авторизация')

@section('content')

<div class="inner-page-content">

            <div class="page-title">
                <h1 class="page-title__h1">Первая авторизация</h1>
                <div class="page-title_big">Первая авторизация</div>
                <div class="page-title_small">Первая авторизация</div>
            </div>
	
	<p><strong>{{ $userName }}</strong>, для того, чтобы начать пользоваться личным кабинетом, необходимо<br /> ввести в строку ниже Ваш e-mail и нажать кнопку "отправить".</p>

	<form class="default-evona-form" action="{{ route('confirmEmail') }}/" method="post">
                {{ csrf_field() }}
		<p>Электронная почта</p>
                <div class="input-holder">
                    <p><input type="text" name="email" id="emailinput" class="site-input site-input_small" value="{{ old('email') }}"></p>
                    <div class="error-line">{{ $errors->first('email') }}</div>
                </div>
		<p><button type="submit" name="submit_email" class="site-button site-button-pad">Отправить</button>
	</form>

<div class="too-soon-seven hide">
    <p>С момента отправки письма на  прошло менее <b>7 минут</b>.
        Если в течение этого времени письмо не придёт – нажмите на кнопку ещё раз.</p>
    <div class="form-line_center">
        <button class="site-button site-button-pad site-button_popup" onclick="$.fancybox.close();">Хорошо</button>
    </div>
</div>


<div class="too-soon-five hide">
    <div class="error_close"></div>
    <p>С момента отправки письма на not@test.ru прошло менее <b>5 минут</b>.
        Если в течение этого времени письмо не придёт – повторите процедуру смены электронной почты.</p></div>

<div class="error-mes-send hide">
    <p>
        Сообщение об ошибке отправлено администрации.
    </p>
    <div class="form-line_center">
        <button class="site-button site-button-pad site-button_popup" onclick="$.fancybox.close();">Хорошо</button>
    </div>
</div>


<div class="reactivate-mes-send hide">
    <p>
        Письмо с кодом активации отправлено еще раз.
    </p>
    <div class="form-line_center">
        <button class="site-button site-button_popup site-button-pad" onclick="$.fancybox.close();">Хорошо</button>
    </div>
</div>

</div>

@endsection