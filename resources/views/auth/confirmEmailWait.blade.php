@extends('layouts.templateMain')

@section('title', 'Первая авторизация')

@section('content')

<div class="inner-page-content">

            <div class="page-title">
                <h1 class="page-title__h1">Подтверждение</h1>
                <div class="page-title_big">Подтверждение</div>
                <div class="page-title_small">Подтверждение</div>
            </div>
        <!--<h2>Личный кабинет</h2>-->
            @if (isset($errorMessage) && $errorMessage)
                <div class="error"><p><b>{{ $errorMessage }}</b></p></div>
            @endif
			<p class="maintext">На указанный Вами адрес электронной почты
        <b class="big_white">{{ $userEmail }}</b> отправлено письмо для подтверждения e-mail адреса.</p>
		<p class="destext">Письмо не приходит? &mdash; Посмотрите в папке <b class="big_white">"Спам"</b>, проверьте <b class="big_white">правильный ли указан e-mail</b>.</p>
    <form action="{{ route('confirmEmail') }}/" method="post">            
        {{ csrf_field() }}
        <input type="hidden" name="again" value = "1">
        <button class="site-button change-email-resend site-button-pad" name="again" value="1" data-min="five">Отправить ещё одно письмо</button>
    </form>
    <input type="hidden" class="email-to" value="qwe@qwe.qq">


			<p class="error">Возможно вы указали некорректный почтовый домен <b>{{ $emailDomain }}</b>.</p>
		


        <p>Если Вы не получили письмо по эл. почте для подтверждения авторизации на сайте, то проверьте папку "спам".
            Если в указанной папке письма также нет, то нажмите на кнопку "отправить сообщение о проблеме".</p>


    <div class="problem-b">
        <form class="" action="{{ route('errorConfirm') }}/" method="post">
                    {{ csrf_field() }}	
        <p><button class="submit site-button change-email-send-error site-button-pad" data-min="five">Отправить сообщение о проблеме</button></p>
        </form>
    </div>


		<p class="psmall_white_more">Если адрес <b class="small_big_white">{{ $userEmail }}</b> указан некорректно или Вы используете другой ящик, воспользуйтесь формой ниже:</p>
		<h3>Указать другой e-mail:</h3>
		<form class="default-evona-form change_email_form_not_send" action="{{ route('confirmEmail') }}/" method="post">
                    {{ csrf_field() }}	
                    <p>
                        
                    <input type="text" name="email" id="emailinput" class="site-input site-input_small">
                    <br><span class="error-line">
                        {{ $errors->first('email') }}</span>
                    </p>
                    <button class="site-button site-button-pad" type="submit" name="submit_email" >Отправить</button>

		</form>


<div class="too-soon-seven hide">
    <p>С момента отправки письма на {{ $userEmail }} прошло менее <b>7 минут</b>.
        Если в течение этого времени письмо не придёт – нажмите на кнопку ещё раз.</p>
    <div class="form-line_center">
        <button class="site-button site-button-pad site-button_popup" onclick="$.fancybox.close();">Хорошо</button>
    </div>
</div>


<div class="too-soon-five hide">
    <div class="error_close"></div>
    <p>С момента отправки письма на {{ $userEmail }} прошло менее <b>5 минут</b>.
        Если в течение этого времени письмо не придёт – повторите процедуру смены электронной почты.</p></div>
                
<div class="too-soon-never hide">
    <div class="error_close"></div>
    <p>Попробуйте отправить письмо повторно или указать другой адрес</p></div>
                
<div class="too-soon-early hide">
    <div class="error_close"></div>
    <p>С момента отправки последнего сообщения о проблеме прошло менее <b>5 минут</b>.</p></div>


<div class="error-mes-send hide">
    <p>
        Сообщение об ошибке отправлено администрации.
    </p>
    <div class="form-line_center">
        <button class="site-button site-button-pad site-button_popup" onclick="$.fancybox.close();">Хорошо</button>
    </div>
</div>


<div class="reactivate-mes-send hide">
    <p>
        Письмо с кодом активации отправлено еще раз.
    </p>
    <div class="form-line_center">
        <button class="site-button site-button_popup site-button-pad" onclick="$.fancybox.close();">Хорошо</button>
    </div>
</div>        </div>

@endsection