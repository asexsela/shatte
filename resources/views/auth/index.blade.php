@extends('layouts.templateMain')

@section('title', 'Личный кабинет')

@section('content')
    <style>
        html, body {
            overflow-y: auto;
        }
        .screens-body {
            height: auto;
        }
        @media (max-width: 1024px) {
            section.personal ul {
                padding-left: 20px;
            }
        }
    </style>
    <div class="screens-body ">
        <section class="personal">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>Личный кабинет</h1>
                    </div>
                </div>
                <div class="login">
                    <form method="post" action="{{ route('personal') }}/">
                        {{ csrf_field() }}

                        <div class="input-holder">
                            <input type="text" class="site-input checkLOGIN" id="cardNumber" name="cardNumber" value="{{ old('cardNumber') }}" placeholder="Логин (номер вашей дисконтной карты)">
                            <div class="error-line">{!! $errors->first('cardNumber') !!}</div>
                        </div>

                        <div class="input-holder">
                            <lable class="message_phone" for="phone">+7</lable>
                            <input type="text" class="site-input checkPHONE" id="phone" name="phone" value="{{ old('phone') }}" placeholder="Пароль (номер вашего телефона)">
                            <div class="error-line">{!! $errors->first('phone') !!}</div>
                        </div>

                        <div class="form-line">
                            <button class="site-button" type="submit">Войти</button>
                        </div>
                    </form>
                </div>
                <div class="space"></div>
                <div class="row justify-content-md-center">
                    <div class="col-md-6 col-12">
                        <p>
                            Данный сервис доступен только для владельцев дисконтных карт SHATTE. Дисконтную карту можно получить в любом магазине нашей сети после совершения первой покупки. Изначально,
                            до регистрации на сайте, действует следующая накопительная система скидок:
                        </p>
                        <ul>
                            <li>До 10 000 ₽ – 5 %</li>
                            <li>10 000 – 25 000 ₽ – 10 %</li>
                            <li>25 000 – 45 000 ₽ – 15%</li>
                            <li>От 45 000 ₽ — 20%</li>
                        </ul>
                        <p>
                            Данный сервис доступен только для владельцев дисконтных карт SHATTE. Дисконтную карту можно получить в любом магазине нашей сети после совершения первой покупки. Изначально,
                            до регистрации на сайте, действует следующая накопительная система скидок:
                        </p>
                        <ul>
                            <li>До 10 000 ₽ – 10%</li>
                            <li>10 000 – 25 000 ₽ – 15%</li>
                            <li>25 000 – 45 000 ₽  – 20%</li>
                            <li>От 45 000 ₽ — 25%</li>
                        </ul>
                        <p>
                               <b> Эксклюзивно, для держателей дисконтных карт,  осуществляющих покупки  в  магазине г. Мурманск, ул. Самойловой, 9  с 09.02.2019, скидка увеличена еще на  5% и после регистрации на сайте 
                                (в скобках – без регистрации) размер скидки будет составлять:
                               </b>
                                
                        </p>
                        <ul>
                            <li>До 10 000 ₽ – 15% (-10 %)</li>
                            <li>10 000 – 25 000 ₽ – 20% (-15%)</li>
                            <li>25 000 – 45 000 ₽ – 25% (-20%)</li>
                            <li>От  45 000 ₽ — 30% (-25%)</li>
                        </ul>
                        <p>
                                Для зарегистрированных на сайте, владельцев дисконтных карт нашей сети, становится доступным личный кабинет, который содержит все данные о покупках, что позволяет контролировать увеличение скидки по дисконтной карте.

                        </p>
                        <br>
                        <p>
                                <b> Для держателей дисконтных карт, осуществляющих покупки
                                        в магазине г. Мурманск, ул. Самойловой, 9 дополнительная скидка 5% суммируется и отражается в списке совершенных покупок
                                        (а в личных данных  - не отражается) 
                                        
                                </b>
                                 
                         </p>
                    </div>
                </div>
            </div>
            
        </section>
        
    </div>















        {{-- 


        <form action="{{ route('errorNotify') }}/" class="mistake-auth__form">
            {{ csrf_field() }}
            
            <input type="hidden" name="oldCardNumber" value="{{ old('cardNumber') }}"/>
            <input type="hidden" name="oldPhone" value="{{ old('phone') }}"/>
            <div class="popup-form-title">Нет номера карты</div>
            <div class="form-line">
                <label for="name" class="site-label">Ф. И. О. (полностью)*</label>
                <input type="text" id="name" name="name" class="site-input site-input_popup checkname">
                <div class="error-line hide"></div>
            </div>
            <div class="form-line">
                <label for="card" class="site-label">Номер Вашей карты (6 цифр)*</label>
                <input type="text" id="cardNumber" name="cardNumber" class="site-input site-input_popup checkcardNumber">
                <div class="error-line hide"></div>
            </div>
            <div class="form-line">
                <label for="phone" class="site-label">Телефон*</label>
                <span class="plus_seven">+7</span>
                <input type="text" id="phone" name="phone" class="site-input site-input_popup checkphone">
                <div class="error-line phone-error-line hide"></div>
            </div>
            <div class="form-line">
                <label for="email" class="site-label">Email*</label>
                <input type="text" id="email" name="email" class="site-input site-input_popup checkemail">
                <div class="error-line hide"></div>
            </div>

            <input type="hidden" value="" name="subject" class="ERRDESC">
            <input type="hidden" value="" name="ERRID" class="ERRID">

            <input type="hidden" value="" name="INPUT_CARD">
            <input type="hidden" value="" name="INPUT_PHONE">


            <div class="form-line">
                <button class="site-button site-button_popup" type="submit">Отправить</button>
            </div>
        </form> --}}

@endsection