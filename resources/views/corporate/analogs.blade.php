@extends('layouts.templateCorporate')

@section('content')
<link rel="stylesheet" href="{{ asset('adminLTE/bower_components/select2/dist/css/select2.min.css') }}">
<!--<link rel="stylesheet" href="{{ asset('adminLTE/dist/css/AdminLTE.min.css') }}">-->
<!--<link rel="stylesheet" href="{{ asset('adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">-->
<link rel="stylesheet" href="{{ asset('css/slick.css') }}">
<link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
<link rel="stylesheet" href="{{ asset('css/analogs.css') }}?t={{ time() }}">
<div class="menu_corporate"><a href="{{ route('corporate.index') }}">личные данные</a> <a href="{{ route('corporate.regulations.index') }}">регламенты</a> <a class="selected" href="{{ route('corporate.analogs.index') }}">товары аналоги</a> </div>
<div class="analogs">
    @if ($message)
    <div class="message">
        <p>Данные отправлены в 1С <a href="{{ route('corporate.analogs.index') }}">Создать новый аналог</a></p>
    </div>
    @else
    <form method="POST" enctype="multipart/form-data" action="{{ route('corporate.analogs.send') }}/">
        {{ csrf_field() }}
        <input type="hidden" name="group_id" value="{{ old('group_id', $analogInfo['group_id'] ?? NULL) }}">
        <input type="hidden" name="product_id" value="{{ old('product_id', $analogInfo['product_id'] ?? NULL) }}">
        <input type="hidden" name="shop_type" value="{{ old('shop_type', $analogInfo['shop_type'] ?? NULL) }}">
        <input type="hidden" name="detected" value="0">
        
        @if(count($breaks))
        <div class="form_block">
            <p><label for="shop">Служебный перерыв</label></p>
            <select name="break" style="width:100%" class="form-control select2 select2-hidden-accessible "> 
                <option value="">&nbsp;</option>
                @foreach($breaks as $break)
                    <option data-used="{{ $break['used'] }}" value="{{ $break['code1C'] }}" {{ $break['code1C'] == old('break', $analogInfo['break'] ?? NULL) ? 'selected':'' }}>{{ date('Y-m-d',strtotime($break['time_begin'])).' / '.date('H:i',strtotime($break['time_begin'])).($break['time_end'] ? ' - '.date('H:i',strtotime($break['time_end'])) : '' ) }}</option>
                @endforeach
            </select>
        </div>
        @endif
        
        <div class="form_block">
            <p><label for="shop">Магазин конкурента</label><span class="error-line">{!! $errors->first('shop') !!}</span></p>
            <div class="form_element @if($errors->first('shop')) has_error @endif">
                <select name="shop" style="width:100%" class="form-control select2 select2-hidden-accessible "> 
                    <option value="">&nbsp;</option>
                    @foreach($shops as $shop)
                        <option data-type="{{ $shop->type }}" value="{{ $shop->id }}" {{ $shop->id == old('shop', $analogInfo['shop'] ?? NULL) ? 'selected':'' }}>{{ $shop->name }}{{ $shop->mall ? ', '.$shop->mall : '' }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form_block detected-info">
            Товар-аналог не обнаружен
        </div>
        <div class="form_block">
            <p><label for="product_id">Товар Компании</label><span class="error-line">{!! $errors->first('product_id') !!}</span></p>
            <div class="form_element @if($errors->first('product_id')) has_error @endif">
                <div class="tree" data-url="{{ route('corporate.analogs.products') }}/">
                    <ul class="first">
                        @foreach($tree as $group)
                            @include('corporate.include.groupsTree')
                        @endforeach
                    </ul>
                    <div class="products_wrapper">
                        <div class="products">
                        </div>
                    </div>  
                </div>
            </div>
        </div>
        <div class="form_block">
            <p><label for="type">Идентичный/Схожий</label><span class="error-line">{!! $errors->first('type') !!}</span></p>
            <div class="form_element @if($errors->first('type')) has_error @endif">
                <select name="type" style="width:100%" class="form-control select2 select2-hidden-accessible ">
                    <option value="">&nbsp;</option>
                    @foreach($types as $id => $type)
                        <option value="{{ $id }}" {{ $id === old('type', $analogInfo['type'] ?? NULL) ? 'selected':'' }}>{{ $type }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form_block color" style="@if(old('type', $analogInfo['type'] ?? NULL) == 1) {{ 'display:none;' }} @endif">
            <p><label for="colors">Цвет</label><span class="error-line">{!! $errors->first('colors') !!}</span></p>
            <div class="form_element @if($errors->first('colors')) has_error @endif">
               <select name="colors[]" style="width:100%" class="form-control select2 with_search select2-hidden-accessible " multiple>
                    @foreach($colors as $color)
                        <option value="{{ $color['code1C'] }}" {{ (collect(old('colors'))->contains($color['code1C'])) || in_array($color['code1C'], $analogInfo['colors'] ?? []) ? 'selected':'' }}>{{ $color['name'] }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form_block comment" style="@if(old('type', $analogInfo['type'] ?? NULL) == 1 || old('type', $analogInfo['type'] ?? NULL) == 2) {{ 'display:block;' }} @endif">
            <p><label for="shop">Комментарий</label><span class="error-line">{!! $errors->first('comment') !!}</span></p>
            <div class="form_element @if($errors->first('comment')) has_error @endif">
                <textarea class="" name="comment">{{ old('comment', $analogInfo['comment'] ?? NULL) }}</textarea>
            </div>
        </div>
        <div class="form_block">
            <p><label for="price">Цена</label><span class="error-line">{!! $errors->first('price') !!}</span></p>
            <div class="form_element @if($errors->first('price')) has_error @endif">
                <input type="text" name="price" class="" value="{{ old('price', $analogInfo['price'] ?? NULL) }}">
            </div>
        </div>
        <div class="form_block">
            <p style="padding-bottom: 3px;"><label for="photo">Фото</label><span class="error-line">{!! $errors->first('photo') !!}{!! $errors->first('url') !!}</span></p>
            <div class="form_element @if($errors->first('photo')) has_error @endif @if($errors->first('url')) has_error @endif">
                <div class="photo-container">
                    @if(isset($analogInfo['photo']) && $analogInfo['photo'])
                        <img src="{{ $analogInfo['photo'] }}">
                    @else
                        <input type="file" name="photo" class="">
                    @endif
                </div>
                <input style="display:none;" type="text" name="url" class="" value="{{ old('url', $analogInfo['url'] ?? NULL) }}">
            </div>
        </div>
        <div class="form_block">
            @if(count($analogInfo))
                <a class="" href="{{ route('corporate.analogs.index') }}">Назад</a>
            @else
                <input type="submit" class="yellow_submit" name="send" value="Отправить">
            @endif
            <a class="doc-list" data-url="{{ route('corporate.analogs.doclist') }}/" href="#">Список документов</a>
        </div>
    </form>
    <div class="doc-list-container">
        
    </div>
    @endif
</div>

<script src="{{ asset('adminLTE/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/slick.min.js') }}"></script>
<script src="{{ asset('js/analogs.js') }}?t={{ time() }}"></script>

@endsection