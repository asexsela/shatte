@if (count($analogs) == 0)
    <p style="color: #ecb800 !important;">Документы осутствуют</p>
@else
    <p style="margin-bottom: 5px;">Документы за последние 2 дня:</p>
@endif
@foreach($analogs as $analog)
    <p><a href="{{ route('corporate.analogs.index', $analog['code1C']) }}">{{ date('Y-m-d H:i',strtotime($analog['date'])) }} - {{ $analog['shop'] }}</a></p>
@endforeach