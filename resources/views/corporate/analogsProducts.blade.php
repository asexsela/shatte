@foreach($products as $product)
    <div class="product_img_wrapper">
        <img class="close" src="/img/close.png">
        <p>{{ $product['name'] }}</p>
        <img class="product_img" data-id="{{ $product['code1C'] }}" src="{{ config('settings.filePaths.products').$product['photo'] }}?t={{ date('Ym') }}">
    </div>
@endforeach
@if (count($products) == 0)
    <p style="color: #ecb800 !important;">Товаров данной группы нет в наличии</p>
@endif