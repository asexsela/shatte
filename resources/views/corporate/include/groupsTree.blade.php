<li class="" >
    <a class="{{ $group['class'] }}" data-code1C="{{ $group['code1C'] }}" href="#"><span>{{ $group['name'] }}</span></a>
    <span class="clear"></span>
    @if (isset($group['children']))
        <ul>
        @foreach($group['children'] as $group)
            @include('corporate.include.groupsTree')
        @endforeach
        </ul>
    @endif
</li>



