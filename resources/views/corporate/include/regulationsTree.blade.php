<li class="{{ $regulation['class'] }}" >
    @if($regulation['isGroup'])
        {{ $regulation['name'] }}
        @if($regulation['inc'] == 1)
            <span class="date_reg">Дата<br>изменения</span>
            <span class="clear"></span>
        @endif
    @else
        <a class="fancyFrame" href="{{ $regulation['code'] }}">{{ $regulation['name'] }}</a>
        <span class="date_reg {{ $regulation['dateClass'] }}">{{ $regulation['date'] }}</span>
        <span class="clear"></span>
    @endif
    @if (isset($regulation['children']))
        <ul>
        @foreach($regulation['children'] as $regulation)
            @include('corporate.include.regulationsTree')
        @endforeach
        </ul>
    @endif
</li>



