@extends('layouts.templateCorporate')

@section('content')

@if(isset($message))
<p>{!! $message !!}</p>
@endif

<div class="auth-form"> <h1>Авторизация</h1>
<div class="bx-system-auth-form">

<form method="post" action="{{ route('corporate.login') }}/">
    {{ csrf_field() }}
    @if (!$errors->isEmpty())
    <div class="error_auth_block">
        <div class="error_close"></div>
        @foreach($errors->all() as $error)
            <p class="error_auth">{!! $error !!}</p>
        @endforeach
    </div>
    @endif
    
	
	<table width="95%">
		<tr>
			<td colspan="2">
			<p><label for="email">электронная почта</label></p>
			<input id="email" class="text" type="text" name="email" maxlength="50" value="{{ old('email') }}" size="17" /></td>
		</tr>
		<tr>
			<td colspan="2">
			<p><label for="password">пароль</label></p>
			<input id="password" class="text" type="password" name="password" maxlength="50" value="{{ old('password') }}" size="17" />
			</td>
		</tr>
                <tr>
			<td colspan="2" style="width:100%;"><input type="submit" class="yellow_submit" name="Login" value="войти"><noindex><a class="remember-link" href="{{ route('corporate.remember.index') }}" rel="nofollow">Забыли пароль?</a></noindex><span class="clear">
			</td>
		</tr>
		<tr>
			<td colspan="2" class="registration-link">
			<span class="registration-link-a"><noindex><a href="/corporate/registration/" rel="nofollow">Зарегистрироваться</a></noindex></span><br />
			</td>
		</tr>
	</table>
</form>
</div> </div>
@endsection