@extends('layouts.templateCorporate')

@section('content')
<div class="menu_corporate"><a class="selected" href="{{ route('corporate.index') }}">личные данные</a> <a href="{{ route('corporate.regulations.index') }}">регламенты</a> <a href="{{ route('corporate.analogs.index') }}">товары аналоги</a> </div>
<div class="user_corporate_info">
    <p>ФИО</p>
    <p class="yellow">{{ $user->lastName }} {{ $user->name }} {{ $user->secondName }}</p>
    <p>Дата рождения</p>
    <p class="yellow">{{ date('d.m.Y',strtotime($user->dateBirthday)) }}</p>
    <p>Мобильный телефон</p>
    <p class="yellow">{{ $user->phone }}</p>
        <p>Электронная почта</p>
    <p class="yellow">{{ $user->email }}</p>
</div>
@endsection