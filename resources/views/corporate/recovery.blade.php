@extends('layouts.templateCorporate')

@section('content')
<br>
<h2>Восстановление доступа</h2>

@if(isset($errorMessage))
<p>{!! $errorMessage !!}</p>
@endif

<form id="remember_from_corporate" class="default-evona-form corporate" action="{{ route('corporate.recovery') }}/" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="code" value="{{ $code }}" />
    <input type="hidden" name="id" value="{{ $id }}" />
    <p>Придумайте новый пароль *</p>
    <p><input id="password1" class="text_input" type="password" name="password1" value="{{ old('password1') }}" /> <span class="error_text">{{ $errors->first('password1') }}</span></p>
    <p>Пароль ещё раз *</p>
    <p><input id="password2" class="text_input" type="password" name="password2" value="{{ old('password2') }}" /> <span class="error_text">{{ $errors->first('password2') }}</span></p>
    <p><input class="submit" type="submit" name="submit_email" value="Подтвердить" /> <span class="button-href-cancel corporate"><a href="{{ route('corporate.index') }}">Назад</a></span> </p>
</form>

@endsection