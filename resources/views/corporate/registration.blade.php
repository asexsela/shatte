@extends('layouts.templateCorporate')

@section('content')

<h2>Регистрация</h2>

    @if (!isset($email))
	<form id="reg_from_corporate" class="default-evona-form corporate" action="{{ route('corporate.registration') }}/" method="post">
            {{ csrf_field() }}
            <p>Email *</p>
            <p class="ajax_load_cont_p"><input id="email" class="text_input" type="text" value="{{ old('email') }}" name="email" /> <span class="error_text_show">{{ $errors->first('email') }}</span></p>
            <p>Придумайте пароль *</p>
            <p><input id="password1" class="text_input" type="password" name="password1" value="{{ old('password1') }}" /> <span class="error_text">{{ $errors->first('password1') }}</span></p>
            <p>Пароль ещё раз *</p>
            <p><input id="password2" class="text_input" type="password" name="password2" value="{{ old('password2') }}" /> <span class="error_text">{{ $errors->first('password2') }}</span></p>
            <p><input class="submit" type="submit" name="submit_email" value="Зарегистрироваться" /> <span class="button-href-cancel corporate"><a href="{{ route('corporate.index') }}">Назад</a></span> </p>
	</form>
    @else
        <div class="success">
            <p>На указанный Вами адрес электронной почты {{ $email }} отправлено письмо для подтверждения e-mail адреса.</p>
	</div>
    @endif

@endsection