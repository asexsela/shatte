@extends('layouts.templateCorporate')

@section('content')
<div class="menu_corporate"><a href="{{ route('corporate.index') }}">личные данные</a> <a class="selected" href="{{ route('corporate.regulations.index') }}">регламенты</a> <a href="{{ route('corporate.analogs.index') }}">товары аналоги</a> </div>
<div class="regulations_menu">
    <ul class="first">
        @foreach($tree as $regulation)
            @include('corporate.include.regulationsTree')
        @endforeach
    </ul>
</div>
@endsection