@extends('layouts.templateCorporate')

@section('content')
<br>


<h2>Восстановление доступа</h2>

<form id="remember_from_corporate" class="default-evona-form corporate" action="{{ route('corporate.remember') }}/" method="post">
    {{ csrf_field() }}
    <p>Ваша электронная почта</p>
    <p><input id="remember_from_corporate_email" class="text_input" value="{{ old('email') }}" type="text" name="email"> <span class="error_text">{!! $errors->first('email') !!}</span></p>
    <p><input class="submit" type="submit" name="remebmer" value="Сменить пароль"> <span class="button-href-cancel corporate"><a href="{{ route('corporate.index') }}">Назад</a></span> </p>
</form>

@endsection