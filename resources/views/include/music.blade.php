<div class="audio-player">
    @foreach($music as $sound)
        <div data-id="{{ $sound->id }}" data-volume="{{ $sound->volume / 100 }}" class="button paused"></div>
        <audio preload="auto" src="{{ $path.$sound->name }}"  id="mp3-{{ $sound->id }}" loop></audio>
    @endforeach
</div>