﻿@extends('layouts.templateMain')

@section('content')
               
    <div class="screens-body">
        <div class="screen main-screen" id="main-screen" data-anime="default">
            <div class="screen-body main-screen-body">
                <div class="screen-body__wrapper">
                    <div class="bg bg--left"></div>
                    <div class="bg bg--right"></div>

                    <img class="girl-animation screen-1-girl-1" src="{{ asset('/static/images/screen_1_girl_1.png') }}" alt="alt" data-easing="easeInOutQuart" data-anime="200"/>
                    <img class="girl-animation screen-1-girl-2" src="{{ asset('/static/images/screen_1_girl_2.png') }}" alt="alt" data-easing="easeInOutQuart" data-anime="-200"/>

                    <svg class="screen-1-layers-1 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="890" data-easing="easeInOutQuart" data-anime="200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-1') }}"></use>
                    </svg>
                    <svg class="screen-1-layers-2 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="838" data-easing="easeInOutQuart" data-anime="-200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-2') }}"></use>
                    </svg>
                    <div class="helper">
                        <span class="helper__text">Используйте скролл</span><span class="helper__line"><i></i></span><b class="helper__text">вниз</b>
                    </div>
                </div>
            </div>
        </div>
        <div class="screen company-screen" id="company-screen" data-anime="default">
            <div class="screen-body company-screen-body">
                <div class="screen-body__wrapper">
                    <div class="bg bg--left"></div>
                    <div class="bg bg--right"></div>
                    <svg class="screen-1-layers-1 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="890" data-easing="easeInOutQuart" data-anime="200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-1') }}"></use>
                    </svg>
                    <svg class="screen-1-layers-2 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="838" data-easing="easeInOutQuart" data-anime="-200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-2') }}"></use>
                    </svg>
                    <div class="screen-text-content">
                        <p class="screen-text-content__title text-animate" data-easing="easeOutCubic" data-anime="300">Доступная мода <br/>для современных женщин</p>
                        <div class="screen-text-content__text text-animate" data-easing="easeOutCubic" data-anime="300">
                            <p>Компания Shatte была создана в 2008 году. Мы ставили перед собой амбициозную задачу: сделать моду доступной современным женщинам. В коллекциях Shatte достигается тонкий компромисс между уникальностью, характерной для высокой моды, и комфортом, которого иногда так не хватает в повседневности.</p>
                        </div>
                    </div>
                    <img class="girl-animation screen-2-girl" src="{{ asset('/static/images/screen-2-girl.png') }}" alt="alt" data-easing="easeInOutQuart" data-anime="-200"/>
                </div>
            </div>
        </div>
        <div class="screen advantage-screen" id="advantage-screen" data-anime="default">
            <div class="screen-body advantage-screen-body">
                <div class="screen-body__wrapper">
                    <div class="bg bg--left"></div>
                    <div class="bg bg--right"></div>
                    <svg class="screen-1-layers-1 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="890" data-easing="easeInOutQuart" data-anime="200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-1') }}"></use>
                    </svg>
                    <svg class="screen-1-layers-2 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="838" data-easing="easeInOutQuart" data-anime="-200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-2') }}"></use>
                    </svg>
                    <div class="advantage-screen-content">
                        <p class="advantage-screen-content__header title-animation">Shatte сегодня это</p>
                        <div class="advantage-screen-content-advantages">
                            <div class="advantage-screen-content-advantage">
                                <p class="advantage-screen-content-advantage__count"><span class="advantage-screen-content-advantage__sup">Более</span><span class="advantage-screen-content-advantage__int count-number" data-speed="2000" data-max="160">0</span></p>
                                <p class="advantage-screen-content-advantage__text">Сотрудников</p>
                            </div>
                            <div class="advantage-screen-content-advantages__line left"></div>
                            <div class="advantage-screen-content-advantage">
                                <p class="advantage-screen-content-advantage__count"><span class="advantage-screen-content-advantage__sup">Более</span><span class="advantage-screen-content-advantage__int count-number" data-speed="2000" data-max="1000">0</span></p>
                                <p class="advantage-screen-content-advantage__text">Моделей одежды и акссесуаров в год</p>
                            </div>
                            <div class="advantage-screen-content-advantages__line right"></div>
                            <div class="advantage-screen-content-advantage">
                                <p class="advantage-screen-content-advantage__count"><span class="advantage-screen-content-advantage__int count-number" data-speed="2000" data-max="30">0</span></p>
                                <p class="advantage-screen-content-advantage__text">Магазинов</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="screen client-screen" id="client-screen" data-anime="default">
            <div class="screen-body client-screen-body">
                <div class="screen-body__wrapper">
                    <div class="bg bg--left"></div>
                    <div class="bg bg--right"></div>
                    <svg class="screen-1-layers-1 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="890" data-easing="easeInOutQuart" data-anime="200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-1') }}"></use>
                    </svg>
                    <svg class="screen-1-layers-2 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="838" data-easing="easeInOutQuart" data-anime="-200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-2') }}"></use>
                    </svg>
                    <div class="screen-text-content">
                        <p class="screen-text-content__title text-animate" data-easing="easeOutCubic" data-anime="300">Найди свой стиль <br/>вместе с Shatte</p>
                        <div class="screen-text-content__text text-animate" data-easing="easeOutCubic" data-anime="300">
                            <p>Девушка в стиле Shatte – уверенная в себе девушка, которая ищет комфорта и уюта, но не готова жертвовать индивидуальностью и стилем. Наши клиенты всегда выглядят привлекательно и современно в любой одежде, ведь они знают, как проявить свою оригинальность, дополнив образ разнообразными аксессуарами.</p>
                        </div>
                    </div>
                    <img class="girl-animation screen-4-girl" src="{{ asset('/static/images/screen-4-girl.png') }}" alt="alt" data-easing="easeInOutQuart" data-anime="-200"/>
                </div>
            </div>
        </div>
        <div class="screen shop-screen" id="shop-screen" data-anime="default">
            <div class="screen-body shop-screen-body">
                <div class="screen-body__wrapper">
                    <div class="bg bg--left"></div>
                    <div class="bg bg--right"></div>
                    <svg class="screen-1-layers-1 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="890" data-easing="easeInOutQuart" data-anime="200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-1') }}"></use>
                    </svg>
                    <svg class="screen-1-layers-2 path-animation" xmlns="http://www.w3.org/2000/svg" width="1749" height="838" data-easing="easeInOutQuart" data-anime="-200">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#layers-2') }}"></use>
                    </svg>
                    <div class="screen-text-content">
                        <p class="screen-text-content__title text-animate" data-easing="easeOutCubic" data-anime="300">Магазины Shatte</p>
                        <div class="screen-text-content__text text-animate" data-easing="easeOutCubic" data-anime="300"> 
                            <p>Магазины Shatte отражают общую концепцию бренда – вас ждет светлоe, просторное и комфортное пространство, в котором вы без труда сможете не только подобрать все необходимое, но и немного расслабиться и отдохнуть от повседневной суеты. </p>
                            <p>Однажды побывав в любом из магазинов Shatte, вы легко узнаете нас в другом городе или торговом центре.</p>
                        </div>
                    </div>
                    <div class="gallery">
                        <div class="gallery__item item1"><img class="gallery__img" src="{{ asset('static/images/img1.png') }}" alt=""/></div>
                        <div class="gallery__item item2"><img class="gallery__img" src="{{ asset('static/images/img2.png') }}" alt=""/></div>
                        <div class="gallery__item item3"><img class="gallery__img" src="{{ asset('static/images/img3.png') }}" alt=""/></div>
                        <div class="gallery__item item4"><img class="gallery__img" src="{{ asset('static/images/img4.png') }}" alt=""/></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="screen contacts-screen" id="contacts-screen" data-anime="default">
            <div class="screen-body contacts-screen-body">
                <div class="screen-body__wrapper">
                    <div class="bg bg--left"></div>
                    <div class="bg bg--right"></div>
                    <div class="screen-contacts-wrapper">
                        <div class="map"><i class="map__marker map__marker--1"></i><i class="map__marker map__marker--2"></i><i class="map__marker map__marker--3"></i><i class="map__marker map__marker--4"></i><i class="map__marker map__marker--5"></i><i class="map__marker map__marker--6"></i><i class="map__marker map__marker--7"></i><i class="map__marker map__marker--8"></i><i class="map__marker map__marker--9"></i><i class="map__marker map__marker--10"></i><i class="map__marker map__marker--11"></i><i class="map__marker map__marker--12"></i><i class="map__marker map__marker--13"></i>
                        <img class="map__img" src="{{ asset('static/images/map.png') }}" alt=""/></div>
                        <div class="screen-contacts-wrapper__body">
                            <div class="contacts">
                                <p class="contacts__title">Контакты</p>
                                <p class="contacts__desc">30 магазинов представлены в лучших торговых центрах в 20 городах России</p>
                                <p class="contacts__caption">Головной офис в Ярославле</p>
                                <p class="contacts__desc">150040, ул. Володарского, 62 корпус 2</p>
                                <div class="contact">
                                    <p class="contact__caption">Телефоны</p>
                                    <div class="contact__wrap"><a class="contact__value" href="tel:8 (4852) 49-19-50">8 (4852) 49-19-50</a><a class="contact__value" href="tel:8 (4852) 49-19-52">8 (4852) 49-19-52</a><a class="contact__value" href="tel:8 (4852) 49-19-53">8 (4852) 49-19-53</a></div>
                                </div>
                            </div>
                            <div class="cities">
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Ярославль</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Ярославль</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Сыктывкар</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Сыктывкар</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Сургут</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Сургут</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Владимир</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Владимир</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Рязань</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Рязань</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Череповец</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Череповец</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Калининград</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Калининград</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Киров</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Киров</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Тверь</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Тверь</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Вологда</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Вологда</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Великий</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Великий</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Смоленск</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Смоленск</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Кострома</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Кострома</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Новгород</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Новгород</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Архангельск</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Архангельск</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Мурманск</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Мурманск</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Астрахань</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Астрахань</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Пенза</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Пенза</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Саратов</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Саратов</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cities__item"><a class="cities__link" href="#"><span class="hover">Петрозаводск</span></a>
                                    <div class="hide-content">
                                        <div class="contact-info">
                                            <p class="contact-info__title">Петрозаводск</p>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                            <div class="contact-info__wrapper">
                                                <p class="contact-info__text"><span class="contact-info__value">ТЦ «Серебряный город»</span></p>
                                                <p class="contact-info__text"><span>Адрес:</span><span class="contact-info__value">ул. 8 Марта, д. 32</span></p>
                                                <p class="contact-info__text"><span>Телефон:</span><span class="contact-info__value">+ 7 (4932) 93-94-73</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="screen-contacts-wrapper__footer">
                            <p class="footer-copirait">© 2019 «Shatte»</p><a class="made-by" href="http://place-start.ru" target="_blank"><span class="made-by__text">Сделано в</span>
                                <svg class="made-by__svg" xmlns="http://www.w3.org/2000/svg">
                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#ps') }}"></use>
                                </svg></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection