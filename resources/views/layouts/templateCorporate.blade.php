
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Корпоративный раздел</title>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/img/old/v2/img/favs/144.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/img/old/v2/img/favs/114.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/img/old/v2/img/favs/72.png">
<link rel="apple-touch-icon-precomposed" href="/img/old/v2/img/favs/57.png">
<link rel="shortcut icon" href="/img/old/v2/img/favs/32.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="/favicon.ico" type="image/x-icon">
<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!--[if lt IE 9]><script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
<link rel="stylesheet" href="{{ asset('css/corporate.css') }}?t={{ date('Ymd') }}">
<link rel="stylesheet" href="{{ asset('css/jquery.fancybox.corporate.css') }}?t={{ date('Ymd') }}">
<script src="{{ asset('js/jquery.min.js') }}"></script>
</head>
<body>
<div id="container">
		<div class="site_width">
		<div class="empty_pre_header"></div>
		<div class="left_col">
			<header>
				<a href="/"><img src="{{ asset('img/old/logo.png') }}" alt="evona fashion" title="evona fashion"></a>
			</header>
			
<!--		<nav class="main_menu">
			<ul>
				<li><a href="{{ route('vacancyIndex') }}">Вакансии</a></li>
			</ul>
		</nav>-->
		</div>
		<div class="right_col">
                    <div class="user_info">
		@if (Auth::check() && Auth::user()->isEmployee())	
                            <p><a class="name_user_text" href="/corporate/">{{ Auth::user()->name }}</a>, добро пожаловать!
            &nbsp; <a href="{{ route('corporate.logout') }}">Выход</a></p>
        @endif
							</div> 
<h1 style="font-size: large;">Корпоративный раздел</h1>
@yield('content')
		</div>
		<div class="clear"></div>
		<div class="empty_pre_footer"></div>
	</div>
</div>
<footer class="site_width">
	<p>© 2010 - {{ date('Y') }} <b>evona</b></p>
        
       
<script src="{{ asset('js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('plugins/fancybox/jquery.fancybox.pack.js') }}"></script>
<script src="{{ asset('js/jquery.selection.js') }}"></script>
<script src="{{ asset('js/corporate.js') }}"></script>
</footer>
</body>
</html>