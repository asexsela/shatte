<!DOCTYPE html>
<html>
<head>


    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title','Evona')</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('/site.webmanifest') }}">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="{{ asset('static/css/vendors.css') }}"/>
    <link rel="stylesheet" href="{{ asset('static/css/style.css') }}"/>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111612838-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-111612838-1');
    </script>

</head>
<body onclick="">
    <nav class="mobile-menu">
        <ul>
            <li><a href="#" data-anchore="main-screen">Главная</a></li>
            <li><a href="#" data-anchore="company-screen">О компании</a></li>
            <li><a href="#" data-anchore="advantage-screen">Сегодня мы это</a></li>
            <li><a href="#" data-anchore="client-screen">Наши клиенты</a></li>
            <li><a href="#" data-anchore="shop-screen">Магазины</a></li>
            <li><a href="#" data-anchore="contacts-screen">Контакты</a></li>
        </ul>
    </nav>
    <div class="wrapper">
    <div class="screens">
        <img class="logo-bg" src="{{ asset('static/images/logo-bg.svg') }}" alt=""/>
            <div class="screens-header">

                <nav class="header-menu">
                    @if (Request::is('/'))
                        
                        <a class="header-menu__item hover" href="#" data-to-screen="company-screen">О нас</a>
                        <a class="header-menu__item hover" href="#" data-to-screen="contacts-screen">Контакты</a>
                    
                    @else

                        <a class="header-menu__item hover" href="/#company-screen">О нас</a>
                        <a class="header-menu__item hover" href="/#contacts-screen">Контакты</a>

                    @endif
                </nav>

                <a class="logo {{ Request::is('/') ? '' : 'pages-logo' }}" href="/" style="visibility: hidden;">
                    <svg class="logo__svg" role="img">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#logo') }}"></use>
                    </svg>
                    @if (Request::is('/'))
                        <span class="logo__text">Сеть магазинов женской одежды</span>
                    @endif
                </a>
                <div class="nav-peoples">
                    <a class="key-lc hover" href="{{ route('personalIndex') }}">
                        <svg class="key-lc__svg" role="img" width="25" height="12">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#key') }}"></use>
                        </svg><span class="key-lc__text">Личный кабинет</span>
                    </a>
                    @auth
                    <a class="key-lc hover line" href="{{ route('logout') }}">   
                        <span class="exit_button">Выйти</span>
                    </a>
                    @endauth
                </div>
                <a class="mobile-menu-open" href="#"><span class="mobile-menu-open__button"></span></a>
            </div>

            @yield('content')

        </div>
        @if (Request::is('/'))
            <div class="screens-nav">
                <div class="screens-nav-item">
                    <div class="screens-nav-item__body"><span class="screens-nav-item__number">01</span>
                        <p class="screens-nav-item__text"><span>главная</span></p>
                    </div>
                </div>
                <div class="screens-nav-item">
                    <div class="screens-nav-item__body"><span class="screens-nav-item__number">02</span>
                        <p class="screens-nav-item__text"><span>О компании</span></p>
                    </div>
                </div>
                <div class="screens-nav-item">
                    <div class="screens-nav-item__body"><span class="screens-nav-item__number">03</span>
                        <p class="screens-nav-item__text"><span>Сегодня мы это</span></p>
                    </div>
                </div>
                <div class="screens-nav-item">
                    <div class="screens-nav-item__body"><span class="screens-nav-item__number">04</span>
                        <p class="screens-nav-item__text"><span>Наши клиенты</span></p>
                    </div>
                </div>
                <div class="screens-nav-item">
                    <div class="screens-nav-item__body"><span class="screens-nav-item__number">05</span>
                        <p class="screens-nav-item__text"><span>Магазины</span></p>
                    </div>
                </div>
                <div class="screens-nav-item">
                    <div class="screens-nav-item__body"><span class="screens-nav-item__number">06</span>
                        <p class="screens-nav-item__text"><span>Контакты</span></p>
                    </div>
                </div>
            </div>
        @endif


    </div>

<div class="modal" id="modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <svg role="img" width="14" height="14">
                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#close') }}"></use>
                    </svg>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-body__container"></div>
            </div>
        </div>
    </div>
</div>



<script src="{{ asset('static/js/main.js') }}"></script>
<script src="{{ asset('static/js/screens.js') }}"></script>
<script src="{{ asset('static/js/script.js') }}"></script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter49170625 = new Ya.Metrika2({
                        id:49170625,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/tag.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks2");
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/49170625" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->
    
</body>
</html>