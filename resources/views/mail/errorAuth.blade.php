<html>
Посетитель сайта <a href="http://evonafashion.ru">evonafashion.ru</a>: {{ $data['name'] }} <a href="mailto:{{ $data['email'] }}">{{ $data['email'] }}</a><br>
Номер карты: {{ $data['cardNumber'] }}<br>
Номер телефона: {{ $data['phone'] }}<br>
Дата отправки письма администратору: {{ date('Y-m-d H:i:s') }}<br><br>
Введённые данные в форму авторизации:<br>
Номер карты: {{ $data['oldCardNumber'] }}<br>
Номер телефона: {{ $data['oldPhone'] }}<br>
</html>