Посетитель сайта evonafashion.ru: {{ $data['name'] }} {{ $data['email'] }}
Номер карты: {{ $data['cardNumber'] }}
Номер телефона: {{ $data['phone'] }}
Дата отправки письма администратору: {{ date('Y-m-d H:i:s') }}

Введённые данные в форму авторизации:
Номер карты: {{ $data['oldCardNumber'] }}
Номер телефона: {{ $data['oldPhone'] }}