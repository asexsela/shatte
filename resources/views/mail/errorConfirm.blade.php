<html>
Посетитель сайта <a href="http://evonafashion.ru">evonafashion.ru</a>: {{ $data['lastName'] }} {{ $data['name'] }} {{ $data['secondName'] }} <a href="mailto:{{ $data['email'] }}">{{ $data['email'] }}</a><br>
Номер карты: {{ $data['cardNumber'] }}<br>
Номер телефона: {{ $data['phone'] }}<br>
Дата отправки письма с кодом подтверждения: {{ date('Y-m-d H:i:s') }}
</html>