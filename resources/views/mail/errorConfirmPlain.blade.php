Посетитель сайта evonafashion.ru: {{ $data['lastName'] }} {{ $data['name'] }} {{ $data['secondName'] }} {{ $data['email'] }}
Номер карты: {{ $data['cardNumber'] }}
Номер телефона: {{ $data['phone'] }}
Дата отправки письма с кодом подтверждения: {{ date('Y-m-d H:i:s') }}