<html>
Город:<br>
<b>{{ $data['city'] }}</b><br>
Должность:<br>
<b>{{ $data['type'] }}</b><br>
Как срочно:<br>
<b>{{ $data['time'] }}</b><br>
Имя:<br>
<b>{{ $data['name'] }}</b><br>
Фамилия:<br>
<b>{{ $data['lastName'] }}</b><br>
Возраст:<br>
<b>{{ $data['age'] }}</b><br>
Опыт работы:<br>
<b>{{ $data['experience'] }}</b><br>
Телефон:<br>
<b>{{ $data['phone'] }}</b><br>
Email:<br>
<b><a href="mailto:{{ $data['email'] }}">{{ $data['email'] }}</a></b><br>
Фото:<br>
<b><a href="{{ $data['urlPhoto'] }}">Ссылка</a></b><br>
Резюме:<br>
<b>@if(isset($data['resume'])) <a href="{{ $data['urlResume'] }}">Ссылка</a> @else Не приложено @endif</b><br>
Комментарий:<br>
<b>{{ $data['comment'] }}</b><br>
</html>