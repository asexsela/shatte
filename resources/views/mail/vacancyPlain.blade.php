Город:
{{ $data['city'] }}
Должность:
{{ $data['type'] }}
Как срочно:
{{ $data['time'] }}
Имя:
{{ $data['name'] }}
Фамилия:
{{ $data['lastName'] }}
Возраст:
{{ $data['age'] }}
Опыт работы:
{{ $data['experience'] }}
Телефон:
{{ $data['phone'] }}
Email:
{{ $data['email'] }}
Фото:
{{ $data['urlPhoto'] }}
Резюме:
@if(isset($data['resume']))
{{ $data['urlResume'] }}
@else
Не приложено
@endif
Комментарий:
{{ $data['comment'] }}