@extends('layouts.templateMain')

@section('title', 'Изменение информации')

@section('content')

<style>
    html, body {
        overflow: auto;
    }
    .screens-body {
        height: auto;
    }
</style>

<div class="screens-body ">
        <section class="personal">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>Изменение информации</h1>
                    </div>
                    <div class="col-12">
                        <div class="second-nav">
                            <a href="{{ route('purchases') }}/" class="second-nav-link">история покупок</a>
                            <a href="" class="second-nav-link  second-nav-link_active">мои данные</a>
                        </div>
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-lg-4 col-sm-8 col-12">


                        <div class="personal-info">
                            <div class="form-line">
                                <label for="login" class="site-label">Имя</label>
                                <div class="personal-info-value">{{ $user->lastName }} {{ $user->name }} {{ $user->secondName }}</div>
                            </div>
                            <div class="form-line">
                                <label for="phone" class="site-label">Телефон</label>
                                <div class="personal-info-value">+7{{ $user->phone }}</div>
                            </div>
                            <div class="form-line">
                                <label for="email" class="site-label">Email</label>
                                <div class="personal-info-value">{{ $user->email }}</div>
                            </div>
                            <div class="form-line">
                                <label class="site-label">Текущая скидка</label>
                                <div class="personal-info-value">{{ $user->discount }}%</div>                
                            </div>
                            <div class="form-line form-line_center">
                                <button class="site-button site-button_small change-personal-info">Я хочу изменить данные</button>
                            </div>
                        </div>

                        <div class="personal-info-edit">
                            <form action="{{ route('info') }}/" method="post" class="personal-change-form">
                                {{ csrf_field() }}
                                <div class="form-line">
                                    <label for="pLastName" class="site-label">Фамилия:</label>
                                    <div class="input-holder @if($errors->first('lastName')) has_error text-first-name  @endif">
                                        <input type="text" class="site-input site-input_personal site-input_big checkLAST_NAME" id="pLastName" name="lastName" value="{{ old('lastName', $user->lastName) }}">
                                        @if ($errors->first('lastName'))
                                            <div class="error-box">{{ $errors->first('lastName') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-line">
                                    <label for="pName" class="site-label">Имя:</label>
                                    <div class="input-holder @if($errors->first('name')) has_error  @endif">
                                        <input type="text" class="site-input site-input_personal site-input_big checkNAME" id="pName" name="name" value="{{ old('name', $user->name) }}">
                                        @if ($errors->first('name'))
                                            <div class="error-box">{{ $errors->first('name') }}</div>    
                                        @endif
                                    </div>
                                </div>
                                <div class="form-line">
                                    <label for="pSecondName" class="site-label">Отчество:</label>
                                    <div class="input-holder @if($errors->first('secondName')) has_error  @endif">
                                    <input type="text" class="site-input site-input_personal site-input_big checkSECOND_NAME" id="pSecondName" name="secondName" value="{{  old('secondName', $user->secondName) }}">
                                    @if ($errors->first('secondName'))
                                        <div class="error-box">{{ $errors->first('secondName') }}</div>
                                    @endif
                                    </div>
                                </div>

                                <div class="form-line">
                                    <label for="pEmail" class="site-label">Email:</label>
                                    <div class="input-holder @if($errors->first('email')) has_error  @endif">
                                    <input type="text" class="site-input site-input_small site-input_personal checkEMAIL" id="pEmail" name="email" value="{{ old('email', $user->email) }}">
                                    @if ($errors->first('email'))
                                        <div class="error-box">{{ $errors->first('email') }}</div>
                                    @endif
                                </div>
                                </div>
                    
                                <div class="form-line form-line_center form-button">
                                    <button class="site-button site-button_small button-save-info" type="submit">Сохранить изменения</button>
                                    <a href="#" class="dotted-link personal-info-do-not-save">Не сохранять</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div> 


{{-- 
<div class="inner-page-content">

            <div class="page-title">
                <h1 class="page-title__h1">Изменение информации</h1>
                <div class="page-title_big">Изменение информации</div>
                <div class="page-title_small">Изменение информации</div>
            </div>
    
    <div class="second-nav">
        <a href="{{ route('purchases') }}/" class="second-nav-link ">история покупок</a>
        <a href="#" class="second-nav-link second-nav-link_active">мои данные</a>
    </div>

    <div class="personal-info @if (count($errors->all()) > 0) hide @endif">
            <div class="form-line">
                <label for="login" class="site-label">Имя:</label>
                <div class="personal-info-value">{{ $user->lastName }} {{ $user->name }} {{ $user->secondName }}</div>
            </div>
            <div class="form-line">
                <label for="phone" class="site-label">Телефон:</label>
                <div class="personal-info-value">+7{{ $user->phone }}</div>
            </div>
            <div class="form-line">
                <label for="email" class="site-label">Email:</label>
                <div class="personal-info-value">{{ $user->email }}</div>
            </div>
            <div class="form-line">
                <label class="site-label">Текущая скидка:</label>
                <div class="personal-info-value">{{ $user->discount }}%</div>

                                
            </div>
            <div class="form-line form-line_center">
                <button class="site-button site-button_small change-personal-info">Я хочу изменить данные</button>
            </div>
    </div>

    <div class="personal-info-edit @if (count($errors->all()) == 0) hide @endif">
        <form action="{{ route('info') }}/" method="post" class="personal-change-form">
            {{ csrf_field() }}
            <div class="form-line">
                <label for="pLastName" class="site-label">Фамилия:</label>
                <div class="input-holder @if($errors->first('lastName')) has_error  @endif">
                    <input type="text" class="site-input site-input_personal site-input_big checkLAST_NAME" id="pLastName" name="lastName" value="{{ old('lastName', $user->lastName) }}">
                    <div class="error-box">{{ $errors->first('lastName') }}</div>
                </div>
            </div>
            <div class="form-line">
                <label for="pName" class="site-label">Имя:</label>
                <div class="input-holder @if($errors->first('name')) has_error  @endif">
                    <input type="text" class="site-input site-input_personal site-input_big checkNAME" id="pName" name="name" value="{{ old('name', $user->name) }}">
                    <div class="error-box">{{ $errors->first('lastName') }}</div>
                </div>
            </div>
            <div class="form-line">
                <label for="pSecondName" class="site-label">Отчество:</label>
                <div class="input-holder @if($errors->first('secondName')) has_error  @endif">
                <input type="text" class="site-input site-input_personal site-input_big checkSECOND_NAME" id="pSecondName" name="secondName" value="{{  old('secondName', $user->secondName) }}">
                <div class="error-box">{{ $errors->first('secondName') }}</div>
                </div>
            </div>
<!--            <div class="form-line">
                <label for="pEmail" class="site-label">Телефон:</label>
                <div class="input-holder @if($errors->first('phone')) has_error  @endif">
                <span class="plus_seven" style="width:35px;margin-left:-35px;">+7</span>
                <input type="text" class="site-input site-input_small site-input_personal checkEMAIL" id="pEmail" name="phone" value="{{ old('phone', $user->phone) }}">
                <div class="error-box">{{ $errors->first('phone') }}</div>
                </div>
            </div>-->
            <div class="form-line">
                <label for="pEmail" class="site-label">Email:</label>
                <div class="input-holder @if($errors->first('email')) has_error  @endif">
                <input type="text" class="site-input site-input_small site-input_personal checkEMAIL" id="pEmail" name="email" value="{{ old('email', $user->email) }}">
                <div class="error-box">{{ $errors->first('email') }}</div>
            </div>
            </div>

            <div class="form-line form-line_center">
                <button class="site-button site-button_small button-save-info" type="submit">Сохранить изменения</button>

                <div id="fountainG">
                    <div id="fountainG_1" class="fountainG"></div>
                    <div id="fountainG_2" class="fountainG"></div>
                    <div id="fountainG_3" class="fountainG"></div>
                    <div id="fountainG_4" class="fountainG"></div>
                    <div id="fountainG_5" class="fountainG"></div>
                    <div id="fountainG_6" class="fountainG"></div>
                    <div id="fountainG_7" class="fountainG"></div>
                    <div id="fountainG_8" class="fountainG"></div>
                </div>
                <p>
                    <a href="#" class="dotted-link personal-info-do-not-save">Не сохранять</a>
                </p>
            </div>
        </form>
    </div>


            <div id="message_new_email_remember" class="hide">
            <p>При изменении Вами  адреса эл. почты на ,
                Вы не произвели его подтверждение – не была активирована ссылка в письме, которое было отправлено на адрес .
                Если у Вас сохранилось намерение изменить e-mail, то необходимо произвести подтверждение замены e-mail способом указанным выше, либо заново осуществить процедуру изменения e-mail.</p>
            <button class="site-button" onclick="$.fancybox.close(); $.get('/ajax/clear_new_email.php');">Хорошо</button>
        </div>
    


        </div> --}}

@endsection

