@extends('layouts.templateMain')

@section('content')

<div class="inner-page-content">

    <div class="page-title">
        <h1 class="page-title__h1">SMS</h1>
        <div class="page-title_big">SMS</div>
        <div class="page-title_small">SMS</div>
    </div>
	
    <p><strong>{{ $userName }}</strong>, для того чтобы авторизоваться на сайте, необходимо<br /> ввести в строку ниже SMS-код, отправленный на номер {{ $userPhone }}.</p>

    <form class="default-evona-form" action="{{ route('confirmSms') }}" method="post">
            {{ csrf_field() }}
            <p>Код из смс</p>
            <div class="input-holder">
                <p><input type="text" name="smsCode" id="smsCode" value="{{ old('smsCode') }}" class="site-input site-input_small"></p>
                <div class="error-line">{{ $errors->first('smsCode') }}</div>
            </div>
            <p><button type="submit" name="submit_email" class="site-button site-button-pad">Подтвердить</button>
    </form>
</div>

@endsection