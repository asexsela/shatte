<div style="width: 100%; max-width: 700px; text-align: center; line-height: 100%;">
    <div style="padding: 15px; text-align: center;">
        <form id="form">
            <div style="padding: 20px 30px; text-align: left;">
                <div id="vdiv1">
                    <b>{{ $question->name }}</b>
                    <div style="font-size: 11pt; margin-top: 10px; margin-bottom: 20px;">
                        @foreach($answers as $answer)
                            <label style="width:100%;display: inline-block;"><input type="radio" class="poll_answer"  name="answer" value="{{ $answer->id }}">{{ $answer->name }}</label>
                            <br>
                        @endforeach
                    </div>
                    <div class="mega-buttons" style="text-align: center;">
                        <button type="button" data-id="{{ $question->poll_id }}" data-url="{{ route('poll.next') }}/" data-list="{{ $question->list }}" data-old="{{ $question->id }}" class="mega-btn poll_next">Далее</button>
                    </div>
                </div>
                <div style="color: #F00; text-align: center; margin-top: 10px;" class="poll_error"></div>
            </div>
        </form>
    </div>
</div>