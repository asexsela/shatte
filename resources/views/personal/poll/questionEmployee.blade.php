<div style="width: 100%; max-width: 1200px; text-align: center; line-height: 100%;">
   <div style="background-color: #686868; padding: 15px; text-align: center;">
      <form id="form">
         <table width="100%" border="0" cellspacing="0" cellpadding="2">
            <tbody>
               <tr>
                  <td colspan="{{ count($employees) }}" align="left" valign="top">Отметьте продавца-консультанта, который общался с Вами ДО ТОГО МОМЕНТА, как Вы подошли к кассовой стойке для оплаты своей покупки <b style="color: red">&nbsp;*</b></td>
                  <td></td>
               </tr>
               <tr>
                   @foreach($employees as $employee)
                  <td valign="middle" align="center">
                     <label for="emp{{ $employee['id'] }}">
                        <img src="{{ $employee['photo'] }}" style="height: 250px; padding: 1px; border-radius: 10px; border: 2px solid #8A8A8A;" alt="">
                        <br>
                        <span class="">
                           &nbsp;<!-- 1-->{{ $employee['name'] }}&nbsp;
                        </span>
                     </label>
                     <br>
                     <input type="radio" id="emp{{ $employee['id'] }}" class="poll_answer" name="answer" value="{{ $employee['id'] }}">
                  </td>
                  @endforeach
               </tr>
            </tbody>
         </table>
          <div style="color: #F00; text-align: center; margin-bottom: 10px;" class="poll_error"></div>
      </form>
      <div class="mega-buttons"><button type="button" data-id="{{ $id }}" data-url="{{ route('poll.next') }}/" data-list="1" data-old="-1" class="mega-btn poll_next">Далее</button></div>
   </div>
</div>