<div style="width: 100%; max-width: 700px; text-align: center; line-height: 100%;"><div style="padding: 15px; text-align: center;"><p>{{ $question->name }}</p>
		<div class="mega-buttons">
                    @foreach($answers as $answer)
                        <button class="@if($answer->index == 1) mega-btn-2 @else mega-btn-2 @endif poll_start" data-id="{{ $question->poll_id }}" data-url="{{ route('poll.start') }}/" data-list = "{{ $answer->index }}" data-old="0">{{ $answer->name }}</button>
                    @endforeach
                </div></div></div>
