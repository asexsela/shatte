<div style="width: 100%; max-width: 700px; text-align: center; line-height: 100%;">
    <div style="padding: 15px; text-align: center;">
       
    
            <div style="padding: 20px 30px; text-align: left;">
                <div id="vdiv1">
                    <b>Для завершения опроса необходимо ввести SMS-код, отправленный на номер +7{{ $user->phone }}</b>
                    <div class="form-line" style="
    margin-top: 20px;
    text-align: center;
">
                <label for="name" class="site-label" style="
">Код из смс</label>
                <input type="text" id="smsCode" name="smsCode" class="site-input site-input_popup checkname poll_sms">
                <div class="error-line hide"></div>
            </div>
                    <div class="mega-buttons" style="text-align: center;">
                        <button type="button" data-id="{{ $poll->id }}" data-url="{{ route('poll.finish') }}/"  class="mega-btn poll_next">Завершить опрос</button> <button class="mega-btn-2" data-id="{{ $poll->id }}"  data-url="{{ route('poll.ignore') }}/" id="poll_ignore">Отмена</button>
                    </div>
                </div>
                <div style="color: #F00; text-align: center; margin-top: 10px;" class="poll_error"></div>
            </div>
       
    </div>
</div>