@extends('layouts.templateMain')

@section('title', 'Мои покупки')

@section('content')

    <style>
        html, body {
            overflow: auto;
        }
        .screens-body {
            height: auto;
        }
    </style>
    <div class="screens-body ">
        <section class="personal">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h1>Мои покупки</h1>
                    </div>
                    <div class="col-12">
                        <div class="second-nav">
                            <a href="#" class="second-nav-link second-nav-link_active">история покупок</a>
                            <a href="{{ route('infoIndex') }}/" class="second-nav-link">мои данные</a>
                        </div>
                    </div>
                    <div class="col-12">
                        @if($pollMessage)
                            <p style="align: center; color: red; font-size: 16px;">
                                <b>{{ $pollMessage }}</b>
                            </p>
                        @endif
                    </div>
                </div>
                <div class="row justify-content-md-center">
                    <div class="col-lg-8 col-12">
                        @if (count($purchases) > 0)
                            <div class="scrolls">
                                <table class="buys-table">
                                    <thead>
                                        <tr>
                                            <td class="buys-table-td_date">Дата покупки</td>
                                            <td class="buys-table-td_art-number">Артикул</td>
                                            <td class="buys-table-td_item">Покупка</td>
                                            <td class="buys-table-td_count">Кол-во</td>
                                            <td class="buys-table-td_price">Цена</td>
                                            <td class="buys-table-td_discount">Скидка</td>
                                            <td class="buys-table-td_with-discount">Со скидкой</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($purchases as $purchase)
                                            <tr class="buys-table__tr buys-table__tr_uneven">
                                                <td class="buys-table-td_date">{{ date('d.m.Y',strtotime($purchase->datePurchase)) }}</td>
                                                <td class="buys-table-td_art-number">{{ $purchase->vendorCode }}</td>
                                                <td class="buys-table-td_item">{{ $purchase->name }}</td>
                                                <td class="buys-table-td_count">{{ $purchase->quantity }}</td>
                                                <td class="buys-table-td_price">{{ $purchase->price }} <span class="b-rub">Р</span></td>
                                                <td class="buys-table-td_discount">{{ $purchase->discount }}%</td>
                                                <td class="buys-table-td_with-discount">{{ $purchase->finalPrice }}<span class="b-rub">Р</span></td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="buys-summary__holder">
                                <div class="buys-summary">
                                    Общая сумма покупок: <span class="buys-summary__summ">{{ number_format($totalPrice, 0,'',' ') }}<span class="b-rub">Р</span></span>
                                </div>
                            </div>
                        @else
                            <div class="error"><p><b>Вы не совершали покупок по дисконтной карте.</b></p></div>
                            Если вы обнаружили ошибку — <a href="#" class="dotted-link buys-info-mistake-form">сообщите нам</a>.
                        @endif
                    </div>
                    <div class="col-lg-7 col-12">
                        @if (count($purchases) != 0)
                            <div class="purchase_line d-none d-sm-none d-md-block">
                                <div class='colorLine' style='width:122px;'>
                                    <span class='pers' style='left: 52px;'>10%</span>
                                    <span class='cost' style='left:95px;'>10 000 <span class="b-rub">Р</span></span>
                                </div>
                                <div class='colorLine' style='width:305px;'>
                                    <span class='pers' style='left: 205px;'>15%</span>
                                    <span class='cost' style='left: 278px;'>25 000 <span class="b-rub">Р</span></span>
                                </div>
                                <div class='colorLine' style='width:454px;'>
                                    <span class='pers' style='left: 369px;'>20%</span>
                                    <span class='cost' style='left:430px;'>45 000 <span class="b-rub">Р</span></span>
                                </div>
                                <div class='colorLine last-child' style='width: 570px;'>
                                    <span class='pers' style='left: 504px;'>25%</span>
                                </div>
                                <div class="lineS" style="left:{{ $linePos }}px;"></div>
                                <div id="description_discount" class="description_discount displaynone">
                                        Здесь имеется ввиду базовая скидка - без учёта надбавок за участие в опросах.
                                </div>        
                            </div>
                            <div class="pushare_line_y d-none d-sm-block d-block d-md-none">
                                <div class="line_top"></div>
                                <div class="arrow-pushare" style="top:{{ $linePos }}px"></div>

                                <div class="percents">
                                    <div class="one-per">10%</div>
                                    <div class="two-per">15%</div>
                                    <div class="three-per">20%</div>
                                    <div class="four-per">25%</div>
                                </div>

                                <div class="summ-pushare">
                                    <div class="one-summ">10 000 ₽</div>
                                    <div class="two-summ">25 000 ₽</div>
                                    <div class="three-summ">45 000₽</div>
                                </div>

                                <div class="box-section-pushare">
                                    <div class="one-section"></div>
                                    <div class="two-section"></div>
                                    <div class="three-section"></div>
                                    <div class="four-section"></div>
                                </div>

                                <div class="line_bottom"></div>
                            </div>
                            <p class="rep">{!! $repl !!}</p>
                            <div class="buys-info">
                                Если вы обнаружили ошибку - <a href="#" data-toggle="modal" data-target="#errors_cabinet" class="dotted-link buys-info-mistake-form">сообщите нам</a>.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>  


 

                    <div class="modal" id="errors_cabinet" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                            <svg role="img" width="14" height="14" style="fill: #fff;">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="{{ asset('static/images/sprite.svg#close') }}"></use>
                                            </svg>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                            <form action="{{ route('errorPurchases') }}/" class="mistake-form__form" method="POST">
                                                {{ csrf_field() }}
                                                <div class="popup-form-title">Ошибка в данных о покупке</div>
                                                <div class="form-line">
                                                    <label for="COMMENT" class="site-label">
                                                        Опишите, пожалуйста, ошибку на сайте, с которой вы столкнулись. <br> <br>
                                                    </label>
                                                    <textarea name="comment" id="comment" class="site-textarea site-textarea_mistake"></textarea>
                                                    <div class="error-line phone-error-line hide"></div>
                                                </div>
                                                
                                                <div class="form-line form-line_center">
                                                    <button class="site-button site-button_popup send_errors" type="submit">Отправить</button>
                                                </div>
                                            </form>
                                    </div>
                                </div>
                            </div>
                        </div>
{{--     
 <div class="inner-page-content">

            <div class="page-title">
                <h1 class="page-title__h1">Мои покупки</h1>
                <div class="page-title_big">Мои покупки</div>
                <div class="page-title_small">Мои покупки</div>
            </div>
    

    <div class="buys-content">

        <div class="second-nav">
            <a href="#" class="second-nav-link second-nav-link_active">история покупок</a>
            <a href="{{ route('infoIndex') }}/" class="second-nav-link">мои данные</a>
        </div>
        
        @if($pollMessage)
        <p style="align: center; color: red; font-size: 16px;">
            <b>{{ $pollMessage }}</b>
        </p>
        @endif
        
        @if (count($purchases) > 0)
        <table class="buys-table">
            <thead>
                <tr>
                    <td class="buys-table-td_date">Дата покупки</td>
                    <td class="buys-table-td_art-number">Артикул</td>
                    <td class="buys-table-td_item">Покупка</td>
                    <td class="buys-table-td_count">Кол-во</td>
                    <td class="buys-table-td_price">Цена</td>
                    <td class="buys-table-td_discount">Скидка</td>
                    <td class="buys-table-td_with-discount">Со скидкой</td>
                </tr>
            </thead>
            <tbody>
                @foreach($purchases as $purchase)
                    <tr class="buys-table__tr buys-table__tr_uneven">
                        <td class="buys-table-td_date">{{ date('d.m.Y',strtotime($purchase->datePurchase)) }}</td>
                        <td class="buys-table-td_art-number">{{ $purchase->vendorCode }}</td>
                        <td class="buys-table-td_item"><strong>{{ $purchase->name }}</strong></td>
                        <td class="buys-table-td_count">{{ $purchase->quantity }}</td>
                        <td class="buys-table-td_price">{{ $purchase->price }} <span class="b-rub">Р</span></td>
                        <td class="buys-table-td_discount">{{ $purchase->discount }}%</td>
                        <td class="buys-table-td_with-discount">{{ $purchase->finalPrice }}<span class="b-rub">Р</span></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="buys-summary__holder">
            <div class="buys-summary">
                Общая сумма покупок: <span class="buys-summary__summ">{{ number_format($totalPrice, 0,'',' ') }}<span class="b-rub">Р</span></span>
            </div>
        </div>
        
        @else
            <div class="error"><p><b>Вы не совершали покупок по дисконтной карте.</b></p></div>
            Если вы обнаружили ошибку — <a href="#" class="dotted-link buys-info-mistake-form">сообщите нам</a>.
        @endif
        
    </div>

    @if (count($purchases) != 0)
        <div class="purchase_line">
            <div class='colorLine' style='width:112px;'>
                <span class='pers' style='left:43px;'>10%</span>
                <span class='cost' style='left:87px;'>10 000 <span class="b-rub">Р</span></span>
            </div>
            <div class='colorLine' style='width:280px;'>
                <span class='pers' style='left:181px;'>15%</span>
                <span class='cost' style='left:256px;'>25 000 <span class="b-rub">Р</span></span>
            </div>
            <div class='colorLine' style='width:504px;'>
                <span class='pers' style='left:378px;'>20%</span>
                <span class='cost' style='left:480px;'>45 000 <span class="b-rub">Р</span></span>
            </div>
            <div class='colorLine last-child' style='width:616px;'>
                <span class='pers' style='left:547px;'>25%</span>
            </div>
                        <div class="lineS" style="left:{{ $linePos }}px;"></div>
                                        <div id="description_discount" class="description_discount displaynone">
                    Здесь имеется ввиду базовая скидка - без учёта надбавок за участие в опросах.</div>        </div>
        <br/>
        <p>{!! $repl !!}</p>
    <div class="buys-info">
        <!--Данные о покупках обновляются на следующий день после их совершения.-->
        Если вы обнаружили ошибку — <a href="#" class="dotted-link buys-info-mistake-form">сообщите нам</a>.
    </div>
    @endif




            </div> --}}

@endsection

