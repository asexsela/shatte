@extends('layouts.templateMain')

@section('title', 'Вакансии')

@section('content')
<div class="inner-page-content">

    <div class="page-title">
        <h1 class="page-title__h1">Вакансии</h1>
        <div class="page-title_big">Вакансии</div>
        <div class="page-title_small">Вакансии</div>
    </div>




    <p>
        Компания предлагает сотрудникам  конкурентоспособную заработную плату и социальный пакет, который включает
        добровольное медицинское страхование.
    </p>

    <p>
        Мы стараемся привлекать действительно талантливых людей, которые смогут внести заметный вклад в развитие нашей
        компании.  Мы ставим перед сотрудниками задачи, позволяющие проявить свои способности и  добиться высоких
        результатов. Работа в EVONA позволяет повышать профессиональный уровень, строить карьеру.
    </p>

    <p>
        Мы предоставляем нашим сотрудникам широкие возможности для профессионального роста – это и профессиональные и
        управленческие тренинги и программы обучения, участие в профессиональных мероприятиях рынка: семинарах,
        конференциях, выставках.
    </p>




    <div class="form-line form-line_center form-line_margin">
        <button class="site-button site-button_vacancy vacancy-step-0">Я хочу работать в компании Evona</button>
    </div>

    <form action="{{ route('vacancyFormHandler') }}/" method="post" id="qForm">
        {{ csrf_field() }}
        <script> var vacanciesInfo;vacanciesInfo = {'ITEMS':{'1355939':{'NAME':'Продавец-консультант','ID':'1355939','CITIES':['61','62','63','64','65','66','68','69','70','225905','225906','225907','250664','1232981'],'now':'\n'+
                        '<p> <strong>Требования</strong> \n'+
                        '  <br />\n'+
                        ' Пол: женский \n'+
                        '  <br />\n'+
                        ' Возраст: 22&mdash;30 \n'+
                        '  <br />\n'+
                        ' Образование: не ниже среднеспециального \n'+
                        '  <br />\n'+
                        ' Статус: не важен \n'+
                        '  <br />\n'+
                        ' </p>\n'+
                        ' <strong>Условия</strong> График: 3/2 \n'+
                        '<br />\n'+
                        ' З/п определяется по формуле: \n'+
                        '<br />\n'+
                        ' 1) норма-день Х коэфициент качества труда + \n'+
                        '<br />\n'+
                        ' 2) % от суммы личных продаж + \n'+
                        '<br />\n'+
                        ' 3) % от суммы общих продаж при выполнении плана продаж + \n'+
                        '<br />\n'+
                        ' 4) бонус за оформление дисконтных карт. Стабильный доход: от 20 000 руб. без учета подоходного налога (-13%) \n'+
                        '<br />\n'+
                        ' Льготы: в соответствии с законодательством РФ \n'+
                        '<br />\n'+
                        ' ','look':'\n'+
                        '<div>Если на данный момент у Вас есть работа, но Вы рассматриваете более интересные варианты, то предлагаем вступить в кадровый резерв компании EVONA.</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div><b> Это означает</b>, что Вы проходите наше собеседование, и по его итогам стажировку длительностью в 1-3 рабочих дня. При положительном результате:</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 1. Мы оплачиваем Вам время стажировки из расчета 2-4 тыс. руб./день, в зависимости от вакансии.</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 2. Заключаем с Вами соглашение, что при освобождении вакансии в EVONA, мы принимаем Вас на работу без испытательного срока.</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 3. И в качестве подарка от нас передаем Вам дисконтную карту EVONA со скидкой 25%на одежду и аксессуары EVONA.</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div><b> При появлении вакансии</b>, мы автоматически приглашаем Вас на работу, предлагая</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 1. Официальное трудоустройство по ТК РФ</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 2. Размер заработной платы в среднем на 15-20% выше, чем в магазинах федеральных конкурентов</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div>Дополнительно: </div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 1. Работая в EVONA, наш коуч-тренер обучит Вас коммуникативным навыкам, которые будут полезны для Вас не только в работе, но и в повседневной жизни.</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> 2. Работая в EVONA, наши менеджеры обучат Вас управленческим навыкам, которые позволят добиваться успеха не только на работе, но и других областях Вашей жизнедеятельности.</div>\n'+
                        ' \n'+
                        '<div> \n'+
                        '  <br />\n'+
                        ' </div>\n'+
                        ' \n'+
                        '<div> Если Вы позитивны в жизни, стремитесь к знаниям и хотите быть уверенным в завтрашнем дне, то записывайтесь в кадровый резерв EVONA.</div>\n'+
                        ' '},'1355940':{'NAME':'Директор магазина','ID':'1355940','CITIES':['61','62','63','64','65','66','68','69','70','225905','225906','225907','250664','1232981'],'look':'<p> <strong>Требования</strong>\n'+
                        '  <br />\n'+
                        ' Пол: женский \n'+
                        '  <br />\n'+
                        ' Возраст: 22&mdash;30\n'+
                        '  <br />\n'+
                        ' Образование: высшее\n'+
                        '  <br />\n'+
                        ' Опыт работы: в торговой розничной сети, знание технологий и бизнес процессов розничной торговли, кассы. \n'+
                        '  <br />\n'+
                        ' Опыт руководства коллективом от 4 человек. \n'+
                        '  <br />\n'+
                        ' Навыки: уверенный пользователь ПК, способность быстро изучать новую информацию, знание закона о защите   прав потребителей.\n'+
                        '  <br />\n'+
                        ' Статус: не важен\n'+
                        '  <br />\n'+
                        ' </p>\n'+
                        ' <strong>Условия</strong> \n'+
                        'График: 5/2, сб. и вс. выходной. Полный рабочий день, полная занятость. \n'+
                        '<br />\n'+
                        ' З/п от 25 000 рублей (минимальный доход при отработке 165 ч.) до 45 000 (почасовая ставка + вознаграждение за каждый день достижения плана по коэффициенту конверсии). \n'+
                        '<br />\n'+
                        'Корпоративная скидка на товар компании.\n'+
                        '<br />\n'+
                        'Полный социальный пакет. \n'+
                        '<br />\n'+
                        'Оформление по ТК РФ. \n'+
                        '<br />\n'+
                        ' Льготы: в соответствии с законодательством РФ\n'+
                        '<br />\n'+
                        ''}},'CITIES':[{'ID':'61','~ID':'61','NAME':'Ярославль','~NAME':'Ярославль'},{'ID':'62','~ID':'62','NAME':'Иваново','~NAME':'Иваново'},{'ID':'63','~ID':'63','NAME':'Владимир','~NAME':'Владимир'},{'ID':'64','~ID':'64','NAME':'Калининград','~NAME':'Калининград'},{'ID':'65','~ID':'65','NAME':'Вологда','~NAME':'Вологда'},{'ID':'66','~ID':'66','NAME':'Кострома','~NAME':'Кострома'},{'ID':'68','~ID':'68','NAME':'Мурманск','~NAME':'Мурманск'},{'ID':'69','~ID':'69','NAME':'Сыктывкар','~NAME':'Сыктывкар'},{'ID':'70','~ID':'70','NAME':'Рязань','~NAME':'Рязань'},{'ID':'225905','~ID':'225905','NAME':'Петрозаводск','~NAME':'Петрозаводск'},{'ID':'225906','~ID':'225906','NAME':'Пенза','~NAME':'Пенза'},{'ID':'225907','~ID':'225907','NAME':'Архангельск','~NAME':'Архангельск'},{'ID':'250664','~ID':'250664','NAME':'Смоленск','~NAME':'Смоленск'},{'ID':'1232981','~ID':'1232981','NAME':'Тверь','~NAME':'Тверь'}]}</script>

        <div class="vacancy-block__step1 hide">
            <div class="page-title">
                <h1 class="page-title__h1">Выберите вакансию</h1>

                <div class="page-title_big">Выберите вакансию</div>
                <div class="page-title_small">Выберите вакансию</div>
            </div>


            <div class="form-block">
                <div class="form-line">

                    <label for="city" class="site-inline-label">В каком городе вы ищете работу?</label>

                    <div class="inline-input-holder">
                        <select name="city" id="city" class="site-selector">
                            <option value="61">Ярославль</option>                            <option value="62">Иваново</option>                            <option value="63">Владимир</option>                            <option value="64">Калининград</option>                            <option value="65">Вологда</option>                            <option value="66">Кострома</option>                            <option value="68">Мурманск</option>                            <option value="69">Сыктывкар</option>                            <option value="70">Рязань</option>                            <option value="225905">Петрозаводск</option>                            <option value="225906">Пенза</option>                            <option value="225907">Архангельск</option>                            <option value="250664">Смоленск</option>                            <option value="1232981">Тверь</option>                    </select>
                    </div>

                </div>
                <div class="form-line">

                    <label for="type" class="site-inline-label">Какая работа вам интересна?</label>

                    <div class="inline-input-holder">
                        <select name="type" id="type" class="site-selector">
                            <option value="1355939">Продавец-консультант</option>                                <option value="1355940">Директор магазина</option>                    </select>
                    </div>

                </div>
                <div class="form-line">
                    <div class="site-inline-label">Как срочно вам нужна работа?</div>
                    <div class="inline-input-holder inline-input-holder_with-pad">

                        <input type="radio" name="time" id="vTime_srochno" value="now" class="site-radio" checked>
                        <label for="vTime_srochno" class="radio-label">Работа нужна срочно</label>



                        <input type="radio" name="time" id="vTime_variants" value="look" class="site-radio" >
                        <label for="vTime_variants" class="radio-label">У меня есть работа, но я рассматриваю более интересные предложения</label>

                    </div>

                </div>
            </div>

            <div class="form-line form-line_center form-line_margin">
                <button class="site-button site-button_vacancy vacancy-step-1">Далее</button>
            </div>


        </div>



        <div class="vacancy-block__step2 hide">

            <div class="page-title">
                <h1 class="page-title__h1">Подходящие вакансии</h1>
                <div class="page-title_big">Подходящие вакансии</div>
                <div class="page-title_small">Подходящие вакансии</div>
            </div>


            <div class="vacancy-description">

            </div>


            <div class="form-line form-line_center form-line_margin">
                <button class="site-button site-button_vacancy vacancy-step-2">Заполнить анкету</button>
            </div>


        </div>


        <div class="vacancy-block__step3 hide">

            <div class="page-title">
                <h1 class="page-title__h1">Анкета соискателя</h1>
                <div class="page-title_big">Анкета соискателя</div>
                <div class="page-title_small">Анкета соискателя</div>
            </div>


            <div class="form-line">
                <label for="name" class="site-inline-label">Имя*</label>
                <div class="inline-input-holder">
                    <input type="text" name="name" id="name" class="site-input site-input_vacancy checkname">
                    <div class="error-box error-box_inline"></div>
                </div>
            </div>
            <div class="form-line">
                <label for="lastName" class="site-inline-label">Фамилия*</label>
                <div class="inline-input-holder">
                    <input type="text" name="lastName" id="vLastname" class="site-input site-input_vacancy checklastName">
                    <div class="error-box error-box_inline"></div>
                </div>
            </div>


            <div class="form-line">
                <label for="age" class="site-inline-label">Ваш возраст:*</label>
                <div class="inline-input-holder">
                    <select  id="age" name="age" class="site-selector">
                        <option>18</option>
                        <option>19</option>
                        <option>20</option>
                        <option>21</option>
                        <option>22</option>
                        <option>23</option>
                        <option>24</option>
                        <option>25</option>
                        <option>26</option>
                        <option>27</option>
                        <option>28</option>
                        <option>29</option>
                        <option>30</option>
                        <option>31</option>
                        <option>32</option>
                        <option>33</option>
                        <option>34</option>
                        <option>35</option>
                        <option>36</option>
                        <option>37</option>
                        <option>38</option>
                        <option>39</option>
                        <option>40</option>
                        <option>41</option>
                        <option>42</option>
                        <option>43</option>
                        <option>44</option>
                        <option>45</option>
                    </select>

                    <div class="error-box error-box_inline"></div>
                </div>
            </div>


            <div class="form-line">
                <label for="experience" class="site-inline-label">Опыт работы:*</label>
                <div class="inline-input-holder">
                    <select  id="experience" name="experience" class="site-selector">
                        <option>без опыта</option>
                        <option>до полугода</option>
                        <option>1  год</option>
                        <option>1.5 года</option>
                        <option>2 года</option>
                        <option>2.5 года</option>
                        <option>3 года</option>
                        <option>более 3х лет</option>
                    </select>
                    <div class="error-box error-box_inline"></div>
                </div>
            </div>

            <div class="form-line">
                <label for="phone" class="site-inline-label">Телефон:*</label>
                <div class="inline-input-holder">
                    <input type="text" name="phone" id="phone" class="site-input site-input_vacancy checkphone">
                    <div class="error-box error-box_inline"></div>
                </div>
            </div>

            <div class="form-line">
                <label for="email" class="site-inline-label">Email:*</label>
                <div class="inline-input-holder">
                    <input type="text" name="email" id="email" class="site-input site-input_vacancy checkemail">
                    <div class="error-box error-box_inline"></div>
                </div>
            </div>

            <div class="form-line">
                <label for="photo" class="site-inline-label">Ваше фото:*</label>
                <div class="inline-input-holder">
                    <input type="hidden" class="property-name" value="photo">
                    <div class="b-upload">
                        <div class="b-upload__files"></div>
                        <div class="b-upload__container">
                            <button class="b-upload__select site-button b-upload__select_photo">Загрузить фото</button>
                        </div>
                    </div>
                    <span class="checkphoto"></span>
                    <div class="error-box error-box_inline"></div>
                </div>
            </div>


            <div class="form-line">
                <label for="resume" class="site-inline-label">Ваше резюме:</label>
                <div class="inline-input-holder">
                    <input type="hidden" class="property-name" value="resume">
                    <div class="b-upload">
                        <div class="b-upload__files"></div>
                        <div class="b-upload__container">
                            <button class="b-upload__select site-button b-upload__select_resume">Загрузить резюме</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-line">
                <label for="comment" class="site-inline-label">Дополнительный комментарий:</label>
                <div class="inline-input-holder">
                    <textarea name="comment" id="comment" class="site-textarea"></textarea>
                </div>
            </div>

            <div class="form-line form-line_center form-line_margin">
                <button class="site-button site-button_vacancy final-submit" type="submit">Отправить анкету</button>
            </div>



        </div>

        <div class="vacancy-block__step4 hide">
            <div class="page-title">
                <h1 class="page-title__h1">Большое спасибо!</h1>
                <div class="page-title_big">Большое спасибо!</div>
                <div class="page-title_small">Большое спасибо!</div>
            </div>

            Мы получили ваш отклик на вакансию и свяжемся с вами в ближайшее время.


            <div class="form-line form-line_center form-line_margin">
                <button class="site-button site-button_vacancy" onclick="javascript:window.location.reload();">Выбрать еще одну вакансию</button>
            </div>
        </div>

    </form>

</div>
@endsection