<?php

# Pages #
Route::get('/',                          ['uses' => 'IndexController@index', 'as' => 'index']);
Route::get('/vacancy/',                  ['uses' => 'VacancyController@index', 'as' => 'vacancyIndex']);
Route::get('/test/','TestController@index');
Route::get('/test/{method}/','TestController@method');

Route::post('/vacancy/',                 ['uses' => 'VacancyController@formHandler', 'as' => 'vacancyFormHandler']);


# Auth #
Route::redirect('login', 'personal/', 301)->name('login');

Route::get('personal/',                  ['uses' => 'Auth\LoginController@index', 'as' => 'personalIndex']);
Route::get('personal/logout/',           ['uses' => 'Auth\LoginController@logout', 'as' => 'logout']);
Route::get('personal/confirm_email/',    ['uses' => 'Auth\ConfirmEmailController@index', 'as' => 'confirmEmailIndex']);
Route::get('personal/confirm_sms/',      ['uses' => 'Auth\ConfirmSmsController@index', 'middleware' => 'confirmedEmail','as' => 'confirmSmsIndex']);

Route::post('personal/',                 ['uses' => 'Auth\LoginController@login', 'as' => 'personal']);
Route::post('personal/error/',           ['uses' => 'Auth\LoginController@errorNotify', 'as' => 'errorNotify']);
Route::post('personal/error_confirm/',   ['uses' => 'Auth\ConfirmEmailController@errorConfirm', 'middleware' => 'auth', 'as' => 'errorConfirm']);
Route::post('personal/confirm_email/',   ['uses' => 'Auth\ConfirmEmailController@sendEmail', 'middleware' => 'auth', 'as' => 'confirmEmail']);
Route::post('personal/confirm_sms/',     ['uses' => 'Auth\ConfirmSmsController@checkSmsCode', 'middleware' => 'confirmedEmail','as' => 'confirmSms']);

# Personal #
Route::get('personal/items/',            ['uses' => 'Personal\PurchasesController@index', 'as' => 'purchases', 'middleware' => 'confirmedEmail']);
Route::get('personal/info/',             ['uses' => 'Personal\InfoController@index', 'as' => 'infoIndex', 'middleware' => 'confirmedEmail']);
Route::get('personal/info/sms/',         ['uses' => 'Personal\InfoController@indexSms', 'as' => 'infoSmsIndex', 'middleware' => 'confirmedEmail']);

Route::post('personal/items/error/',     ['uses' => 'Personal\PurchasesController@errorPurchases', 'as' => 'errorPurchases', 'middleware' => 'confirmedEmail']);
Route::post('personal/info/',            ['uses' => 'Personal\InfoController@changeInfo', 'as' => 'info', 'middleware' => 'confirmedEmail']);
Route::post('personal/info/sms/',        ['uses' => 'Personal\InfoController@checkSmsCode', 'as' => 'infoSms', 'middleware' => 'confirmedEmail']);
Route::post('personal/poll/hello/',      ['uses' => 'Personal\PollController@hello', 'as' => 'poll.hello', 'middlware' => 'confirmedEmail']);
Route::post('personal/poll/check/',      ['uses' => 'Personal\PollController@check', 'as' => 'poll.check', 'middlware' => 'confirmedEmail']);
Route::post('personal/poll/start/',      ['uses' => 'Personal\PollController@start', 'as' => 'poll.start', 'middlware' => 'confirmedEmail']);
Route::post('personal/poll/ignore/',     ['uses' => 'Personal\PollController@ignore', 'as' => 'poll.ignore', 'middlware' => 'confirmedEmail']);
Route::post('personal/poll/next/',       ['uses' => 'Personal\PollController@next', 'as' => 'poll.next', 'middlware' => 'confirmedEmail']);
Route::post('personal/poll/finish/',     ['uses' => 'Personal\PollController@finishPoll', 'as' => 'poll.finish', 'middlware' => 'confirmedEmail']);

# Admin #
Route::get('admin/',                     ['uses' => 'Admin\IndexController@index', 'as' => 'admin.index']);
Route::post('admin/',                    ['uses' => 'Admin\IndexController@login', 'as' => 'admin.login']);

Route::group(['middleware' => 'admin', 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('dashboard/',             ['uses' => 'IndexController@dashboard', 'as' => 'admin.dashboard']);
    Route::get('logout/',                ['uses' => 'IndexController@logout', 'as' => 'admin.logout']);
    
    Route::get('music/',                 ['uses' => 'MusicController@index', 'as' => 'admin.music.index']);
    Route::post('music/',                ['uses' => 'MusicController@edit', 'as' => 'admin.music.edit']);
});

# Corporate #
Route::get('corporate/',                 ['uses' => 'Corporate\LoginController@index', 'as' => 'corporate.index']);
Route::get('corporate/registration/',    ['uses' => 'Corporate\RegistrationController@index', 'as' => 'corporate.registration.index']);
Route::get('corporate/remember/',        ['uses' => 'Corporate\RememberController@index', 'as' => 'corporate.remember.index']);
Route::get('corporate/recovery/',        ['uses' => 'Corporate\RecoveryController@index', 'as' => 'corporate.recovery.index']);

Route::post('corporate/',                ['uses' => 'Corporate\LoginController@login', 'as' => 'corporate.login']);
Route::post('corporate/registration/',   ['uses' => 'Corporate\RegistrationController@registration', 'as' => 'corporate.registration']);
Route::post('corporate/remember/',       ['uses' => 'Corporate\RememberController@remember', 'as' => 'corporate.remember']);
Route::post('corporate/recovery/',       ['uses' => 'Corporate\RecoveryController@recovery', 'as' => 'corporate.recovery']);

Route::group(['middleware' => 'employee', 'prefix' => 'corporate', 'namespace' => 'Corporate'], function () {
    Route::get('logout/',                ['uses' => 'LoginController@logout', 'as' => 'corporate.logout']);
    Route::get('regulations/{code?}',    ['uses' => 'RegulationsController@index', 'as' => 'corporate.regulations.index']);
    Route::get('analogs/{code?}',        ['uses' => 'AnalogsController@index', 'as' => 'corporate.analogs.index']);
    Route::post('analogs/products/',     ['uses' => 'AnalogsController@loadProducts', 'as' => 'corporate.analogs.products']);
    Route::post('analogs/',              ['uses' => 'AnalogsController@sendAnalog', 'as' => 'corporate.analogs.send']);
    Route::post('analogs/doclist',       ['uses' => 'AnalogsController@doclist', 'as' => 'corporate.analogs.doclist']);
});