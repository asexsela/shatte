<?php

class Bot{
    
    public $files = [
        'nonce' => PATH_DIR.'files/nonce.txt',
        'pairs' => PATH_DIR.'files/pairs.txt',
        'api' => PATH_DIR.'files/api.txt',
        'orders' => PATH_DIR.'files/orders.txt',
        'common' => PATH_DIR.'files/common.txt',
    ];
    public $last_money = [];
    private $stop_file_path = PATH_DIR.'files/stop_trade/';
    
    public $money_limit = 0.05;
    public $percent_limit = 1;
    public $percent_limit_risk = 0.1;
    public $money_min = 0.002;
    public $time_limit_order1 = 10;
    public $time_limit_order2 = 90;
    public $time_limit_order3 = 300;
    public $cur_time;
    
    public $f_order;
    public $mli;
    
    public $main_pair, $first_cur, $second_cur, $dir;
     
    public $chain_data = [];
    
    function __construct($main_pair, $first_cur = false, $second_cur = false) {
        
        $this->main_pair = $main_pair;
//        $this->dir = $dir;
        $pair_exp = explode('_',$main_pair);
        $this->first_cur = $first_cur ? $first_cur : $pair_exp[0];
        $this->second_cur = $second_cur ? $second_cur : $pair_exp[1];
        
        $this->f_order = fopen($this->files['orders'],'a+');
        
        //$this->mli = $mli = mysqli_connect('localhost', 'root', '_Iam19k16_', 'bot');
    }
    
    function telegram($message){
        file_get_contents('https://api.telegram.org/bot467747777:AAG9axmmyRD1-WHZm1YWtNiZstkroRi8oAs/sendMessage?chat_id=130081066&text='. urlencode($message));
    }
    
    function telegram_belocoin($message){
        file_get_contents('https://api.telegram.org/bot467747777:AAG9axmmyRD1-WHZm1YWtNiZstkroRi8oAs/sendMessage?chat_id=126170008&text='. urlencode($message));
        file_get_contents('https://api.telegram.org/bot467747777:AAG9axmmyRD1-WHZm1YWtNiZstkroRi8oAs/sendMessage?chat_id=130081066&text='. urlencode($message));
    }
    
    function api_info($method,$param1 = NULL,$param2 = NULL){
    
        $start_time = microtime(true);
        $url = 'https://yobit.io/api/3/'.$method;
        if ($param1){
            $url.= '/'.$param1;
        }
        if($param2){
            $arr = [];
            foreach($param2 as $i => $v){
                $arr[] = $i.'='.$v;
            }
            $url.='?'.implode('&',$arr);
        }
        $res = json_decode(file_get_contents($url),true);
        $f = fopen($this->files['api'],'a+');
        fwrite($f,date('Y-m-d H:i:s').' '.$method.' '.number_format(microtime(true) - $start_time,10)."\n");
        fclose($f);
        set_counter();
        return $res;
    }
    
    function api_info2($method,$param1 = NULL,$param2 = NULL){
    
        $start_time = microtime(true);
        $url = 'https://yobit.net/api/3/'.$method;
        if ($param1){
            $url.= '/'.$param1;
        }
        if($param2){
            $arr = [];
            foreach($param2 as $i => $v){
                $arr[] = $i.'='.$v;
            }
            $url.='?'.implode('&',$arr);
        }
       
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_INTERFACE, '185.195.27.47');
        $res = curl_exec($ch);
        curl_close($ch);
        
        $f = fopen($this->files['api'],'a+');
        fwrite($f,date('Y-m-d H:i:s').' '.$method.' '.number_format(microtime(true) - $start_time,10).'  curlyk!'."\n");
        fclose($f);
        set_counter();
        return $res;
    }

    function api_trade($method, $req = array())
    {

        $start_time = microtime(true);
        $api_key    = '8A4E97E45D97B81DABE54A3CFE3A400E';
        $api_secret = '2978a7457c7c1e754419b8667d181b66';

        $req['method'] = $method;

        $nonce = file($this->files['nonce'])[0];
        $nonce++;
        $fn = fopen($this->files['nonce'],'w+');
        fwrite($fn,$nonce);
        fclose($fn);

        $req['nonce'] = $nonce;
        $post_data = http_build_query($req, '', '&');
        $sign = hash_hmac("sha512", $post_data, $api_secret);
        $headers = array(
            'Sign: '.$sign,
            'Key: '.$api_key,
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; SMART_API PHP client; '.php_uname('s').'; PHP/'.phpversion().')');
        curl_setopt($ch, CURLOPT_URL, 'https://yobit.io/tapi/');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_ENCODING , 'gzip');
        $res = curl_exec($ch);
        if($res === false)
        {
            $e = curl_error($ch);
            debuglog($e);
            curl_close($ch);
            return null;
        }

        curl_close($ch);

        $result = json_decode($res, true);
        if(!$result){ print_r($res);}

        $f = fopen($this->files['api'],'a+');
        fwrite($f,date('Y-m-d H:i:s').' '.$method.' '.number_format(microtime(true) - $start_time,10).' '.json_encode($result)."\n");
        fclose($f);
        return $result;
    }  
    function generate_currency_file(){

        $vol_limit = [
            'doge' => 1000000,
            'eth' => 5,
            'usd' => 10000,
            'rur' => 500000,
            'btc' => 2.5,
        ];

        $result = [];
        
        $info = $this->api_info('info');
        $currencies = [];
        $pair_arr = [];
        foreach($info['pairs'] as $pair => $data){
            $pair_exp = explode('_',$pair);
            if ($pair_exp[1] == $this->first_cur || $pair_exp[1] == $this->second_cur){
                $pair_arr[] = $pair;
                $currencies[$pair_exp[1]][] = $pair_exp[0];
            }
        }
        $pair_str = implode('-',$pair_arr);
        $inc = 0;
        $pair_str = '';
        $pair_arr = [];
        $total = 0;
        foreach($currencies as $cur1 => $data){
            foreach($data as $cur2){

                $inc++;
                $total++;
                $pair = $cur2.'_'.$cur1;
                $pair_arr[] = $pair;

                if ($inc >= 50){
                    $pair_str = implode('-',$pair_arr);
                    $info = $this->api_info('ticker',$pair_str);
                    foreach($info as $pair => $data){
                        if ($data['vol'] >= $vol_limit[$cur1]){
                            echo $pair.' '.$data['vol']." \n";
                            $result[] = $pair;
                        }
                    }
                    $inc = 0;
                    $pair_str = '';
                    $pair_arr = [];
                    echo 'parsed: '.$total.PHP_EOL;
                } 
            }
            if($inc > 0){
                $pair_str = implode('-',$pair_arr);
                $info = $this->api_info('ticker',$pair_str);
                foreach($info as $pair => $data){
                    if ($data['vol'] >= $vol_limit[$cur1]){
                        echo $pair.' '.$data['vol']." \n";
                        $result[] = $pair;
                    }
                }
                $inc = 0;
                $pair_str = '';
                $pair_arr = [];
                echo 'parsed: '.$total.PHP_EOL;
            }
        }
        
        $currencies = [];
        foreach($result as $pair){
            $pair_exp = explode('_',$pair);
            $currencies[trim($pair_exp[1])][trim($pair_exp[0])] = trim($pair);
        }

        $pair_arr1 = [];
        $pair_arr2 = [];
        $curs = [];
        
        foreach($currencies[$this->first_cur] as $cur2 => $pair1){

            if (isset($currencies[$this->second_cur][$cur2])){
                $pair2 = $currencies[$this->second_cur][$cur2];
                $pair_arr1[] = $pair1;
                $pair_arr2[] = $pair2;
                $curs[] = $cur2;
            }

        }
        
        $pair_str1 = implode('-',$pair_arr1);
        $pair_str2 = implode('-',$pair_arr2);
        
        $f = fopen($this->get_pair_file(),'w+');
        fwrite($f,$pair_str1.'-'.$pair_str2.'-'.$this->main_pair."\n");
        fwrite($f,json_encode($curs));
        fclose($f);
        echo 'parsed finish!'.PHP_EOL;
    }

    function parse_pairs($cur = NULL){
        $start_time = microtime(true);
        
        if ($cur){
            $pairs = $cur.'_'.$this->second_cur.'-'.$cur.'_'.$this->first_cur.'-'.$this->main_pair;
            $curs = [$cur];
        }else{
            $pairs_data = file($this->get_pair_file());
            $pairs = trim($pairs_data[0]);
            $curs = json_decode($pairs_data[1],true);
        }
        $info = $this->api_info('depth',$pairs,['limit'=>2]);
        $price3 = $info[$this->main_pair]['bids'][0][0];
        
        foreach($curs as $cur){
            
            $pair1 = $cur.'_'.$this->first_cur;
            $pair2 = $cur.'_'.$this->second_cur;
            
            if (!isset($info[$pair1]['asks']) || !isset($info[$pair2]['bids'])){
                continue;
            }
            
            $price1 = $info[$pair1]['asks'][0][0];
            $price2 = $info[$pair2]['bids'][0][0];
            $price2_check = $info[$pair2]['asks'][0][0];
            
            $result_sum = $price2 / $price1 * $price3 * 0.998 * 0.998;
        
            $per = $result_sum * 100 - 100;
            
            if ($per > 0){
                $price2_risk = $info[$pair2]['bids'][1][0];
                $per_risk = ($price2_risk / $price1 * $price3 * 0.998 * 0.998) * 100 - 100;
                
                $loop_message =  date('Y-m-d H:i:s',time()).' '.green(number_format($per,2,'.',' ').'%').' '.$pair1.' '.$pair2.' risk: '.number_format($per_risk,2,'.',' ').'% #'.get_counter().' '.PHP_EOL;
                //echo $loop_message;
            }
            
            if (($per > $this->percent_limit || ($per > 0 && $per + $per_risk > $this->percent_limit_risk)) && $per_risk > -5){
                
                $money = $this->money_limit;
                
                $vol1 = $info[$pair1]['asks'][0][1] * $price1;
                $vol2 = $info[$pair2]['bids'][0][1];
                
//                $dif2 = $info[$pair2]['bids'][0][0]/($info[$pair2]['bids'][1][0]/100) - 100;
                
                if ($vol1 <= $money){
                    $money = $vol1;
                }
                
                $count_buy = $money/$price1;
                $count_buy = floor($count_buy * 100000000)/100000000;
                
                if ($vol2 <= $count_buy){
                    $count_buy = $vol2;
                }
                
                $money = floor($count_buy*$price1*100000000) / 100000000;
                
                $loop_message2 = 'money: '.number_format($money,8).', dif:'.$dif2.'% '.PHP_EOL;
                //echo $loop_message2;
                
                if ($money >= $this->money_min && !$this->stop_cur_check($cur)){
                    
                    $dif1 = 100 - $info[$pair1]['asks'][0][0]/($info[$pair1]['asks'][1][0]/100);
                    
                    $per_str = number_format($per,2,'.',' ');
                    $dif_str = number_format($dif1,2,'.',' ').'% '.number_format($dif2,2,'.',' ').'%';
                    
                    $message = green($per_str.'%').' '.$pair1.' '.$pair2.' '.$dif_str.' '.round($money,8).' #'.get_counter().' risk: '.number_format($per_risk,2,'.',' ').'% '.PHP_EOL;
                    $message_telegram = 'ar_bot: '.($per_str.'%').' '.$pair1.' '.$pair2.' '.$dif_str.' '.round($money,8).' risk: '.number_format($per_risk,2,'.',' ').'% '.PHP_EOL;

                    
                    $this->order_log($pair1.': '.json_encode($info[$pair1]), true);
                    $this->order_log($pair2.': '.json_encode($info[$pair2]), true);
                    $this->order_log($this->main_pair.': '.json_encode($info[$this->main_pair]), true);
                    $this->order_log($message);
                    $this->order_log($money.' '.$count_buy.' '.$price1.' '.$price2.' '.$price3);
                    
//                    $this->chain_data = [
//                        'cur' => $cur,
//                        'pair1' => $pair1,
//                        'pair2' => $pair2,
//                        'price1' => $price1,
//                        'price2' => $price2,
//                        'price3' => $price3,
//                        'count_buy' => $count_buy,
//                        'money' => $money,
//                    ];
                    
                    
//                    $this->telegram(hex2bin("F09F92B2")." ".$message_telegram);
//                    $this->walk_chain_step1();
                    break;
                }
            }
        }
        
    }
    
    function walk_chain_step1(){
            
        $this->stop_cur_begin($this->chain_data['cur']);

        $this->order_log('order_1 process...');

        $order_params = [
            'pair' => $this->chain_data['pair1'],
            'type' => 'buy',
            'rate' => number_format($this->chain_data['price1'],8,'.',''),
            'amount' => $this->chain_data['count_buy'],
        ];

        $result1 = $this->api_trade('Trade',$order_params);

        if ($result1['success'] == 1){

            if ($result1['return']['received'] == $this->chain_data['count_buy']){
                $this->walk_chain_step2();
            }else{
                $order_id = $result1['return']['order_id'];

                $wait = true;
                $start_time = time();

                while($wait){

                    if (time() - $start_time > $this->time_limit_order1){
                        $exat_cancel = true;
                        if (isset($data) && $data['return'][$order_id]['status'] == 3){
                            $exat_cancel = false;
                        }
                        if ($exat_cancel){
                            $cancel = $this->api_trade('CancelOrder',['order_id'=>$order_id]);
                            if ($cancel['success'] == 1){
                                $wait = false;
                                $this->stop_cur_end($this->chain_data['cur']);
                                $this->order_log('order_1 canceled, time limit ('.$this->time_limit_order1.' sec)');
                                $this->telegram(hex2bin('F09F9280').' canceled');
                                break;
                            }else{
                                $this->order_log('order_1 cancel fail:');
                                $this->order_log(json_encode($cancel));
                            }
                        }
                    }

                    $data = $this->api_trade('OrderInfo',['order_id'=>$order_id]);

                    if ($data['success'] == 1){
                        $order_info = $data['return'][$order_id];
                        if ($order_info['status'] == 1){
                            $this->walk_chain_step2();
                            $wait = false;
                            break;
                        }
                    }else{
                        if ($order_info){
                            $this->order_log('order_1 check fail:');
                            $this->order_log(json_encode($data));
                        }
                    }
                    usleep(500000);
                }
            }

        }else{
            $this->order_log('order_1 fail:');
            $this->order_log(json_encode($result1));
            $this->order_log(json_encode($order_params));
            $this->stop_cur_end($this->chain_data['cur']);
        }
    }
    
    function walk_chain_step2(){
        $this->order_log('order_1 success!');
        $this->order_log('order_2 process...');
        
        $order_params = [
            'pair' => $this->chain_data['pair2'],
            'type' => 'sell',
            'rate' => number_format($this->chain_data['price2'],8,'.',''),
            'amount' => $this->chain_data['count_buy'],
        ];
        
        $result2 = $this->api_trade('Trade',$order_params);
        
        if ($result2['success'] == 1){
                
            if ($result2['return']['received'] == $this->chain_data['count_buy']){
                $this->walk_chain_step3();
            }else{
                $order_id = $result2['return']['order_id'];
                
                $wait = true;
                $start_time = time();

                while($wait){

                    if (time() - $start_time > $this->time_limit_order2){
                        if (isset($data)){
                            $this->chain_data['order_id'] = $order_id;
                            $this->chain_data['vol_current'] = $data['return'][$order_id]['amount'];
                            $this->new_lost_order();
                            $this->order_log('order_2 was lost! =(');
                            $this->telegram(hex2bin('F09F9980').' was lost');
                            $this->stop_cur_end($this->chain_data['cur']);
                            break;
                        }
                    }

                    $data = $this->api_trade('OrderInfo',['order_id'=>$order_id]);

                    if ($data['success'] == 1){
                        $order_info = $data['return'][$order_id];
                        if ($order_info['status'] == 1){
                            $this->walk_chain_step3();
                            $wait = false;
                            break;
                        }
                    }else{
                        if ($order_info){
                            $this->order_log('order_2 check fail:');
                            $this->order_log(json_encode($data));
                        }
                    }
                    usleep(500000);
                }
                
            }

        }else{
            $this->order_log('order_2 fail:');
            $this->order_log(json_encode($result2));
            $this->order_log(json_encode($order_params));
            $this->stop_cur_end($this->chain_data['cur']);
        }
        
    }
    
     function walk_chain_step3(){
        $this->order_log('order_2 success!');
        $this->order_log('order_3 process...');
        
        $count_buy = floor(($this->chain_data['count_buy']*$this->chain_data['price2'])*100000000)/100000000;           
        $count_buy = $count_buy - floor($count_buy * 0.002*100000000)/100000000;
        $count_buy = floor($count_buy * 0.998 * 100000000)/100000000;
        $count_buy = floor( $count_buy / $this->chain_data['price3'] * 100000000) / 100000000;
        
        $order_params = [
            'pair' => $this->main_pair,
            'type' => 'buy',
            'rate' => number_format($this->chain_data['price3'],8,'.',''),
            'amount' => number_format($count_buy,8),
        ];
        
        $result3 = $this->api_trade('Trade',$order_params);
        
        if ($result3['success'] == 1){
                
            if ($result3['return']['received'] == $count_buy){
                $this->order_log('order_3 success!');
            }else{
                $order_id = $result3['return']['order_id'];
                
                $wait = true;
                $start_time = time();

                while($wait){

                    if (time() - $start_time > $this->time_limit_order3){
                        //lost 3 ??
                    }

                    $data = $this->api_trade('OrderInfo',['order_id'=>$order_id]);

                    if ($data['success'] == 1){
                        $order_info = $data['return'][$order_id];
                        if ($order_info['status'] != 0){ // === 1  !!!!
                            $this->order_log('order_3 success!');
                            $this->order_log('profit: '.number_format($count_buy - $this->chain_data['money'],8).' ('. number_format($count_buy / ($this->chain_data['money'] / 100) - 100,2).'%)');
                            $this->telegram_belocoin(hex2bin("F09F92B0").' '.number_format($count_buy - $this->chain_data['money'],8).' ('. $this->round_to(($count_buy - $this->chain_data['money'])* $this->chain_data['price3'],2).' '.$this->second_cur.')');
                            $this->stop_cur_end($this->chain_data['cur']);
                            $wait = false;
                            break;
                        }
                    }else{
                        if ($order_info){
                            $this->order_log('order_3 check fail:');
                            $this->order_log(json_encode($data));
                        }
                    }
                    usleep(500000);
                }
                
            }

        }else{
            $this->order_log('order_3 fail:');
            $this->order_log(json_encode($result3));
            $this->order_log(json_encode([
                'pair' => $this->main_pair,
                'type' => 'buy',
                'rate' => number_format($this->chain_data['price3'],8,'.',''),
                'amount' => number_format($count_buy,8),
            ]));
            $this->stop_cur_end($this->chain_data['cur']);
        }
        
    }
    
    function new_lost_order(){
        $data = [
            'order_id' => '"'.$this->chain_data['order_id'].'"',
            'status' => 0,
            'cur' => '"'.$this->chain_data['cur'].'"',
            'pair1' => '"'.$this->chain_data['pair1'].'"',
            'pair2' => '"'.$this->chain_data['pair2'].'"',
            'price1' => $this->chain_data['price1'],
            'price2' => $this->chain_data['price2'],
            'price3' => $this->chain_data['price3'],
            'count_buy' => $this->chain_data['count_buy'],
            'vol_current' => $this->chain_data['vol_current'],
            'money' => $this->chain_data['money'],
            'time' => 'NOW()',
        ];
        $inserts = [];
        foreach($data as $i => $v){
            $inserts[] = '`'.$i.'` = '.$v;
        }
        $query = 'INSERT INTO `lost_orders` SET '.implode(',',$inserts);
        $this->mli->query($query);
    }
    
    function stop_cur_check($cur){
        $file_name = $this->stop_file_path.'stop_trade_'.$cur;
        if (file_exists($file_name)){
            $f_data = file($file_name)[0];
            if ($f_data == 'stop'){
                return true;
            }else{
                if (time() - $f_data < 5){
                    return true;
                }else{
                    return false;
                }
            }
        }else{
            return false;
        }
    }
    
    function stop_cur_end($cur){
        $f = fopen($this->stop_file_path.'stop_trade_'.$cur,'w+');
        fwrite($f,time());
        fclose($f);
    }
    
    function stop_cur_begin($cur){
        $f = fopen($this->stop_file_path.'stop_trade_'.$cur,'w+');
        fwrite($f,'stop');
        fclose($f);
    }
    
    function order_log($str, $only_file = false){
        fwrite($this->f_order, date('Y-m-d H:i:s ').$str.PHP_EOL);
        if (!$only_file){
            echo date('Y-m-d H:i:s ').$str.PHP_EOL;
        }
    }
    
    function lost_orders(){
        $query = 'SELECT * FROM `lost_orders` WHERE status = 0';
        $result = $this->mli->query($query);
        
        while($res = $result->fetch_assoc()){
            $this->lost_info($res);
            $this->lost_info($res,false);
        }
    }
    
    function lost_info($order,$fast = true){
        
        $curs = ['btc','doge','usd','rur'];
        
        $curs = array_flip($curs);
        unset($curs[$order['cur']]);
        $curs = array_flip($curs);
        
        $main_pair_exp = explode('_', $this->main_pair);
        $main_cur = $main_pair_exp[0];
        
        $pairs_arr = [];
        
        foreach($curs as $cur){
            
            $pair1 = $order['cur'].'_'.$cur;
            $pair2 = $main_cur.'_'.$cur;
            
            $pairs_arr[] = $pair1;
            $pairs_arr[] = $pair2;
            
        }
        $pair_back = $order['cur'].'_'.$main_cur;
        
        $pairs = implode('-',$pairs_arr).'-'.$pair_back;
        
        $info = $this->api_info('depth',$pairs,['limit'=>2]);
        
        $per_max = -100;
        $data_max = [];
        foreach($curs as $cur){
            
            $pair1 = $order['cur'].'_'.$cur;
            $pair2 = $main_cur.'_'.$cur;
            
            if (!isset($info[$pair1]['bids']) || !isset($info[$pair2]['asks'])){
                continue;
            }
            
            $price1 = $fast ? $info[$pair1]['bids'][0][0] : $info[$pair1]['asks'][0][0] - 0.00000001;
            
            $price2 = $info[$pair2]['asks'][0][0];
            
            $vol1 = $info[$pair1]['bids'][0][1];
            $vol2 = $info[$pair1]['asks'][0][1];
            
            if ($vol1 >= $order['count_buy']){
                $result_sum = 0.998 * 0.998 * $order['count_buy'] * $price1 / $price2;

                $per = $result_sum / ($order['money'] / 100) - 100;
            
//                echo $cur.' '.round($per,2).'% '.$price1.' '.$price2.PHP_EOL;

                if ($per_max < $per){
                    $per_max = $per;
                    $data_max = [
                        'pair1' => $pair1,
                        'pair2' => $pair2,
                        'price1' => $price1,
                        'price2' => $price2,
                        'cur' => $cur,
                    ];
                }
            }else{
                //see more 1 orders
            }
            
            
        }
        
        $price1 =  $fast ? $info[$pair_back]['bids'][0][0] : $info[$pair_back]['asks'][0][0] - 0.00000001;
        $result_sum = $price1 * $order['count_buy'] * 0.998;
        $per = $result_sum / ($order['money'] / 100) - 100;
        //echo $main_cur.' '.round($per,2).'% '.$price1.' '.PHP_EOL;

        
        if ($per_max < $per){
            $per_max = $per;
            echo $order['cur'].'-'.'back: '.round($per,2).' '.$price1.' '.($fast ? 'fast' : 'long').' '.PHP_EOL;
            if ($per_max > 0){
                
            }
        }else{
            echo $order['cur'].'-'.$data_max['cur'].' '.round($per_max,2).'% '.$data_max['price1'].' '.$data_max['price2'].' '.($fast ? 'fast' : 'long').' '.PHP_EOL;
        }
        
        
    }
    
    function round_to($val, $to) {
	$mn=0;
	if ($val==0) {
		return 0;
	}
	while (abs($val)<0.1) {
		$val *= 10;
		$mn--;
	}

	$val = round($val, $to);
	if ($mn!=0) $val *= pow(10,$mn);
	return $val;
    }
    
    function start(){
        
        $this->parse_pairs();
        
    }
    
    function get_pair_file(){
        return str_replace('.txt','_'.$this->main_pair.'.txt',$this->files['pairs']);
    }
    
}

function pr($data){
    echo '<pre>';
    print_r($data);
    echo '</pre>';
}

function pr_echo($data){
    ob_start();
    print_r($data);
    return  ob_get_clean();
}

function red($str){
    return "\033[0;31m".$str."\033[0m";
}


function set_counter(){
    global $global_counter;
    return $global_counter++;
}

function get_counter(){
    global $global_counter;
    return $global_counter;
}

function green($str){
    return "\033[0;32m".$str."\033[0m";
}